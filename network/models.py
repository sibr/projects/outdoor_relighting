# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


import collections

import numpy as np
import scipy.io
import tensorflow as tf

from imgProc import lab_to_rgb, deprocess_lab
from netLayers import lrelu, gen_conv, gen_deconv, conv2d, deconv2d, instance_norm
from options import CROP_SIZE
from options import a

Model = collections.namedtuple("Model", "outputs, outputs_refinementSce, outputs_refinementTar, losses, gen_grads_and_vars, train")
##### MODELS ADN LOSSES
def compute_error_mask(real, fake, mask):
    return tf.reduce_mean(tf.multiply(tf.abs(fake - real), mask))  # simple loss


def compute_error(real, fake):
    return tf.reduce_mean(tf.abs(fake - real))  # simple loss


def build_net(ntype, nin, nwb=None, name=None):
    if ntype == 'conv':
        return tf.nn.relu(tf.nn.conv2d(nin, nwb[0], strides=[1, 1, 1, 1], padding='SAME', name=name) + nwb[1])
    elif ntype == 'pool':
        return tf.nn.avg_pool(nin, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')


def get_weight_bias(vgg_layers, i):
    weights = vgg_layers[i][0][0][2][0][0]
    weights = tf.constant(weights)
    bias = vgg_layers[i][0][0][2][0][1]
    bias = tf.constant(np.reshape(bias, (bias.size)))
    return weights, bias


def build_vgg19(input, reuse=False):
    if reuse:
        tf.get_variable_scope().reuse_variables()
    net = {}
    vgg_rawnet = scipy.io.loadmat('vgg/imagenet-vgg-verydeep-19.mat')
    vgg_layers = vgg_rawnet['layers'][0]
    net['input'] = input - np.array([123.6800, 116.7790, 103.9390]).reshape((1, 1, 1, 3))
    net['conv1_1'] = build_net('conv', net['input'], get_weight_bias(vgg_layers, 0), name='vgg_conv1_1')
    net['conv1_2'] = build_net('conv', net['conv1_1'], get_weight_bias(vgg_layers, 2), name='vgg_conv1_2')
    net['pool1'] = build_net('pool', net['conv1_2'])
    net['conv2_1'] = build_net('conv', net['pool1'], get_weight_bias(vgg_layers, 5), name='vgg_conv2_1')
    net['conv2_2'] = build_net('conv', net['conv2_1'], get_weight_bias(vgg_layers, 7), name='vgg_conv2_2')
    net['pool2'] = build_net('pool', net['conv2_2'])
    net['conv3_1'] = build_net('conv', net['pool2'], get_weight_bias(vgg_layers, 10), name='vgg_conv3_1')
    net['conv3_2'] = build_net('conv', net['conv3_1'], get_weight_bias(vgg_layers, 12), name='vgg_conv3_2')
    net['conv3_3'] = build_net('conv', net['conv3_2'], get_weight_bias(vgg_layers, 14), name='vgg_conv3_3')
    net['conv3_4'] = build_net('conv', net['conv3_3'], get_weight_bias(vgg_layers, 16), name='vgg_conv3_4')
    net['pool3'] = build_net('pool', net['conv3_4'])
    net['conv4_1'] = build_net('conv', net['pool3'], get_weight_bias(vgg_layers, 19), name='vgg_conv4_1')
    net['conv4_2'] = build_net('conv', net['conv4_1'], get_weight_bias(vgg_layers, 21), name='vgg_conv4_2')
    net['conv4_3'] = build_net('conv', net['conv4_2'], get_weight_bias(vgg_layers, 23), name='vgg_conv4_3')
    net['conv4_4'] = build_net('conv', net['conv4_3'], get_weight_bias(vgg_layers, 25), name='vgg_conv4_4')
    net['pool4'] = build_net('pool', net['conv4_4'])
    net['conv5_1'] = build_net('conv', net['pool4'], get_weight_bias(vgg_layers, 28), name='vgg_conv5_1')
    net['conv5_2'] = build_net('conv', net['conv5_1'], get_weight_bias(vgg_layers, 30), name='vgg_conv5_2')
    return net


def create_generator_UNet(generator_inputs, generator_outputs_channels, ngf):
    layers = []

    # encoder_0: [batch, 256, 256, in_channels] => [batch, 256, 256, ngf//2]

    with tf.variable_scope("encoder_1"):
        initializer = tf.random_normal_initializer(0, 0.02)
        output = tf.layers.conv2d(generator_inputs, ngf, kernel_size=3, strides=(1, 1), padding="same",
                                  kernel_initializer=initializer)
        layers.append(output)

    layer_specs = [
        ngf * 1,  # encoder_1: [batch, 256, 256, ngf] => [batch, 128, 128, ngf]
        ngf * 2,  # encoder_2: [batch, 128, 128, ngf] => [batch, 64, 64, ngf * 2]
        ngf * 4,  # encoder_3: [batch, 64, 64, ngf * 2] => [batch, 32, 32, ngf * 4]
        ngf * 8,  # encoder_4: [batch, 32, 32, ngf * 4] => [batch, 16, 16, ngf * 8]
        ngf * 8,  # encoder_5: [batch, 16, 16, ngf * 8] => [batch, 8, 8, ngf * 8]
        ngf * 8,  # encoder_6: [batch, 8, 8, ngf * 8] => [batch, 4, 4, ngf * 8]
        ngf * 8,  # encoder_7: [batch, 4, 4, ngf * 8] => [batch, 2, 2, ngf * 8]
        ngf * 8,  # encoder_8: [batch, 2, 2, ngf * 8] => [batch, 1, 1, ngf * 8]
    ]

    for out_channels in layer_specs:
        with tf.variable_scope("encoder_%d" % (len(layers) + 1)):
            rectified = lrelu(layers[-1], 0.2)
            # [batch, in_height, in_width, in_channels] => [batch, in_height/2, in_width/2, out_channels]
            convolved = gen_conv(rectified, out_channels)
            output = convolved
            layers.append(output)

    layer_specs = [
        (ngf * 8, 0.5),  # decoder_8: [batch, 1, 1, ngf * 8] => [batch, 2, 2, ngf * 8 * 2]
        (ngf * 8, 0.5),  # decoder_7: [batch, 2, 2, ngf * 8 * 2] => [batch, 4, 4, ngf * 8 * 2]
        (ngf * 8, 0.5),  # decoder_6: [batch, 4, 4, ngf * 8 * 2] => [batch, 8, 8, ngf * 8 * 2]
        (ngf * 8, 0.0),  # decoder_5: [batch, 8, 8, ngf * 8 * 2] => [batch, 16, 16, ngf * 8 * 2]
        (ngf * 4, 0.0),  # decoder_4: [batch, 16, 16, ngf * 8 * 2] => [batch, 32, 32, ngf * 4 * 2]
        (ngf * 2, 0.0),  # decoder_3: [batch, 32, 32, ngf * 4 * 2] => [batch, 64, 64, ngf * 2 * 2]
        (ngf, 0.0),  # decoder_2: [batch, 64, 64, ngf * 2 * 2] => [batch, 128, 128, ngf * 2]
        (ngf, 0.0),  # decoder_1: [batch, 128, 128, ngf * 2 * 2] => [batch, 256, 256, ngf * 2]
    ]

    num_encoder_layers = len(layers)
    for decoder_layer, (out_channels, dropout) in enumerate(layer_specs):
        skip_layer = num_encoder_layers - decoder_layer - 1
        with tf.variable_scope("decoder_%d" % (skip_layer + 1)):
            if decoder_layer == 0:
                # first decoder layer doesn't have skip connections
                # since it is directly connected to the skip_layer
                input = layers[-1]
            else:
                if a.mode == "test":
                    in_shape = tf.shape(layers[-1])
                    skip_shape = tf.shape(layers[skip_layer])
                    offsets = [0, (in_shape[1] - skip_shape[1]) // 2, (in_shape[2] - skip_shape[2]) // 2, 0]
                    size = [-1, skip_shape[1], skip_shape[2], layer_specs[decoder_layer - 1][0]]
                    layers[-1] = tf.slice(layers[-1], offsets, size)
                input = tf.concat([layers[-1], layers[skip_layer]], axis=3)

            rectified = tf.nn.relu(input)
            # [batch, in_height, in_width, in_channels] => [batch, in_height*2, in_width*2, out_channels]
            output = gen_deconv(rectified, out_channels)

            if dropout > 0.0:
                output = tf.nn.dropout(output, keep_prob=1 - dropout)

            layers.append(output)

    # decoder_1: [batch, 256, 256, ngf ] => [batch, 256, 256, generator_outputs_channels]
    with tf.variable_scope("decoder_1"):
        if a.mode == "test":
            in_shape = tf.shape(layers[-1])
            skip_shape = tf.shape(layers[0])
            offsets = [0, (in_shape[1] - skip_shape[1]) // 2, (in_shape[2] - skip_shape[2]) // 2, 0]
            size = [-1, skip_shape[1], skip_shape[2], ngf]
            layers[-1] = tf.slice(layers[-1], offsets, size)
        input = tf.concat([layers[-1], layers[0]], axis=3)
        rectified = tf.nn.relu(input)
        output = tf.layers.conv2d_transpose(rectified, generator_outputs_channels, kernel_size=3, strides=(1, 1),
                                            padding="same", kernel_initializer=initializer)
        # output = gen_deconv(rectified, generator_outputs_channels)
        if a.tanh:
            output = tf.tanh(output)
        layers.append(output)

    return layers[-1]


def create_generator_ResNet(generator_inputs, generator_outputs_channels, ngf):
    def residule_block(x, dim, ks=3, s=1, name='res'):
        p = int((ks - 1) / 2)
        y = tf.pad(x, [[0, 0], [p, p], [p, p], [0, 0]], "REFLECT")
        y = instance_norm(conv2d(y, dim, ks, s, padding='VALID', name=name + '_c1'), name + '_bn1')
        y = tf.pad(tf.nn.relu(y), [[0, 0], [p, p], [p, p], [0, 0]], "REFLECT")
        y = instance_norm(conv2d(y, dim, ks, s, padding='VALID', name=name + '_c2'), name + '_bn2')
        return y + x

    # Justin Johnson's model from https://github.com/jcjohnson/fast-neural-style/
    # The network with 9 blocks consists of: c7s1-32, d64, d128, R128, R128, R128,
    # R128, R128, R128, R128, R128, R128, u64, u32, c7s1-3
    c0 = tf.pad(generator_inputs, [[0, 0], [3, 3], [3, 3], [0, 0]], "REFLECT")
    c1 = tf.nn.relu(conv2d(c0, ngf, 7, 1, padding='VALID', name='g_e1_c'))
    c2 = tf.nn.relu(conv2d(c1, ngf * 2, 3, 2, name='g_e2_c'))
    c3 = tf.nn.relu(conv2d(c2, ngf * 4, 3, 2, name='g_e3_c'))
    # define G network with 9 resnet blocks
    r1 = residule_block(c3, ngf * 4, name='g_r1')
    r2 = residule_block(r1, ngf * 4, name='g_r2')
    r3 = residule_block(r2, ngf * 4, name='g_r3')
    r4 = residule_block(r3, ngf * 4, name='g_r4')
    r5 = residule_block(r4, ngf * 4, name='g_r5')
    r6 = residule_block(r5, ngf * 4, name='g_r6')
    r7 = residule_block(r6, ngf * 4, name='g_r7')
    r8 = residule_block(r7, ngf * 4, name='g_r8')
    r9 = residule_block(r8, ngf * 4, name='g_r9')

    d1 = deconv2d(r9, ngf * 2, 3, 2, name='g_d1_dc')
    if a.mode == "test":
        in_shape = tf.shape(c2)
        out_shape = tf.shape(d1)
        offsets = [0, (in_shape[1] - out_shape[1]) // 2, (in_shape[2] - out_shape[2]) // 2, 0]
        size = [-1, out_shape[1], out_shape[2], ngf * 2]
        d1 = tf.slice(d1, offsets, size)
    d1 = tf.nn.relu(instance_norm(d1, 'g_d1_bn'))
    d2 = deconv2d(d1, ngf, 3, 2, name='g_d2_dc')
    if a.mode == "test":
        in_shape = tf.shape(generator_inputs)
        out_shape = tf.shape(d2)
        offsets = [0, (in_shape[1] - out_shape[1]) // 2, (in_shape[2] - out_shape[2]) // 2, 0]
        size = [-1, out_shape[1], out_shape[2], ngf]
        d2 = tf.slice(d2, offsets, size)
    d2 = tf.nn.relu(instance_norm(d2, 'g_d2_bn'))
    d2 = tf.pad(d2, [[0, 0], [3, 3], [3, 3], [0, 0]], "REFLECT")
    pred = conv2d(d2, generator_outputs_channels, 7, 1, padding='VALID', name='g_pred_c')
    if a.tanh:
        pred = tf.nn.tanh(pred)

    return pred


def create_generator(generator_inputs, generator_outputs_channels, ngf):
    if a.arch == "unet":
        return create_generator_UNet(generator_inputs, generator_outputs_channels, ngf)
    elif a.arch == "resnet":
        return create_generator_ResNet(generator_inputs, generator_outputs_channels, ngf)


# MODIFIED FROM https://github.com/markdtw/least-squares-gan/blob/master/model.py
def lrelu_dis(x, leak=0.2, name="lrelu"):
    with tf.variable_scope(name):
        f1 = 0.5 * (1 + leak)
        f2 = 0.5 * (1 - leak)
        return f1 * x + f2 * abs(x)


def create_model(inputs, gtRGB, inputs_refinementSce, inputs_refinementTar, gt_refinementSce, gt_refinementTar,
                 maskTarSMLoss):
    with tf.variable_scope("generatorRefinedSMSce"):
        out_channels_SMSce = int(gt_refinementSce.get_shape()[-1])
        refinedSMSce = create_generator(inputs_refinementSce, out_channels_SMSce, a.ngf // 4)

    with tf.variable_scope("generatorRefinedSMTar"):
        out_channels_SMTar = int(gt_refinementTar.get_shape()[-1])
        refinedSMTar = create_generator(inputs_refinementTar, out_channels_SMTar, a.ngf // 4)

    with tf.variable_scope("generator"):
        out_channels_RGB = int(gtRGB.get_shape()[-1])
        outputRGB = create_generator(tf.concat([inputs, refinedSMSce, refinedSMTar], axis=-1), out_channels_RGB, a.ngf)

    with tf.variable_scope("loss"):
        with tf.name_scope("generator_loss"):

            if a.lab:
                gtRGB_scaled = 255.0 * lab_to_rgb(deprocess_lab(gtRGB))
                outputRGB_scaled = 255.0 * lab_to_rgb(deprocess_lab(outputRGB))
            else:
                gtRGB_scaled = 255.0 * (gtRGB + 1.0) / 2.0
                outputRGB_scaled = 255.0 * (outputRGB + 1.0) / 2.0

            gt_refinementSce_scaled = 255.0 * (gt_refinementSce + 1.0) / 2.0
            refinedSMSce_scaled = 255.0 * (refinedSMSce + 1.0) / 2.0
            gt_refinementTar_scaled = 255.0 * (gt_refinementTar + 1.0) / 2.0
            refinedSMTar_scaled = 255.0 * (refinedSMTar + 1.0) / 2.0

            if a.cloud:
                vgg_real = build_vgg19(gtRGB_scaled[:, :, :, 0:3])
                vgg_fake = build_vgg19(outputRGB_scaled[:, :, :, 0:3], reuse=True)
                vgg_real_c = build_vgg19(gtRGB_scaled[:, :, :, 3:6], reuse=True)
                vgg_fake_c = build_vgg19(outputRGB_scaled[:, :, :, 3:6], reuse=True)
            else:
                vgg_real = build_vgg19(gtRGB_scaled)
                vgg_fake = build_vgg19(outputRGB_scaled, reuse=True)

            if a.plTar:
                vgg_real_tar = build_vgg19(gt_refinementTar_scaled, reuse=True)
                vgg_fake_tar = build_vgg19(refinedSMTar_scaled, reuse=True)

            clipped_RefinedSMTar = tf.clip_by_value(refinedSMTar_scaled, 0.0, 255.0)
            maskLoss = 2.0 * tf.stop_gradient(
                (255.0 - 0.99 * tf.abs(clipped_RefinedSMTar - gt_refinementTar_scaled)) / (
                        clipped_RefinedSMTar + 255.0))

            p0 = compute_error_mask(gtRGB_scaled, outputRGB_scaled, maskLoss)
            p1 = compute_error_mask(vgg_real['conv1_2'], vgg_fake['conv1_2'], maskLoss) / 2.6
            p2 = compute_error_mask(vgg_real['conv2_2'], vgg_fake['conv2_2'], tf.stop_gradient(
                tf.image.resize_bilinear(maskLoss, (CROP_SIZE // 2, CROP_SIZE // 2)))) / 4.8
            p3 = compute_error_mask(vgg_real['conv3_2'], vgg_fake['conv3_2'], tf.stop_gradient(
                tf.image.resize_bilinear(maskLoss, (CROP_SIZE // 4, CROP_SIZE // 4)))) / 3.7
            p4 = compute_error_mask(vgg_real['conv4_2'], vgg_fake['conv4_2'], tf.stop_gradient(
                tf.image.resize_bilinear(maskLoss, (CROP_SIZE // 8, CROP_SIZE // 8)))) / 5.6
            p5 = compute_error_mask(vgg_real['conv5_2'], vgg_fake['conv5_2'], tf.stop_gradient(
                tf.image.resize_bilinear(maskLoss, (CROP_SIZE // 16, CROP_SIZE // 16)))) * 10 / 1.5
            if a.cloud:
                p0_c = compute_error(gtRGB_scaled, outputRGB_scaled)
                p1_c = compute_error(vgg_real_c['conv1_2'], vgg_fake_c['conv1_2']) / 2.6
                p2_c = compute_error(vgg_real_c['conv2_2'], vgg_fake_c['conv2_2']) / 4.8
                p3_c = compute_error(vgg_real_c['conv3_2'], vgg_fake_c['conv3_2']) / 3.7
                p4_c = compute_error(vgg_real_c['conv4_2'], vgg_fake_c['conv4_2']) / 5.6
                p5_c = compute_error(vgg_real_c['conv5_2'], vgg_fake_c['conv5_2']) * 10 / 1.5

            smSce_loss = 10 * compute_error(gt_refinementSce_scaled, refinedSMSce_scaled)

            if a.plTar:
                p0_tar = compute_error_mask(gt_refinementTar_scaled, refinedSMTar_scaled, maskTarSMLoss)
                p1_tar = compute_error_mask(vgg_real_tar['conv1_2'], vgg_fake_tar['conv1_2'], maskTarSMLoss) / 2.6
                p2_tar = compute_error_mask(vgg_real_tar['conv2_2'], vgg_fake_tar['conv2_2'],
                                            tf.image.resize_bilinear(maskLoss, (CROP_SIZE // 2, CROP_SIZE // 2))) / 4.8
                p3_tar = compute_error_mask(vgg_real_tar['conv3_2'], vgg_fake_tar['conv3_2'],
                                            tf.image.resize_bilinear(maskLoss, (CROP_SIZE // 4, CROP_SIZE // 4))) / 3.7
                p4_tar = compute_error_mask(vgg_real_tar['conv4_2'], vgg_fake_tar['conv4_2'],
                                            tf.image.resize_bilinear(maskLoss, (CROP_SIZE // 8, CROP_SIZE // 8))) / 5.6
                p5_tar = compute_error_mask(vgg_real_tar['conv5_2'], vgg_fake_tar['conv5_2'],
                                            tf.image.resize_bilinear(maskLoss,
                                                                     (CROP_SIZE // 16, CROP_SIZE // 16))) * 10 / 1.5
                smTar_loss = p0_tar + p1_tar + p2_tar + p3_tar + p4_tar + p5_tar
            else:
                smTar_L1_loss = compute_error(gt_refinementTar_scaled, refinedSMTar_scaled)
                smTar_loss = smTar_L1_loss

            perceptual_loss = p0 + p1 + p2 + p3 + p4 + p5
            if a.cloud:
                perceptual_loss = perceptual_loss + p0_c + p1_c + p2_c + p3_c + p4_c + p5_c

            gen_loss = perceptual_loss + smSce_loss + 2 * 2 * smTar_loss


    with tf.name_scope("generator_train"):
        gen_tvars = [var for var in tf.trainable_variables() if (
                var.name.startswith("generator") or var.name.startswith(
            "generatorRefinedSMTar") or var.name.startswith("generatorRefinedSMSce"))]
        gen_optim = tf.train.AdamOptimizer(a.lr, a.beta1)
        gen_grads_and_vars = gen_optim.compute_gradients(gen_loss, var_list=gen_tvars)
        gen_train = gen_optim.apply_gradients(gen_grads_and_vars)

    ema = tf.train.ExponentialMovingAverage(decay=0.99)
    list_losses = [gen_loss, perceptual_loss, smSce_loss, smTar_loss]
    update_losses = ema.apply(list_losses)

    global_step = tf.train.get_or_create_global_step()
    incr_global_step = tf.assign(global_step, global_step + 1)

    losses = {
        "gen_loss": ema.average(gen_loss),
        "perceptual_loss": ema.average(perceptual_loss),
        "smSce_loss": ema.average(smSce_loss),
        "smTar_loss": ema.average(smTar_loss)
    }

    return Model(
        losses=losses,
        gen_grads_and_vars=gen_grads_and_vars,
        outputs=outputRGB,
        outputs_refinementSce=refinedSMSce,
        outputs_refinementTar=refinedSMTar,
        train=tf.group(update_losses, incr_global_step, gen_train),
    )
