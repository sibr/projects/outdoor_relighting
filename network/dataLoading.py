# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


import collections
import glob
import math
import os
import random

import tensorflow as tf

from imgProc import preprocess, transform, sRGB_toneMap, preprocess_lab, rgb_to_lab
from options import CROP_SIZE, IN_SIZE
from options import a

Examples = collections.namedtuple("Examples", "count, steps_per_epoch,iterator")
PathInfo = collections.namedtuple("Examples","name_main_dir, name_cam, name_light")
skyFactors=tf.constant([[0.013487552208647021, 0.01103625149925888, 0.00819517607574688], [0.015985048554203515, 0.013137126239913605, 0.009760374069609976], [0.018990568588584094, 0.015102528641223982, 0.010972979715774416], [0.0224637946257313, 0.017725414518338842, 0.012760001679582095], [0.026063956447670962, 0.020643513049064862, 0.014829292312984195], [0.02967885174033294, 0.02371044218249849, 0.01705674493350759], [0.033249917956269394, 0.026845935418652864, 0.01937708057651005], [0.036752924672331086, 0.03000379947942997, 0.021752629181826938], [0.04017651827066509, 0.033155301969561325, 0.02416011416759856], [0.04350969673346498, 0.03627882639972082, 0.02658390218678357], [0.04674267040786689, 0.039358712277959174, 0.02901353520533178], [0.04987585654369937, 0.04238629647759784, 0.0314414436946107], [0.05291330233208983, 0.045357691180088575, 0.033863250612787964], [0.05584208929619091, 0.04826197501392993, 0.03627396244467251], [0.058682121739567925, 0.051105991493151405, 0.03867231731724162], [0.06141393343763125, 0.053877240301769665, 0.04105393409375737], [0.0640444917369355, 0.05657718794067685, 0.043417433554727804], [0.06658329729179026, 0.05920932217850068, 0.04576197771303724], [0.06902356509930584, 0.06176870083341191, 0.04808461767209693], [0.07137895742043252, 0.06426177824663785, 0.050386208613062644], [0.07362859608506754, 0.06667635863891053, 0.05266174773386229], [0.07579418026889229, 0.06902358542887743, 0.05491266868780501], [0.07788056587494771, 0.07130638329422148, 0.05713949647328819], [0.07987808364670426, 0.07351869234414851, 0.059338155492200145], [0.08179581150956605, 0.07566454377147704, 0.06150970088255641], [0.0836352752460108, 0.07774571111106075, 0.06365279017847113], [0.08540169659755242, 0.07976519815051153, 0.06576802253665823], [0.08708731178303569, 0.08171774678123735, 0.06785168436136689], [0.08870971690327845, 0.0836134900173591, 0.06990670036977015], [0.09026004441489444, 0.0854483924223925, 0.07193140640287043], [0.09175562700229492, 0.0872325436355128, 0.07392836546288026], [0.0931771411075544, 0.0889537689278181, 0.07589131830054682], [0.094559762927444, 0.09063571276153873, 0.0778303735791655], [0.0958645339601901, 0.09225160589406865, 0.07973335492834695], [0.09711160129762518, 0.09381574780401612, 0.0816058581204214], [0.09832058021202161, 0.0953414161137727, 0.08345297319278601], [0.09946231919376218, 0.0968093086292714, 0.08526616372758077], [0.1005554973789713, 0.09823157905318651, 0.08704977996816456], [0.10160956781571484, 0.09961590231571424, 0.08880767596121893], [0.10260913172818126, 0.1009509491017547, 0.09053425188551771], [0.10357845166552888, 0.10225326684023263, 0.0922365315972566], [0.10448712408256215, 0.10350344279808064, 0.09390597813683896], [0.10535134318212146, 0.10471126709174362, 0.09554692706106077], [0.10620601704890976, 0.10590304131482252, 0.09717109492074232], [0.10699463229562713, 0.1070381652662876, 0.09875934707405341], [0.10776075761914397, 0.10814800179812262, 0.10032718726725658], [0.1084798569656103, 0.10921469081337563, 0.10186616772339989], [0.109170086434599, 0.1102520983360996, 0.10338163047129453], [0.1098394283673229, 0.11126604972973107, 0.10487839943145492], [0.1104666955431222, 0.11224209486245877, 0.10634865845565274], [0.11106268223548468, 0.11318712142968414, 0.10779521815497241], [0.11162552908972925, 0.1141003708951751, 0.10921826134150717], [0.11216375898586887, 0.11498697890708773, 0.11061976057200155], [0.1126815982163547, 0.11585272824461698, 0.11200467778532792], [0.11316594532811623, 0.1166867039852496, 0.11336608216169411], [0.11361612072736103, 0.11748808211621273, 0.1147031311058046], [0.11405516307774022, 0.11827474327584765, 0.11602684389743176], [0.11447067493099328, 0.11903812357762893, 0.11733118998795557], [0.11484887368273058, 0.11976696951086044, 0.11861128218865984], [0.11521973925997277, 0.12048485870086983, 0.11987939280983523], [0.1155667551097115, 0.1211790722579787, 0.12112840276467977], [0.11588084014824289, 0.12184203201265727, 0.12235505886510177], [0.11618644192118315, 0.1224937733086424, 0.12356988105640378], [0.11647061117238783, 0.1231235430399638, 0.12476803806784945], [0.1167430987103869, 0.12374044958021208, 0.12595346781938999], [0.11698465523917162, 0.12432695243127087, 0.12711568412740396], [0.11721297202866925, 0.12489852225325211, 0.12826439003092063], [0.1174328885327493, 0.12545985737921314, 0.129403883198319], [0.11762428651212531, 0.12599372122400795, 0.13052166124728476], [0.11781215694067596, 0.12652049088058057, 0.13162983199032247], [0.11796132815679097, 0.12701261955839852, 0.13271353460914148], [0.11812272566688833, 0.1275100961746173, 0.13379670398296373], [0.11825233746859702, 0.12797772770521254, 0.13485744227046453], [0.1183775806270405, 0.1284385326380834, 0.13590911249575258], [0.11848617198993901, 0.1288811798240294, 0.13694645693617005], [0.1185785010243182, 0.1293075447153239, 0.13796822706785478], [0.11865807680857211, 0.12971917993025261, 0.13897759562026873], [0.11872551418293181, 0.13011699665484908, 0.13997342788594028], [0.11878388005487414, 0.1305033085795196, 0.1409579891203427], [0.11883495851945798, 0.130881127565217, 0.1419338490435332], [0.11886828201313562, 0.13123934721044211, 0.1428916484143961], [0.1188915779407598, 0.13158678855325243, 0.14383925006082124], [0.11890582392536232, 0.1319225983524369, 0.1447757772482218], [0.11890854106715246, 0.13224424276161711, 0.14569800638933353], [0.11891282614841125, 0.1325647614638949, 0.14661604648512871], [0.11889801700579238, 0.13286538600775719, 0.14751661230503915], [0.1188738301916452, 0.13315370849784267, 0.14840456777684685], [0.11884016259423011, 0.1334315089100409, 0.14928178369747572], [0.11879619108989525, 0.13369584944726562, 0.15014553477699932], [0.118752227622731, 0.1339572614856622, 0.15100247775202977], [0.11869278175960017, 0.13420172621578505, 0.15184436451677766], [0.1186298975124647, 0.13443965115130077, 0.15267780867136846], [0.11855648207243152, 0.13466452961506042, 0.15349710758281387], [0.11847616628889236, 0.13488017340537314, 0.1543057525429201], [0.1183980684302351, 0.13509414603032552, 0.15510898961108602], [0.11831340494414283, 0.1352987454380379, 0.15590162300803345], [0.1182087719123515, 0.13548177330069014, 0.15667435235876198], [0.11810932327666877, 0.13566589488769332, 0.15744250447530247], [0.11799759218035788, 0.13583571087690482, 0.1581965956079565], [0.11789287945831631, 0.13600809577964476, 0.1589482512595229], [0.11777237898719164, 0.13616257089102943, 0.15968156946644935], [0.11764544133635481, 0.13630712666395525, 0.16040321581591793], [0.11751846344142597, 0.13644863470176563, 0.16111810892596992], [0.11738824386023454, 0.13658325313612285, 0.1618233230447379], [0.11725648097317713, 0.13671238991751242, 0.16251924522669056], [0.11711738059672132, 0.13683160970544297, 0.16320274143711783], [0.11697292529413203, 0.13694183437092322, 0.16387530777327491], [0.11682285999497252, 0.1370436564200204, 0.16453603312372528], [0.11666976269954077, 0.13713850577506106, 0.16518658242033527], [0.11651477873346126, 0.1372276709910054, 0.16582732639450523], [0.11635932708767258, 0.13731347557584653, 0.16646102812959362], [0.11619305851531192, 0.13738462733351214, 0.1670774185339419], [0.11603428555034973, 0.1374589690932517, 0.16769193901250307], [0.1158655484783177, 0.13752065324458185, 0.16829055140658591], [0.11570189912895214, 0.13758294061160734, 0.168885251632694], [0.1155246624980607, 0.13762891402029992, 0.16946068187266816], [0.11534838744507776, 0.1376722717958066, 0.1700287882696751], [0.11517268719558425, 0.1377123035199728, 0.1705894440781269], [0.11499282213231643, 0.13774424968142673, 0.17113736005627653], [0.11480978128875534, 0.13776984701382258, 0.1716750387301057], [0.11463165553042874, 0.13779599558969405, 0.1722074644332867], [0.11444750141948869, 0.13781266330865324, 0.17272630302109795], [0.11425753060584122, 0.1378206848218414, 0.17323266294037237], [0.1140677954749489, 0.13782547833285824, 0.17372993092890088], [0.11388131098552325, 0.1378292766995696, 0.17422083337259442], [0.113687784089711, 0.1378227053776716, 0.17469810683580736], [0.11349909482837597, 0.13781761836772613, 0.17517030183004773], [0.11329953996859235, 0.13779826576859847, 0.1756231526621442], [0.1131134295174478, 0.13778853298985083, 0.1760802714588691], [0.1129095141883926, 0.13775891880156968, 0.17651290346348608], [0.11270939789622039, 0.13772839859723676, 0.1769391002617903], [0.11251310167612212, 0.13769953646239225, 0.17736053452775408], [0.11231204498527213, 0.13766226109944193, 0.17776940065384286], [0.1121110307245862, 0.13762171378694396, 0.17816827393696663], [0.1119110987864083, 0.13757936979358706, 0.17855991368938065], [0.111705632385688, 0.13752904015723907, 0.17893801413975985], [0.11150068270627127, 0.1374759632336246, 0.17930700383453302], [0.11129861361303123, 0.1374225323399797, 0.1796701824842361], [0.11109590178139458, 0.1373652940589561, 0.18002359809719895], [0.1108804292123051, 0.1372942703432747, 0.1803572784780338], [0.11067325585274136, 0.13722753310749852, 0.18068844604649476], [0.11046654733439508, 0.13715969341805612, 0.18101304942732993], [0.11025305050947327, 0.13708164935328165, 0.1813210255901033], [0.11004215137491902, 0.13700450568928002, 0.18162314444718253], [0.10983015977233153, 0.136923928512918, 0.18191645101602247], [0.10961447791466064, 0.13683723645129767, 0.1821960521893492], [0.10940336831480268, 0.13675265852080226, 0.1824730108522012], [0.1091898971907305, 0.1366641582266595, 0.18273901086792632], [0.10896777972979231, 0.1365648736360626, 0.1829870578431283], [0.1087565033748598, 0.13647458749904331, 0.1832380691506287], [0.10853699970246822, 0.13637447787595564, 0.1834729167701778], [0.10831411781346215, 0.13626943413617046, 0.18369488843739934], [0.10809336640776528, 0.13616479563268727, 0.18391135876599513], [0.10787651416522742, 0.13606172123157753, 0.18412318516106724], [0.10765096132877354, 0.135949198648256, 0.18431752719864247], [0.10742661357962488, 0.1358363445821623, 0.18450616909833997], [0.1072040085916075, 0.13572353112559823, 0.18468660200852524], [0.10697417907568664, 0.1356026939384994, 0.18485289043756867], [0.10674970795451885, 0.13548540155671937, 0.18501535998868623], [0.10652261144473013, 0.13536424905699926, 0.18516744362583554], [0.10629514736687873, 0.13524212700570445, 0.18531165262427193], [0.10606805804929512, 0.13511911459277365, 0.18544710883647483], [0.10583653456221796, 0.13498996372641595, 0.18557000327750856], [0.10560851431116226, 0.13486360473107661, 0.18568838730234682], [0.1053785720498809, 0.1347340929919713, 0.1857964107597782], [0.10514451851475157, 0.13459933531609694, 0.1858919272977608], [0.10491379346256736, 0.13446694304704004, 0.1859823608595592], [0.10468561670136808, 0.13433556922029657, 0.18606805288870681], [0.10445216765892179, 0.13419871832875835, 0.18613869502434746], [0.10421752653818457, 0.13405912049481772, 0.1862003705301437], [0.10398346367187399, 0.1339189880040089, 0.1862537309118256], [0.10375350455394192, 0.13378131229184279, 0.18630289285103196], [0.10351799335374805, 0.13363765159463792, 0.18633763248504084], [0.10328707937635473, 0.1334967793314291, 0.18636802013153048], [0.10305769436833391, 0.1333561987049795, 0.18639118574620778], [0.10282856237700141, 0.13321407085539916, 0.18640503982486584], [0.10259547477428928, 0.13306734317240462, 0.18640673299861787], [0.1023663745155818, 0.1329227754234618, 0.18640279858618716], [0.10213868785268794, 0.13277744780604117, 0.18639057153514196], [0.10190823662096017, 0.1326279076902415, 0.18636580266370337], [0.10168789856246446, 0.13248656761761737, 0.18634206316249805], [0.1014617877597913, 0.13233765223854901, 0.18630209025711134], [0.10124384933958655, 0.1321946975329757, 0.18626104235086938], [0.10102194269560467, 0.13204523834843418, 0.18620488301460142], [0.10080380645827405, 0.13189753359677195, 0.1861418915687214], [0.10058995804914915, 0.13175162934797136, 0.18607294284054576], [0.1003805663529291, 0.1316075469428706, 0.18599797079395436], [0.1001711641195919, 0.13146051894094463, 0.18591085946436572], [0.09996713961532501, 0.13131609304150196, 0.1858185704638631], [0.09976506907647008, 0.1311708399478443, 0.18571680310403985], [0.0995709220769885, 0.1310296872964738, 0.1856114777164618], [0.09937614443204475, 0.1308845696032571, 0.18549282316562676], [0.09918725628889531, 0.13074267737696263, 0.18536793129606413], [0.09900073056449091, 0.1305988077015827, 0.18523282104191247], [0.09882420268407971, 0.13046146697091782, 0.18509572609017097], [0.09865405946155688, 0.1303264036191071, 0.18495230258639989], [0.09848614511440082, 0.13018975710412886, 0.1847970890875133], [0.09832664579487713, 0.13005705317662694, 0.18463715864684863], [0.09816659416628623, 0.1299193327274355, 0.18446227638154244], [0.0980177820835221, 0.1297886514955679, 0.18428585382286747], [0.09787777496684778, 0.1296618217665738, 0.184104362879759], [0.09773900964703004, 0.12953172948501196, 0.18390863194744558], [0.09761043029518292, 0.129407055508105, 0.18370892312618614], [0.09749128916250276, 0.1292868071359006, 0.18350450157366063], [0.09737895016172374, 0.12916829108324887, 0.18329135037400382], [0.09727426764092459, 0.12905219777637847, 0.18307010769373405], [0.09717775837778837, 0.12893911083297466, 0.18284230493735068], [0.09708763364701746, 0.12882734218832925, 0.1826049230904642], [0.09701082982030254, 0.12872311373627765, 0.18236523267211202], [0.09694109438284981, 0.12862065264910907, 0.18211638088900606], [0.09687941540929725, 0.12852048052390017, 0.18185862845291081], [0.09682599357153276, 0.12842308422709886, 0.1815936244057325], [0.09678346386300538, 0.1283313240486157, 0.1813221961367104], [0.09675132092769498, 0.12824451301066456, 0.18104491205278256], [0.0967256530759645, 0.1281581739060172, 0.18075668111511337], [0.09671533004444959, 0.12808210358971062, 0.18046774701233131], [0.09670995855333692, 0.12800469893026958, 0.18016510488339765], [0.09671757464380282, 0.1279351637885512, 0.17985900319516257], [0.09673778580585204, 0.12787256339689393, 0.17954786028447525], [0.09676278665184007, 0.127809440213238, 0.1792233959186044], [0.09680391340605844, 0.12775711451386124, 0.17889863526870428], [0.0968562239169895, 0.12771020448967804, 0.17856659924678558], [0.09691677343810368, 0.12766655704717308, 0.17822508314941257], [0.09698794080264261, 0.1276283337635772, 0.17787615371825283], [0.09706911013269517, 0.1275952469593133, 0.17751912779969753], [0.0971635082248775, 0.12757010102491664, 0.1771575257519895], [0.0972693294549272, 0.1275516717517002, 0.17678977880264934], [0.09738533103676512, 0.12753855604012052, 0.1764138489690435], [0.09751176420190187, 0.127531508009197, 0.1760299357944344], [0.09765212681241499, 0.12753353996240233, 0.17564214416787777], [0.09780056182416903, 0.12753962202344227, 0.17524394645856178], [0.09796447283816416, 0.12755695940868203, 0.17484426134335065], [0.0981327735237284, 0.12757517706044633, 0.17442996771158462], [0.09832114153436634, 0.12760893549022878, 0.1740180958026491], [0.09851531873375434, 0.12764489197473428, 0.17359362076287765], [0.09871778481738805, 0.12768631649280193, 0.17315994388562458], [0.09893719531144894, 0.12774096428108436, 0.17272564803815618], [0.09916327656866043, 0.12779960039806343, 0.17227930430213556], [0.09940258973561318, 0.12786788057246007, 0.17182949024186256], [0.09964991552341244, 0.1279421282403452, 0.17136969796971174], [0.09991185286675598, 0.12802822759086263, 0.1709067388761279], [0.10018171490969935, 0.12812015040156602, 0.1704345781908135], [0.10046628531037421, 0.1282243872481084, 0.16995940212690158], [0.10075891524419438, 0.12833486958972326, 0.1694750843514403], [0.10105750640335574, 0.12845036906289792, 0.16897950710141998], [0.1013656489013315, 0.12857381032284573, 0.1684766523251389], [0.10169336318865252, 0.12871439056508371, 0.1679762790461772], [0.1020297325348579, 0.12886339015158738, 0.16746808903991553], [0.10236713448975644, 0.1290131864281362, 0.16694379407403007], [0.10272001041874225, 0.12917765810331516, 0.1664192700047761], [0.10308252511138048, 0.12935083711644046, 0.1658871724629277], [0.1034500158879204, 0.12953004701503626, 0.165344267251054], [0.10383482900177113, 0.12972473200716908, 0.16480136427648034], [0.10422627653953527, 0.12992704167037292, 0.16425060570490904], [0.10462462562945297, 0.1301375223294558, 0.16369046053227093], [0.10502891748597462, 0.1303527894336847, 0.16312014898248925]])

##### LOADING

def load_png48bit(path, channels, max_val):
    contents = tf.read_file(path)

    raw_input = tf.image.decode_png(contents, dtype=tf.uint16)
    raw_input = tf.image.convert_image_dtype(raw_input, dtype=tf.float32)
    raw_input = tf.scalar_mul(max_val, raw_input)

    # assertion = tf.assert_equal(tf.shape(raw_input)[2], 3, message="image does not have 3 channels")
    # with tf.control_dependencies([assertion]):
    #    raw_input = tf.identity(raw_input)

    raw_input.set_shape([None, None, 3])
    return raw_input[:, :, 0:channels]


def load_png16bit(path, max_val):
    contents = tf.read_file(path)

    raw_input = tf.image.decode_png(contents, dtype=tf.uint16)
    raw_input = tf.image.convert_image_dtype(raw_input, dtype=tf.float32)
    raw_input = tf.scalar_mul(max_val, raw_input)

    raw_input.set_shape([None, None, 1])
    return raw_input


def load_img8bit(path, channels):
    contents = tf.read_file(path)

    raw_input = tf.image.decode_image(contents, dtype=tf.uint8)
    raw_input = tf.image.convert_image_dtype(raw_input, dtype=tf.float32)

    # assertion = tf.assert_equal(tf.shape(raw_input)[2], 3, message="image does not have 3 channels")
    # with tf.control_dependencies([assertion]):
    #    raw_input = tf.identity(raw_input)

    raw_input.set_shape([None, None, 3])
    return raw_input[:, :, 0:channels]


def load_additional_data(path, channels):
    contents = tf.read_file(path)

    raw_input = tf.image.decode_png(contents)
    raw_input = tf.image.convert_image_dtype(raw_input, dtype=tf.float32)

    raw_input.set_shape([None, None, 3])
    return preprocess(raw_input[:, :, 0:channels])


def load_generic_data(name_main_dir, name_cam):
    paths_normal = name_main_dir + "/normalRecon/" + name_cam + ".png"

    input_normal = load_additional_data(paths_normal, 3)

    return input_normal


def load_light_specific_data(name_main_dir, name_cam, name_light):
    paths_angle = name_main_dir + "/angleRecon/" + name_cam + "/" + name_light + ".png"
    input_angle = load_additional_data(paths_angle, 1)

    paths_sunDir = name_main_dir + "/sunDir/" + name_cam + "/" + name_light + ".png"
    input_sunDir = load_additional_data(paths_sunDir, 3)

    paths_elevation = name_main_dir + "/elevation/" + name_cam + "/" + name_light + ".png"
    input_elevation = load_additional_data(paths_sunDir, 1)

    return [input_angle, input_sunDir, input_elevation]


def load_data(paths, table, table_lightMaps, randomLight, nameLight):
    # return [paths,tf.zeros([256,256,19]),tf.zeros([256,256,3])]
    # synchronize seed for image operations so that we do the same operations to both
    # input and output images
    seed = random.randint(0, 2 ** 31 - 1)

    rescale_size = [
        tf.random_uniform([1], CROP_SIZE, IN_SIZE[0] + 1, dtype=tf.int32)[0],
        tf.random_uniform([1], CROP_SIZE, IN_SIZE[1] + 1, dtype=tf.int32)[0]
    ]

    offset = [
        tf.random_uniform([1], 0, IN_SIZE[0] - rescale_size[0] + 1, dtype=tf.int32)[0],
        tf.random_uniform([1], 0, IN_SIZE[1] - rescale_size[1] + 1, dtype=tf.int32)[0]
    ]

    flip_bool = tf.constant(0)
    if a.flip:
        flip_bool = tf.random_uniform([1], 0, 2, seed=seed, dtype=tf.int32)[0]

    gt_bool = tf.random_uniform([1], 1, 3, seed=seed, dtype=tf.int32)[0]
    # flip_bool = tf.Print(flip_bool,[flip_bool,tf.equal(flip_bool,tf.constant(0)),tf.equal(flip_bool,0)])

    with tf.name_scope("select_target_light"):
        name_main_dir, name_cam, name_light_sce = path_info(paths)
        if randomLight:
            listLights = table.lookup(name_main_dir)
            lights = tf.string_split([listLights], delimiter=",")
            numLight = tf.shape(lights)[1]
            indexLightTar = tf.random_uniform([1], minval=1, maxval=50, dtype=tf.int32, seed=seed)[0]
            name_light_tar = lights.values[indexLightTar]

            lightMap = table_lightMaps.lookup(name_main_dir)
            mapping = tf.string_split([lightMap], delimiter=",")
            name_light_closest_sce = mapping.values[tf.strings.to_number(name_light_sce, out_type=tf.int32)]
        else:
            name_light_tar = nameLight

    with tf.name_scope("load_im"):
        if a.mode == "train":

            raw_input_sun = load_png48bit(paths, 3, 1.5)

            input_shadowMap_sce_sun = load_png48bit(
                name_main_dir + "/shadowMapRGBRecon/" + name_cam + "/" + name_light_closest_sce + "_" + name_light_sce + ".png",
                3, 1.5)

            indexLightSky = tf.random_uniform([1], minval=52, maxval=57, dtype=tf.int32, seed=seed)[0]
            name_light_sky = tf.as_string(indexLightSky, width=8, fill='0')
            input_shadowMap_sce_sky = load_png48bit(
                name_main_dir + "/shadowMapRGBRecon/" + name_cam + "/" + name_light_sky + "_" + name_light_sce + ".png",
                3, 1.5)

            path_SM_tar_sun = tf.cond(tf.equal(gt_bool, 0),
                                      lambda: name_main_dir + "/shadowMapRGB/" + name_cam + "/" + name_light_closest_sce + "_" + name_light_tar + ".png",
                                      lambda: name_main_dir + "/shadowMapRGBRecon/" + name_cam + "/" + name_light_closest_sce + "_" + name_light_tar + ".png")
            path_SM_tar_sky = tf.cond(tf.equal(gt_bool, 0),
                                      lambda: name_main_dir + "/shadowMapRGB/" + name_cam + "/" + name_light_sky + "_" + name_light_tar + ".png",
                                      lambda: name_main_dir + "/shadowMapRGBRecon/" + name_cam + "/" + name_light_sky + "_" + name_light_tar + ".png")

            input_shadowMap_tar_sun = load_png48bit(path_SM_tar_sun, 3, 1.5)
            input_shadowMap_tar_sky = load_png48bit(path_SM_tar_sky, 3, 1.5)

            raw_sky = load_png48bit(name_main_dir + "/rgb/sky/" + name_cam + "_" + name_light_sky + ".png", 3, 1.5)

            paths_gt_sun = name_main_dir + "/rgb/" + a.mode + "/" + name_cam + "_" + name_light_tar + ".png"
            raw_gt_sun = load_png48bit(paths_gt_sun, 3, 1.5)

            gt_shadowMap_sce = load_img8bit(name_main_dir + "/shadowMap/" + name_cam + "/" + name_light_sce + ".png", 1)
            gt_shadowMap_tar = load_img8bit(name_main_dir + "/shadowMap/" + name_cam + "/" + name_light_tar + ".png", 1)

            if a.cloud:
                raw_cloud = 0.15 * load_png48bit(name_main_dir + "/rgb/sky/" + name_cam + "_00000000.png", 3, 1.5)

        elif a.mode == "test":
            raw_input = load_img8bit(paths, 3)
            input_shadowMap_sce = load_img8bit(
                name_main_dir + "/shadowMapRGBRecon/" + name_cam + "/" + name_light_sce + "_" + name_light_sce + ".png",
                3)
            input_shadowMap_tar = load_img8bit(
                name_main_dir + "/shadowMapRGBRecon/" + name_cam + "/" + name_light_sce + "_" + name_light_tar + ".png",
                3)

            raw_gt = tf.zeros([256, 256, 3])
            gt_shadowMap_sce = tf.zeros([256, 256, 1])
            gt_shadowMap_tar = tf.zeros([256, 256, 1])
            paths = name_main_dir + "/rgb/" + a.mode + "/" + name_cam + "_" + name_light_sce + "_to_" + name_light_tar + ".jpg"

    # Loading additional features
    with tf.name_scope("load_additional_data"):
        input_normal = load_generic_data(name_main_dir, name_cam)
        input_angle_sce, input_sunDir_sce, input_elevation_sce = load_light_specific_data(name_main_dir, name_cam,
                                                                                          name_light_sce)
        input_angle_tar, input_sunDir_tar, input_elevation_tar = load_light_specific_data(name_main_dir, name_cam,
                                                                                          name_light_tar)

    # Loading distance shadowmask
    if a.mode == "train":
        path_SM_tar_dist = tf.cond(tf.equal(gt_bool, 0),
                                   lambda: name_main_dir + "/shadowMapDist/" + name_cam + "_" + name_light_tar + ".png",
                                   lambda: name_main_dir + "/shadowMapDistRecon/" + name_cam + "_" + name_light_tar + ".png")
        input_shadowMap_tar_dist_gt = load_png16bit(
            name_main_dir + "/shadowMapDist/" + name_cam + "_" + name_light_tar + ".png", 20.0)
    elif a.mode == "test":
        path_SM_tar_dist = name_main_dir + "/shadowMapDistRecon/" + name_cam + "_" + name_light_tar + ".png"

    input_shadowMap_tar_dist = load_png16bit(path_SM_tar_dist, 20.0)
    input_shadowMap_tar_area = load_png16bit(
        name_main_dir + "/shadowMapAreaRecon/" + name_cam + "_" + name_light_tar + ".png", 20.0)
    input_shadowMap_sce_dist = load_png16bit(
        name_main_dir + "/shadowMapDistRecon/" + name_cam + "_" + name_light_sce + ".png", 20.0)

    with tf.name_scope("transform_input_images"):

        if a.mode == "train":
            raw_sky = transform(raw_sky, flip_bool, rescale_size, offset, False)
            raw_gt_sun = transform(raw_gt_sun, flip_bool, rescale_size, offset, False)
            raw_input_sun = transform(raw_input_sun, flip_bool, rescale_size, offset, False)
            if a.cloud:
                raw_cloud = transform(raw_cloud, flip_bool, rescale_size, offset, False)

            input_shadowMap_sce_sun = transform(input_shadowMap_sce_sun, flip_bool, rescale_size, offset, False)
            input_shadowMap_tar_sun = transform(input_shadowMap_tar_sun, flip_bool, rescale_size, offset, False)
            input_shadowMap_sce_sky = transform(input_shadowMap_sce_sky, flip_bool, rescale_size, offset, False)
            input_shadowMap_tar_sky = transform(input_shadowMap_tar_sky, flip_bool, rescale_size, offset, False)

            gt_shadowMap_sce = transform(gt_shadowMap_sce, flip_bool, rescale_size, offset, False)
            gt_shadowMap_tar = transform(gt_shadowMap_tar, flip_bool, rescale_size, offset, False)

        elif a.mode == "test":
            raw_input = transform(raw_input, flip_bool, rescale_size, offset, False)
            gt_rgb = raw_gt
            input_shadowMap_sce = transform(input_shadowMap_sce, flip_bool, rescale_size, offset, False)
            input_shadowMap_tar = transform(input_shadowMap_tar, flip_bool, rescale_size, offset, False)

        input_normal = transform(input_normal, flip_bool, rescale_size, offset, True)

        input_sunDir_sce = transform(input_sunDir_sce, flip_bool, rescale_size, offset, True)
        input_sunDir_tar = transform(input_sunDir_tar, flip_bool, rescale_size, offset, True)

        input_angle_sce = transform(input_angle_sce, flip_bool, rescale_size, offset, False)
        input_angle_tar = transform(input_angle_tar, flip_bool, rescale_size, offset, False)

        input_elevation_sce = transform(input_elevation_sce, flip_bool, rescale_size, offset, False)
        input_elevation_tar = transform(input_elevation_tar, flip_bool, rescale_size, offset, False)

        input_shadowMap_sce_dist = transform(input_shadowMap_sce_dist, flip_bool, rescale_size, offset, False)
        input_shadowMap_tar_dist = transform(input_shadowMap_tar_dist, flip_bool, rescale_size, offset, False)
        input_shadowMap_tar_area = transform(input_shadowMap_tar_area, flip_bool, rescale_size, offset, False)

    # compositing
    if a.mode == "train":

        input_shadowMap_tar_dist_gt = transform(input_shadowMap_tar_dist_gt, flip_bool, rescale_size, offset, False)
        # skyColor = [tf.random_uniform([1],210.0,230.0)[0]/360.0,tf.random_uniform([1],0.2,1.0)[0],1.0]; #hue
        randRefIndex = tf.random_uniform([1], 50, 200, dtype=tf.int32)[0]
        skyColorSce = skyFactors[tf.cast(255.0 * (0.5 * (input_elevation_sce[0, 0, 0] + 1.0)), tf.int32)] / skyFactors[
            randRefIndex]
        skyColorTar = skyFactors[tf.cast(255.0 * (0.5 * (input_elevation_tar[0, 0, 0] + 1.0)), tf.int32)] / skyFactors[
            randRefIndex]

        fact_sky = tf.pow(2.0, tf.random_uniform([1], -1.5, 1.5, seed=seed))[0]  # 2**(-3,3)
        raw_sky_sce = tf.multiply(raw_sky, fact_sky * skyColorSce)
        raw_sky_tar = tf.multiply(raw_sky, fact_sky * skyColorTar)
        if a.cloud:
            raw_cloud = tf.multiply(raw_cloud, fact_sky)
        input_shadowMap_sce_sky = tf.multiply(input_shadowMap_sce_sky, fact_sky * skyColorSce)
        input_shadowMap_tar_sky = tf.multiply(input_shadowMap_tar_sky, fact_sky * skyColorTar)

        fact_sun = tf.pow(2.0, tf.random_uniform([1], 0.0, 2.0, seed=seed))[0]  # 2**(-3,3)
        sunColor = fact_sun * [tf.pow(2.0, tf.random_uniform([1], -0.10, 0.10))[0],
                               tf.pow(2.0, tf.random_uniform([1], -0.10, 0.10))[0],
                               tf.pow(2.0, tf.random_uniform([1], -0.10, 0.10))[0]]

        raw_input = tf.multiply(raw_input_sun, sunColor) + raw_sky_sce
        raw_gt = tf.multiply(raw_gt_sun, sunColor) + raw_sky_tar

        input_shadowMap_sce = tf.multiply(input_shadowMap_sce_sun, sunColor) + input_shadowMap_sce_sky
        input_shadowMap_tar = tf.multiply(input_shadowMap_tar_sun, sunColor) + input_shadowMap_tar_sky

    if a.mode == "train":
        # random exposure modification
        pctil = tf.contrib.distributions.percentile(raw_input, 90)
        maxExp = +2  # tf.math.log(1.00/pctil)/tf.math.log(2.0)
        minExp = 0  # tf.math.log(0.10/pctil)/tf.math.log(2.0)
        rand_exposure = tf.pow(2.0, tf.random_uniform([1], minExp, maxExp, seed=seed))[0]  # 2**(-3,3)
        raw_input = tf.scalar_mul(rand_exposure, raw_input)
        input_shadowMap_sce = tf.scalar_mul(rand_exposure, input_shadowMap_sce)
        input_shadowMap_tar = tf.scalar_mul(rand_exposure, input_shadowMap_tar)
        raw_gt = tf.scalar_mul(rand_exposure, raw_gt)
        # random color balance
        rand_colorB = [tf.pow(2.0, tf.random_uniform([1], -0.25, 0.25))[0],
                       tf.pow(2.0, tf.random_uniform([1], -0.25, 0.25))[0],
                       tf.pow(2.0, tf.random_uniform([1], -0.25, 0.25))[0]]
        raw_input = tf.multiply(raw_input, rand_colorB)
        input_shadowMap_sce = tf.multiply(input_shadowMap_sce, rand_colorB)
        input_shadowMap_tar = tf.multiply(input_shadowMap_tar, rand_colorB)
        raw_gt = tf.multiply(raw_gt, rand_colorB)
        # Tone mapping to sRGB space
        gamma = tf.random_uniform([1], 1.4, 2.8, seed=seed)[0]  # 2**(-3,3)
        raw_input = sRGB_toneMap(raw_input, gamma)
        input_shadowMap_sce = tf.clip_by_value(sRGB_toneMap(input_shadowMap_sce, gamma), 0.0, 1.0)
        input_shadowMap_tar = tf.clip_by_value(sRGB_toneMap(input_shadowMap_tar, gamma), 0.0, 1.0)
        raw_gt = sRGB_toneMap(raw_gt, gamma)
        if a.cloud:
            raw_cloud = tf.scalar_mul(rand_exposure, raw_cloud)
            #    raw_cloud = tf.multiply(raw_cloud,rand_colorB)
            raw_cloud = sRGB_toneMap(raw_cloud, gamma)
            raw_cloud = tf.clip_by_value(raw_cloud, 0.0, 10.0)

        raw_input = tf.clip_by_value(raw_input, 0.0, 1.0)
        raw_gt = tf.clip_by_value(raw_gt, 0.0, 2.0)

    if a.lab:
        input_rgb = preprocess_lab(rgb_to_lab(raw_input))
        # input_rgb.set_shape([None, None, 3])
        input_shadowMap_sce = preprocess_lab(rgb_to_lab(input_shadowMap_sce))
        # input_shadowMap_sce.set_shape([None, None, 3])
        input_shadowMap_tar = preprocess_lab(rgb_to_lab(input_shadowMap_tar))
        # input_shadowMap_tar.set_shape([None, None, 3])
        gt_rgb = preprocess_lab(rgb_to_lab(raw_gt))
        # gt_rgb.set_shape([None, None, 3])
    else:
        input_rgb = preprocess(raw_input)
        input_shadowMap_sce = preprocess(input_shadowMap_sce)
        input_shadowMap_tar = preprocess(input_shadowMap_tar)
        gt_rgb = preprocess(raw_gt)
        if a.cloud:
            cloud_rgb = preprocess(raw_cloud)
            gt_rgb = tf.concat([gt_rgb, cloud_rgb], -1)

    gt_shadowMap_sce = preprocess(gt_shadowMap_sce)
    gt_shadowMap_tar = preprocess(gt_shadowMap_tar)

    input_data = tf.concat([
        input_rgb,
        input_normal,
        input_sunDir_sce,
        input_sunDir_tar,
        input_angle_sce,
        input_angle_tar,
        input_elevation_sce,
        input_elevation_tar]
        , -1)

    input_data_refinementSce = tf.concat([
        input_rgb,
        input_shadowMap_sce,
        input_shadowMap_sce_dist]
        , -1)

    input_data_refinementTar = tf.concat([
        input_rgb,
        input_shadowMap_tar,
        input_shadowMap_tar_dist,
        input_shadowMap_tar_area]
        , -1)

    # Mask for target SM refinement loss
    if a.mode == "train":
        highVal = tf.ones([256, 256, 1])
        lowVal = 0.1 * tf.ones([256, 256, 1])
        maskTarSMLoss = tf.where(
            tf.logical_and(tf.greater(input_shadowMap_tar_dist, 19.9), tf.less(input_shadowMap_tar_dist_gt, 19.9)),
            lowVal, highVal)
    else:
        maskTarSMLoss = tf.zeros([256, 256, 1])
    return [paths, input_data, gt_rgb, input_data_refinementSce, input_data_refinementTar, gt_shadowMap_sce,
            gt_shadowMap_tar, maskTarSMLoss]


def load_examples(input_paths):
    # Get the light information
    lights_list_path = []

    lightMaps_list_path = []

    if len(input_paths) > 1:
        global datasetPrefix
        datasetPrefix = True

    for inputFolder in input_paths:
        print(inputFolder)
        light_file = open(inputFolder + '/lighting.txt', 'r')
        contentLights = light_file.readlines()
        contentLights = [x.strip() for x in contentLights]

        numLight = 0
        listLights = ""
        for x in contentLights:
            lineSplit = x.split()
            if len(lineSplit) > 0 and numLight < 192:
                lightId = "%08d" % (numLight)
                if numLight == 0:
                    listLights = lightId
                else:
                    listLights = listLights + "," + lightId
                numLight = numLight + 1

        lights_list_path.append(listLights)

        if a.mode == "train":
            light_mapping = open(inputFolder + '/light_mapping.txt', 'r')
            contentLightMap = light_mapping.readlines()
            contentLightMap = [x.strip() for x in contentLightMap]
            contentLightMap = [[int(x.split()[0]), int(x.split()[1])] for x in contentLightMap]
            contentLightMap = sorted(contentLightMap)

            lightMap = "00000000"
            for x in contentLightMap:
                closestLight = "%08d" % (x[1])
                lightMap = lightMap + "," + closestLight

            lightMaps_list_path.append(lightMap)
        print(numLight)

        # Light mapping for rgb shadowmap

    light_file.close()

    # we need to normalize input paths for the hash table.
    if a.mode == "train":
        table_lights = tf.contrib.lookup.HashTable(
            tf.contrib.lookup.KeyValueTensorInitializer(
                [p for p in input_paths],  # [os.path.normpath(p) for p in input_paths],
                lights_list_path),
            "",
            name='hashTableLight')

        table_lightMaps = tf.contrib.lookup.HashTable(
            tf.contrib.lookup.KeyValueTensorInitializer(
                [p for p in input_paths],  # [os.path.normpath(p) for p in input_paths],
                lightMaps_list_path),
            "",
            name='hashTableLightMap')

        fn = lambda x: load_data(x, table_lights, table_lightMaps, True, "")
        input_paths = [path + "/rgb/" + a.mode + "/*.png" for path in input_paths]

        count = 0
        for path_pattern in input_paths:
            count = count + len(glob.glob(path_pattern))
            print(str(len(glob.glob(path_pattern))) + " from " + path_pattern)

        files = tf.data.Dataset.list_files(input_paths[0])
        for path_pattern in input_paths[1:]:
            files = files.concatenate(tf.data.Dataset.list_files(path_pattern))

        dataset = files.repeat()
        dataset = dataset.shuffle(buffer_size=2 * count)

    elif a.mode == "test":

        input_paths = [path + "/rgb/" + a.mode + "/*.jpg" for path in input_paths]

        pathAndLight = []
        p_num = 0
        for path_pattern in input_paths:
            print(str(len(glob.glob(path_pattern))) + " from " + path_pattern)

            for path in glob.glob(path_pattern):
                for light in lights_list_path[p_num].split(","):
                    pathAndLight.append([os.path.normpath(path), light])
                    # pathAndLight.append([path,light])

            p_num += 1
        count = len(pathAndLight)
        # print(pathAndLight)

        fn = lambda x: load_data(x[0], None, None, False, x[1])
        dataset = tf.data.Dataset.from_tensor_slices(pathAndLight)
        # dataset = dataset.repeat()

    with tf.name_scope("load_images"):
        dataset = dataset.map(map_func=fn, num_parallel_calls=24)
        dataset = dataset.batch(batch_size=a.batch_size)
        dataset = dataset.prefetch(max(8, a.batch_size))
        iterator = dataset.make_initializable_iterator()

    steps_per_epoch = int(math.ceil(count / a.batch_size))

    return Examples(
        count=count,
        steps_per_epoch=steps_per_epoch,
        iterator=iterator
    )

class DatasetInitializerHook(tf.train.SessionRunHook):
    def __init__(self, iterator):
        self._iterator = iterator
    def begin(self):
        self._initializer = self._iterator.initializer
    def after_create_session(self, session, coord):
        del coord
        session.run(self._initializer)

def path_info(paths):
    # getting folder and file names
    if a.win :
        split_paths = tf.string_split([paths],delimiter="\\")
    else :
        split_paths = tf.string_split([paths],delimiter="/")
    name_im = split_paths.values[-1]

    split_name_im=tf.string_split([name_im],delimiter="_")

    name_cam = split_name_im.values[0]
    name_light = tf.string_split([split_name_im.values[1]],delimiter=".").values[0]

    name_main_dir = tf.reduce_join([split_paths.values[0:-3]],separator='/')
    if not a.win:
        name_main_dir = '/'+name_main_dir

    return PathInfo(
        name_main_dir=name_main_dir,
        name_cam=name_cam,
        name_light=name_light)

##### END LOADING
