# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import json
import math
import os
import random
import time

import numpy as np
import tensorflow as tf
from tensorflow.python.client import timeline

from dataLoading import DatasetInitializerHook, load_examples
from imgProc import preprocess, deprocess, deprocess_lab, lab_to_rgb
from models import create_model, create_generator
from options import a, CROP_SIZE

datasetPrefix = False

def save_images(fetches, step=None, onlyOutput=False, datasetPrefix=False):
    image_dir = os.path.join(a.output_dir, "images")
    if not os.path.exists(image_dir):
        os.makedirs(image_dir)

    filesets = []
    for i, in_path in enumerate(fetches["paths"]):
        name, _ = os.path.splitext(os.path.basename(in_path.decode("utf8")))
        fileset = {"name": name, "step": step}
        if a.seq :
            filename = name.split("_to_")[0] 
            if datasetPrefix :
                prefix = os.path.basename(os.path.dirname(os.path.dirname(os.path.dirname(in_path)))).decode("utf-8")
                filename = prefix + "/" + filename
            foldername = filename

            if not os.path.exists(os.path.join(image_dir,filename)):
                os.makedirs(os.path.join(image_dir,filename))

            filenameSM = filename +"/SM_" + name.split("_to_")[1] + ".png"
            filenameRefinedSM = filename +"/refinedSM_" + name.split("_to_")[1] + ".png"
            filename = filename +"/" + name.split("_to_")[1]
            
            filename = filename + ".png"
            fileset["outputs"] = foldername
            out_path = os.path.join(image_dir, filename)
            contents = fetches["outputs"][i]
            with open(out_path, "wb") as f:
                f.write(contents)

            out_path = os.path.join(image_dir, filenameSM)
            contents = fetches["targetSM"][i]
            with open(out_path, "wb") as f:
                f.write(contents)

            out_path = os.path.join(image_dir, filenameRefinedSM)
            contents = fetches["refinedSMTar"][i]
            with open(out_path, "wb") as f:
                f.write(contents)
            #Save input if does not exist
            if not os.path.exists(os.path.join(image_dir,foldername,"input.png")):
                out_path = os.path.join(image_dir,foldername,"input.png")
                contents = fetches["inputs"][i]
                with open(out_path, "wb") as f:
                    f.write(contents)
                out_path = os.path.join(image_dir,foldername,"sourceSM.png")
                contents = fetches["sourceSM"][i]
                with open(out_path, "wb") as f:
                    f.write(contents)
                out_path = os.path.join(image_dir,foldername,"refinedSMSce.png")
                contents = fetches["refinedSMSce"][i]
                with open(out_path, "wb") as f:
                    f.write(contents)
        else :
            for kind in ["outputs"]:
                if not onlyOutput or kind=="outputs":
                    filename = name + "-" + kind 
                    if datasetPrefix :
                        prefix = os.path.basename(os.path.dirname(os.path.dirname(os.path.dirname(in_path)))).decode("utf-8")
                        filename = prefix + "_" + filename
                    if step is not None:
                        filename = "%s-%08d" % (filename,step)
                    filename = filename + ".png"

                    fileset[kind] = filename
                    out_path = os.path.join(image_dir, filename)
                    contents = fetches[kind][i]
                    with open(out_path, "wb") as f:
                        f.write(contents)
        filesets.append(fileset)
    return filesets


def append_index(filesets, step=False):
    index_path = os.path.join(a.output_dir, "index.html")
    if os.path.exists(index_path):
        index = open(index_path, "a")
    else:
        index = open(index_path, "w")
        index.write("<html><body><table><tr>")
        if step:
            index.write("<th>step</th>")
        index.write("<th>name</th><th>input</th><th>output</th><th>source SM</th><th>target SM</th><th>refined SM sce</th><th>refined SM tar</th></tr>")

    for fileset in filesets:
        index.write("<tr>")

        if step:
            index.write("<td>%d</td>" % fileset["step"])
        

        if a.seq :
            index.write("<td>%s</td>" % fileset)
            index.write("<td><img src='images/%s'></td>" % (fileset+"/input.png"))
            index.write("<td><video  muted autoplay loop><source src='images/%s' type='video/mp4'></video></td>" % (fileset+"/vid.mp4"))
            index.write("<td><img src='images/%s'></td>" % (fileset+"/sourceSM.png"))
            index.write("<td><video  muted autoplay loop><source src='images/%s' type='video/mp4'></video></td>" % (fileset+"/vid_SM.mp4"))
            index.write("<td><img src='images/%s'></td>" % (fileset+"/refinedSMSce.png"))
            index.write("<td><video  muted autoplay loop><source src='images/%s' type='video/mp4'></video></td>" % (fileset+"/vid_refinedSM.mp4"))
        else:
            index.write("<td>%s</td>" % fileset["name"])
            for kind in ["outputs"]:
                index.write("<td><img src='images/%s'></td>" % fileset[kind])

        index.write("</tr>")
    return index_path

##### END SAVING


def main():
    if a.seed is None:
        a.seed = random.randint(0, 2**31 - 1)

    tf.set_random_seed(a.seed)
    np.random.seed(a.seed)
    random.seed(a.seed)

    if not os.path.exists(a.output_dir):
        os.makedirs(a.output_dir)

    if a.mode == "test" or a.mode == "export" or a.mode == "eval" or a.mode == "meval":
        if a.checkpoint is None:
            raise Exception("checkpoint required for test mode")

        # load some options from the checkpoint
        options = {"ngf", "ndf", "lab_colorization"}
        with open(os.path.join(a.checkpoint, "options.json")) as f:
            for key, val in json.loads(f.read()).items():
                if key in options:
                    print("loaded", key, "=", val)
                    setattr(a, key, val)
        # disable these features in test mode
        a.scale_size = CROP_SIZE
        a.flip = False

    for k, v in a._get_kwargs():
        print(k, "=", v)

    with open(os.path.join(a.output_dir, "options.json"), "w") as f:
        f.write(json.dumps(vars(a), sort_keys=True, indent=4))

    if a.mode == "export":
        # export the generator to a meta graph that can be imported later for standalone generation
        if a.lab_colorization:
            raise Exception("export not supported for lab_colorization")

        input = tf.placeholder(tf.string, shape=[1])
        input_data = tf.decode_base64(input[0])
        input_image = tf.image.decode_png(input_data)

        # remove alpha channel if present
        input_image = tf.cond(tf.equal(tf.shape(input_image)[2], 4), lambda: input_image[:,:,:3], lambda: input_image)
        # convert grayscale to RGB
        input_image = tf.cond(tf.equal(tf.shape(input_image)[2], 1), lambda: tf.image.grayscale_to_rgb(input_image), lambda: input_image)

        input_image = tf.image.convert_image_dtype(input_image, dtype=tf.float32)
        input_image.set_shape([CROP_SIZE, CROP_SIZE, 3])
        batch_input = tf.expand_dims(input_image, axis=0)

        with tf.variable_scope("generator"):
            batch_output = deprocess(create_generator(preprocess(batch_input), 3))

        output_image = tf.image.convert_image_dtype(batch_output, dtype=tf.uint8)[0]
        if a.output_filetype == "png":
            output_data = tf.image.encode_png(output_image)
        elif a.output_filetype == "jpeg":
            output_data = tf.image.encode_jpeg(output_image, quality=80)
        else:
            raise Exception("invalid filetype")
        output = tf.convert_to_tensor([tf.encode_base64(output_data)])

        key = tf.placeholder(tf.string, shape=[1])
        inputs = {
            "key": key.name,
            "input": input.name
        }
        tf.add_to_collection("inputs", json.dumps(inputs))
        outputs = {
            "key":  tf.identity(key).name,
            "output": output.name,
        }
        tf.add_to_collection("outputs", json.dumps(outputs))

        init_op = tf.global_variables_initializer()
        restore_saver = tf.train.Saver()
        export_saver = tf.train.Saver()

        with tf.Session() as sess:
            sess.run(init_op)
            print("loading model from checkpoint")
            checkpoint = tf.train.latest_checkpoint(a.checkpoint)
            restore_saver.restore(sess, checkpoint)
            print("exporting model")
            export_saver.export_meta_graph(filename=os.path.join(a.output_dir, "export.meta"))
            export_saver.save(sess, os.path.join(a.output_dir, "export"), write_meta_graph=False)

        return

    pathPrefix = "/local/mixed/jphilip/RelightingDatasets"
    if a.win:
        pathPrefix = "E:/Users/jphilip/Relighting/RelightingDatasetsMD"
        
    if a.mode == "test" :
        input_paths = [
        #pathPrefix+"/eilenrocULR",
        pathPrefix+"/eilenroc",
        pathPrefix+"/monastere_cloister",
        pathPrefix+"/monastere_place",
        pathPrefix+"/montAlban",
        pathPrefix+"/ruins",
        pathPrefix+"/russian_church",
        pathPrefix+"/SaintAnne",
        pathPrefix+"/arena",
        pathPrefix+"/crissy",
        pathPrefix+"/drone1",
        pathPrefix+"/color_house",
        pathPrefix+"/lighthouse",
        ]
    elif a.mode == "train" :
        input_paths = [
        pathPrefix+"/AE22-010",
        pathPrefix+"/AE22-009",
        pathPrefix+"/AE22-007",
        pathPrefix+"/AE22-005",
        pathPrefix+"/AE22-002",
        pathPrefix+"/AE17-005",
        pathPrefix+"/AE17-003",
        pathPrefix+"/Bistro-Place",
        pathPrefix+"/Bistro-Street",
        pathPrefix+"/Provence",
        ]

    examples = load_examples(input_paths)
    examples_paths,examples_input,examples_gt,examples_input_refinementSce,examples_input_refinementTar,examples_gt_refinementSce,examples_gt_refinementTar,examples_maskTarSMLoss = examples.iterator.get_next()
    print("examples count = %d" % examples.count)

    # inputs and targets are [batch_size, height, width, channels]
    model = create_model(
        examples_input,
        examples_gt,
        examples_input_refinementSce,
        examples_input_refinementTar,
        examples_gt_refinementSce,
        examples_gt_refinementTar,
        examples_maskTarSMLoss
        )

    outputs = model.outputs
    if a.lab:
        print(examples_input.get_shape())
        inputs = tf.concat([lab_to_rgb(deprocess_lab(examples_input[:,:,:,0:3])),deprocess(examples_input[:,:,:,3:16])],-1)
        gts = lab_to_rgb(deprocess_lab(examples_gt))
        outputs = lab_to_rgb(deprocess_lab(outputs))
        inputSMSce = lab_to_rgb(deprocess_lab(examples_input_refinementSce[:,:,:,3:6]))
        inputSMTar = lab_to_rgb(deprocess_lab(examples_input_refinementTar[:,:,:,3:6]))
    else:
        gts = deprocess(examples_gt)
        outputs = deprocess(outputs)
        inputSMSce = deprocess(examples_input_refinementSce[:,:,:,3:6])
        inputSMTar = deprocess(examples_input_refinementTar[:,:,:,3:6])
        inputs = deprocess(examples_input)

    gtSMSce = deprocess(examples_gt_refinementSce)
    gtSMTar = deprocess(examples_gt_refinementTar)

    refinedSMSce = deprocess(model.outputs_refinementSce)
    refinedSMTar = deprocess(model.outputs_refinementTar)

    def convert(image):
        if a.aspect_ratio != 1.0:
            # upscale to correct aspect ratio
            size = [CROP_SIZE, int(round(CROP_SIZE * a.aspect_ratio))]
            image = tf.image.resize_images(image, size=size, method=tf.image.ResizeMethod.BICUBIC)

        return tf.image.convert_image_dtype(image, dtype=tf.uint8, saturate=True)

    # reverse any processing on images so they can be written to disk or displayed to user
    with tf.name_scope("convert_inputs"):
        clippped_inputs_rgb = tf.clip_by_value(inputs[:,:,:,0:3], 0.0, 1.0)
        converted_inputs_rgb = convert(clippped_inputs_rgb)
        converted_inputs = convert(inputs)
        converted_inputsSMSce = convert(inputSMSce)
        converted_inputsSMTar = convert(inputSMTar)

    with tf.name_scope("convert_gts"):
        converted_gts = convert(tf.clip_by_value(gts[:,:,:,0:3], 0.0, 1.0))
        converted_gtsSMSce = convert(tf.clip_by_value(gtSMSce, 0.0, 1.0))
        converted_gtsSMTar = convert(tf.clip_by_value(gtSMTar, 0.0, 1.0))

    with tf.name_scope("convert_outputs"):
        converted_outputs = convert(tf.clip_by_value(outputs[:,:,:,0:3], 0.0, 1.0))
        converted_refinedSMSce = convert(tf.clip_by_value(refinedSMSce, 0.0, 1.0))
        converted_refinedSMTar = convert(tf.clip_by_value(refinedSMTar, 0.0, 1.0))

    if a.cloud:
        with tf.name_scope("convert_cloud"):
            converted_gts_cloud = convert(tf.clip_by_value(gts[:,:,:,3:6], 0.0, 1.0))
            converted_outputs_cloud = convert(tf.clip_by_value(outputs[:,:,:,3:6], 0.0, 1.0))

    with tf.name_scope("convert_diffss"):
        converted_diffs = convert(tf.clip_by_value(tf.scalar_mul(10,tf.abs(outputs[:,:,:,0:3]-gts[:,:,:,0:3])), 0.0, 1.0))

    with tf.name_scope("encode_images"):
        display_fetches = {
            "paths": examples_paths,
            "inputs": tf.map_fn(tf.image.encode_png, converted_inputs_rgb, dtype=tf.string, name="input_pngs"),
            "gts": tf.map_fn(tf.image.encode_png, converted_gts, dtype=tf.string, name="target_pngs"),
            "outputs": tf.map_fn(tf.image.encode_png, converted_outputs, dtype=tf.string, name="output_pngs"),
        }
        if a.mode == "test" :
            display_fetches["sourceSM"] =tf.map_fn(tf.image.encode_png, converted_inputsSMSce, dtype=tf.string, name="input_pngs");
            display_fetches["targetSM"] =tf.map_fn(tf.image.encode_png, converted_inputsSMTar, dtype=tf.string, name="input_pngs");
            display_fetches["refinedSMSce"] =tf.map_fn(tf.image.encode_png, converted_refinedSMSce, dtype=tf.string, name="input_pngs");
            display_fetches["refinedSMTar"] =tf.map_fn(tf.image.encode_png, converted_refinedSMTar, dtype=tf.string, name="input_pngs");


    # summaries

    with tf.name_scope("inputs_normal_summary"):
        tf.summary.image("inputs", converted_inputs[:,:,:,3:6])

    with tf.name_scope("inputs_sunDir_sce_summary"):
        tf.summary.image("inputs", converted_inputs[:,:,:,6:9])

    with tf.name_scope("inputs_sunDir_tar_summary"):
        tf.summary.image("inputs", converted_inputs[:,:,:,9:12])

    with tf.name_scope("inputs_angle_sce_summary"):
        tf.summary.image("inputs", converted_inputs[:,:,:,12:13])

    with tf.name_scope("inputs_angle_tar_summary"):
        tf.summary.image("inputs", converted_inputs[:,:,:,13:14])

    with tf.name_scope("inputs_elevation_sce_summary"):
        tf.summary.image("inputs", converted_inputs[:,:,:,14:15])

    with tf.name_scope("inputs_elevation_tar_summary"):
        tf.summary.image("inputs", converted_inputs[:,:,:,15:16])

    with tf.name_scope("mask_tar_loss"):
        tf.summary.image("mask tar loss", examples_maskTarSMLoss)

    with tf.name_scope("shadowMap_sce_summary"):
        tf.summary.image("000_inputs", converted_inputsSMSce,family="smSce")
        tf.summary.image("001_refined", converted_refinedSMSce,family="smSce")
        tf.summary.image("002_gts", converted_gtsSMSce,family="smSce")

    with tf.name_scope("shadowMap_tar_summary"):
        tf.summary.image("000_inputs", converted_inputsSMTar,family="smTar")
        tf.summary.image("001_refined", converted_refinedSMTar,family="smTar")
        tf.summary.image("002_gts", converted_gtsSMTar,family="smTar")

    with tf.name_scope("rgb_summary"):
        tf.summary.image("000_inputs", converted_inputs_rgb,family="rgb")
        tf.summary.image("001_outputs", converted_outputs,family="rgb")
        tf.summary.image("002_gts", converted_gts,family="rgb")
        if a.cloud:
            tf.summary.image("003_outputs_cloud", converted_outputs_cloud,family="rgb")
            tf.summary.image("004_gts_cloud", converted_gts_cloud,family="rgb")

    with tf.name_scope("diff_summary"):
        tf.summary.image("diff", converted_diffs )

    tf.summary.scalar("generator_loss", model.losses["gen_loss"])
    tf.summary.scalar("perceptual_loss", model.losses["perceptual_loss"])
    tf.summary.scalar("smSce_loss", model.losses["smSce_loss"])
    tf.summary.scalar("smTar_loss", model.losses["smTar_loss"])
    if a.ganTar :
       tf.summary.scalar("smTar_L1_loss", model.losses["smTar_L1_loss"])
       tf.summary.scalar("smTar_GAN_loss", model.losses["smTar_GAN_loss"])
       tf.summary.scalar("dis_loss", model.losses["dis_loss"])

    for var in tf.trainable_variables():
        tf.summary.histogram(var.op.name + "/values", var)

    for grad, var in model.gen_grads_and_vars:
        tf.summary.histogram(var.op.name + "/gradients", grad)

    with tf.name_scope("parameter_count"):
        parameter_count = tf.reduce_sum([tf.reduce_prod(tf.shape(v)) for v in tf.trainable_variables()])

    summary_op = tf.summary.merge_all();

    saver = tf.train.Saver(max_to_keep=1)

    logdir = a.output_dir if (a.trace_freq > 0 or a.summary_freq > 0) else None
    writer = tf.summary.FileWriter(logdir)

    ckpt = tf.train.get_checkpoint_state(a.checkpoint)
    global_step_init = 0
    global_step = tf.train.get_or_create_global_step()
    if ckpt and ckpt.model_checkpoint_path:
        # This is only for the logger (e.g., it is not responsible for saving).
        global_step_init = int(ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1])
  

    initializer_hook = DatasetInitializerHook(examples.iterator)

    saver_hook = tf.train.CheckpointSaverHook(checkpoint_dir=logdir,
                                         saver=saver,
                                         save_steps=a.save_freq)

    #This is done to avoid saving the ckpt-0 model and the new model loaded
    #We redefine the after_create_session function so that it does not save the ckpt-0 model for nothing
    def after_create_session_void(session, coord):
        pass
    saver_hook.after_create_session = after_create_session_void
    #We update the last triggered to correspond to the global starting step
    saver_hook._timer.update_last_triggered_step(global_step_init)

    summary_hook = tf.train.SummarySaverHook(output_dir=logdir,
                                       summary_op=summary_op,
                                           save_steps=a.summary_freq)
    config = tf.ConfigProto(log_device_placement=False,allow_soft_placement=False)
    config.gpu_options.allow_growth=True

    if a.mode=="train":
        hooks =[saver_hook,summary_hook, initializer_hook]
    else :
        hooks =[initializer_hook]

    with tf.train.MonitoredTrainingSession(hooks=hooks,config=config) as sess:

        if ckpt and global_step_init >0 and ckpt.model_checkpoint_path:
            print("loading model from checkpoint "+ ckpt.model_checkpoint_path)
            saver.restore(sess, ckpt.model_checkpoint_path)

        #print("parameter_count =", sess.run(parameter_count))

        max_steps = 2**32
        if a.max_epochs is not None:
            max_steps = global_step_init+examples.steps_per_epoch * a.max_epochs
        if a.max_steps is not None:
            max_steps = a.max_steps

        if a.mode == "test" or a.mode == "eval":
            # testing
            # at most, process the test data once

            start = time.time()
            max_steps = min(examples.steps_per_epoch, max_steps)
            folders=[]
            try:
                while True:
                    results = sess.run(display_fetches)
                    filesets = save_images(results,datasetPrefix=datasetPrefix)
                    for i, f in enumerate(filesets):
                        print("evaluated image", f["name"])
                    if not a.seq :
                        index_path = append_index(filesets)
                    else:
                        for fileset in filesets :
                            folders.append(fileset["outputs"])

            except tf.errors.OutOfRangeError:
                pass
            if a.seq :
                folders =sorted(set(folders))
                for folder in folders :
                    os.system("ffmpeg -y -r 12 -i "+a.output_dir+"/images/"+folder+"/%08d.png -pix_fmt yuv420p -vcodec h264 -y -vf \"pad=ceil(iw/2)*2:ceil(ih/2)*2\" "+a.output_dir+"/images/"+folder+"/vid.mp4")
                    os.system("ffmpeg -y -r 12 -i "+a.output_dir+"/images/"+folder+"/SM_%08d.png -pix_fmt yuv420p -vcodec h264 -y -vf \"pad=ceil(iw/2)*2:ceil(ih/2)*2\" "+a.output_dir+"/images/"+folder+"/vid_SM.mp4")
                    os.system("ffmpeg -y -r 12 -i "+a.output_dir+"/images/"+folder+"/refinedSM_%08d.png -pix_fmt yuv420p -vcodec h264 -y -vf \"pad=ceil(iw/2)*2:ceil(ih/2)*2\" "+a.output_dir+"/images/"+folder+"/vid_refinedSM.mp4")
                index_path = append_index(folders)
            else:
                print("wrote index at", index_path)
            print("rate", (time.time() - start) / max_steps)
        else:
            # training
            start = time.time()

            for step in range(global_step_init,max_steps):

                #start_step = time.time()
                def should(freq):
                    return freq > 0 and ((step + 1) % freq == 0 or step == max_steps - 1)

                options = None
                run_metadata = None
                if should(a.trace_freq):
                    options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                    run_metadata = tf.RunMetadata()

                fetches = {
                    "train": model.train,
                    "global_step":  global_step,
                }

                if should(a.progress_freq):
                    fetches["gen_loss"] = model.losses["gen_loss"]

                #if should(a.summary_freq):
                #   fetches["summary"] = sv.summary_op

                if should(a.display_freq):
                    fetches["display"] = display_fetches

                #start_run = time.time()

                results = sess.run(fetches, options=options, run_metadata=run_metadata)

                #end_run = time.time()

                if should(a.display_freq):
                    print("saving display images")
                    filesets = save_images(results["display"], step=results["global_step"])
                    append_index(filesets, step=True)

                if should(a.trace_freq):
                    print("recording trace")
                    writer.add_run_metadata(run_metadata, "step_%d" % results["global_step"])
                    # Create the Timeline object, and write it to a json file
                    fetched_timeline = timeline.Timeline(run_metadata.step_stats)
                    chrome_trace = fetched_timeline.generate_chrome_trace_format()
                    with open(a.output_dir+"\\timeline_"+str(results["global_step"])+".json", 'w') as f:
                        f.write(chrome_trace)

                if should(a.progress_freq):
                    # global_step will have the correct step count if we resume from a checkpoint
                    train_epoch = math.ceil(results["global_step"] / examples.steps_per_epoch)
                    train_step = (results["global_step"] - 1) % examples.steps_per_epoch + 1
                    rate = ((step-global_step_init) + 1) * a.batch_size / (time.time() - start)
                    remaining = (max_steps - step) * a.batch_size / rate
                    print("progress  epoch %d  step %d  image/sec %0.1f  remaining %dm" % (train_epoch, train_step, rate, remaining / 60))
                    print("gen_loss", results["gen_loss"])

                if should(a.save_freq):
                    print("saving model")
                #    saver.save(sess, os.path.join(a.output_dir, "model"), global_step=fetches["global_step"])

                if sess.should_stop():
                    break

                #end_step = time.time()
                #print("st t %d, st r %d, end r %d, end t %d" % (start_step, 1000.0*(start_run-start_step), 1000.0*(end_run-start_run), 1000.0*(end_step-end_run)))


main()
