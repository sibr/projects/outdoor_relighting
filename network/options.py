# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


import argparse

EPS = 1e-12
IN_SIZE = [555,725]
SCALE_SIZE = 286
CROP_SIZE = 256

parser = argparse.ArgumentParser()
parser.add_argument("--input_dir", help="path to folder containing images")
parser.add_argument("--mode", required=True, choices=["train", "test", "export", "eval", "meval"])
parser.add_argument("--mevalNum", type=int, default=5, help="number of times to apply the network to the previous result in meval mode")
parser.add_argument("--output_dir", required=True, help="where to put output files")
parser.add_argument("--seed", type=int)
parser.add_argument("--checkpoint", default=None, help="directory with checkpoint to resume training from or use for testing")
parser.add_argument("--gamma_input", type=float, default=1.0, help="gamma to apply to input_images, default correspond to linear images")

parser.add_argument("--max_steps", type=int, help="number of training steps (0 to disable)")
parser.add_argument("--max_epochs", type=int, help="number of training epochs")
parser.add_argument("--summary_freq", type=int, default=100, help="update summaries every summary_freq steps")
parser.add_argument("--progress_freq", type=int, default=50, help="display progress every progress_freq steps")
parser.add_argument("--trace_freq", type=int, default=0, help="trace execution every trace_freq steps")
parser.add_argument("--display_freq", type=int, default=0, help="write current training images every display_freq steps")
parser.add_argument("--save_freq", type=int, default=5000, help="save model every save_freq steps, 0 to disable")

parser.add_argument("--arch", required=True, choices=["unet", "resnet"])
parser.add_argument("--aspect_ratio", type=float, default=1.0, help="aspect ratio of output images (width/height)")
parser.add_argument("--lab",  dest="lab", action="store_true", help="split input image into brightness (A) and color (B)")
parser.set_defaults(lab=False)
parser.add_argument("--batch_size", type=int, default=1, help="number of images in batch")
parser.add_argument("--ngf", type=int, default=64, help="number of generator filters in first conv layer")
parser.add_argument("--flip", dest="flip", action="store_true", help="flip images horizontally")
parser.add_argument("--no_flip", dest="flip", action="store_false", help="don't flip images horizontally")
parser.set_defaults(flip=True)
parser.add_argument("--lr", type=float, default=0.0002, help="initial learning rate for adam")
parser.add_argument("--beta1", type=float, default=0.5, help="momentum term of adam")
parser.add_argument("--l1_weight", type=float, default=100.0, help="weight on L1 term for generator gradient")
parser.add_argument("--cx_weight", type=float, default=2.0, help="weight on contextual term for generator gradient")
parser.set_defaults(seq=False)
parser.add_argument("--seq", dest="seq", action="store_true", help="output sequences")
parser.set_defaults(plTar=False)
parser.add_argument("--plTar", dest="plTar", action="store_true", help="use perceptual loss for the target shadowmask")
parser.set_defaults(tanh=False)
parser.add_argument("--tanh", dest="tanh", action="store_true", help="Use tanh at the end of the network.")
parser.set_defaults(win=False)
parser.add_argument("--win", dest="win", action="store_true", help="for windows")
parser.set_defaults(cloud=False)
parser.add_argument("--cloud", dest="cloud", action="store_true", help="Add the cloudiness layer")

# export options
parser.add_argument("--output_filetype", default="png", choices=["png", "jpeg"])
a = parser.parse_args()
