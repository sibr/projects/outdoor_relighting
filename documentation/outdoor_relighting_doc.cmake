set(PROJECT_PAGE "outdoorRelightingPage")
set(PROJECT_LINK "https://gitlab.inria.fr/sibr/projects/outdoor_relighting")
set(PROJECT_DESCRIPTION "Multi-view Relighting Using a Geometry-Aware Network; paper reference (https://www-sop.inria.fr/reves/Basilic/2019/PGZED19/) ")
set(PROJECT_TYPE "OURS")
