# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


__version__ = "3.0"

from meshroom.core import desc
import os

class OutdoorRelighting(desc.CommandLineNode):
    commandLine = 'sibr_outdoor_relighting_app_rwdi {allParams}'

    print(os.path.abspath('.'))
    cpu = desc.Level.INTENSIVE
    ram = desc.Level.INTENSIVE

    inputs = [
        desc.ListAttribute(
            elementDesc = desc.File(
                name = "path",
                label = "Cache folder",
                description = "",
                value = "",
                uid=[0],
            ),
            name='path',
            label='Input Folder',
            description='MeshroomCache folder containing the StructureFromMotion folder, PrepareDenseScene folder, and Texturing folder.'
        ),
        desc.ChoiceParam(
            name='texture-width',
            label='Texture Width',
            description='''Output texture size''',
            value=1024,
            values=(320, 640, 1024, 2560, 3840),
            exclusive=True,
            uid=[0],
        ),
        desc.ListAttribute(
            elementDesc=desc.File(
                name="model",
                label="Models folder",
                description="",
                value="",
                uid=[0],
            ),
            name='model',
            label='Models Folder',
            description='Outdoor Relighting Models folder containing the layout.txt and model.pb file.'
        ),
        desc.ChoiceParam(
            name='outW',
            label='Rendering Width',
            description='''Output render width''',
            value=1024,
            values=(320, 640, 1024, 2560, 3840),
            exclusive=True,
            uid=[0],
        ),
        desc.ChoiceParam(
            name='outH',
            label='Rendering Height',
            description='''Output render height''',
            value=768,
            values=(180, 360, 768, 1440, 2160),
            exclusive=True,
            uid=[0],
        ),
        desc.BoolParam(
            name="cloud",
            label="Cloudiness parameters",
            description="Choose whether to include slider for cloudiness or not?",
            value=True,
            uid=[0],
        ),
        desc.BoolParam(
            name="interactive",
            label="Interactive UI",
            description="Choose whether to include interactive UI or not?",
            value=True,
            uid=[0],
        ),
        desc.BoolParam(
            name="useBlur",
            label="Use Blur",
            description="Choose whether to use blur channel or not?",
            value=True,
            uid=[0],
        )
    ]

    outputs = [
        desc.File(
            name='RelitImagesFolder',
            label='Relit Images Folder',
            description='Folder for images exported after relighting.',
            value=desc.Node.internalFolder + "/RelitImages/",
            uid=[],
        ),
    ]
