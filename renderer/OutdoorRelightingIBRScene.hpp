/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef OUTDOORRELIGHTINGIBRSCENE_H
#define OUTDOORRELIGHTINGIBRSCENE_H

#include <core/scene/BasicIBRScene.hpp>
#include <core/graphics/Window.hpp>
#include "Config.hpp"
#include "OutdoorRelightingDataGen.hpp"


namespace sibr
{

	class SIBR_OUTDOORRELIGHTING_EXPORT OutdoorRelightingIBRScene : public sibr::BasicIBRScene
	{


	public:
		typedef std::shared_ptr<OutdoorRelightingIBRScene>									Ptr;
		OutdoorRelightingIBRScene(OutdoorRelightingAppArgs & myArgs, sibr::Window::Ptr window, bool noRTs = false);
		~OutdoorRelightingIBRScene(void);
		sibr::Vector3f _zenith;
		sibr::Vector3f _inputSunDir;
		sibr::Vector3f _north;
		int _width;
		int _height;

	protected:
		void parseDirOutdoorRelighting(OutdoorRelightingAppArgs & myArgs, sibr::Window::Ptr window);

	};

}

#endif