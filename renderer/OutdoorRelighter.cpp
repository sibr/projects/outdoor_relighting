/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#define _DISABLE_EXTENDED_ALIGNED_STORAGE 
#include "OutdoorRelighter.hpp"
#include "projects/ulr/renderer/ULRV3View.hpp"
#include "projects/tfgl_interop/renderer/tfgl_interop.h"
#include "core/view/InteractiveCameraHandler.hpp"
#include "core/view/SceneDebugView.hpp"

#include <imgui/imgui.h>

namespace sibr {

	OutdoorRelighter::~OutdoorRelighter(void) {};

	OutdoorRelighter::OutdoorRelighter(
		sibr::Window::Ptr window,
		int w,
		int h,
		std::string modelPath,
		const std::vector<sibr::InputCamera::Ptr>& cams,
		const sibr::Mesh::Ptr& proxy,
		const std::vector<sibr::ImageRGB::Ptr>& images,
		sibr::Vector3f& inputSunDir,
		sibr::Vector3f& zenith,
		sibr::Vector3f& north) :
		_width(w),
		_height(h),
		_cams(cams),
		_inputSunDir(inputSunDir),
		_zenith(zenith),
		_north(north),
		_NR(w, h, false, true, true),
		_LDARin(w, h, true),
		_LDARout(w, h, true),
		_proxy(proxy),
		_images(images)
	{

		float elevationIn = std::min(1.0, std::max(0.0, (M_PI_2 - acos(inputSunDir.dot(zenith))) / M_PI_2));

		_inputImageTex = std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height));
		_sunDirInTex = std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height));
		_sunDirOutTex = std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height));
		_elevationInTex = std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height, sibr::Vector4f(elevationIn, elevationIn, elevationIn, 1.0f)));
		_elevationOutTex = std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height));
		_SMRGBSceTex = std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height));
		_SMRGBTarTex = std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height));
		_SMDSceTex = std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height));
		_SMDTarTex = std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height));

		_SMATarTex = std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height));

		_depthMap = sibr::ImageFloat1::Ptr(new sibr::ImageFloat1(_width, _height));
		_RGBSMap = sibr::ImageFloat4(_width, _height, sibr::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
		_ratioMap = sibr::ImageFloat4(_width, _height, sibr::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
		_areaMap = sibr::ImageFloat4(_width, _height, sibr::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));

		_refinedSMSce = sibr::ImageRGBA32F::Ptr(new sibr::ImageRGBA32F(_width, _height));

		//Render the depth maps
		for (int c = 0; c < cams.size(); c++) {
			sibr::ImageFloat1::Ptr d(new sibr::ImageFloat1(_images[c]->w(), _images[c]->h()));
			sibr::DepthRenderer DR(_images[c]->w(), _images[c]->h());
			DR.render(*cams[c], *_proxy);
			DR._depth_RT->readBack(*d);
			_dm.push_back(d);
		}

		//Fill the input_textures
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_inputImageTex)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_NR._normal_RT_32F)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_sunDirInTex)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_sunDirOutTex)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_LDARin._lightDirAlign_RT_32F)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_LDARout._lightDirAlign_RT_32F)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_elevationInTex)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_elevationOutTex)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_SMRGBSceTex)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_SMRGBTarTex)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_SMDSceTex)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_SMDTarTex)));
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_SMATarTex)));

		//Setup the raycaster
		_raycaster.init();
		_raycaster.addMesh(*_proxy);

		////Setup tensorflow architecture
		_input_graph_node_name = { "inputSMSce", "inputSMTar", "inputRGB" };
		loadLayout(modelPath + "/layout.txt");
		_graph_path = modelPath + "/model.pb";
		_output_node_names = { "generator/relit:0" , "generator/cloudy:0"  , "generator/smSce:0", "generator/smTar:0" };

		//_output_node_names = { "generator/concat_1:0","generator/concat_2:0" };

		for (int ot = 0; ot < _output_node_names.size(); ot++) {
			_output_textures.push_back(std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(_width, _height, sibr::Vector4f(1.0f, 0.5f, 0.7f, 1.0f)), SIBR_CLAMP_UVS));
			_run_node_names.push_back("output" + std::to_string(ot));
		}

		std::vector<float> triAreas;
		for (auto t : _proxy->triangles()) {

			sibr::Vector3f v0 = _proxy->vertices()[t[0]];
			sibr::Vector3f v1 = _proxy->vertices()[t[1]];
			sibr::Vector3f v2 = _proxy->vertices()[t[2]];
			float area = (v1 - v0).cross(v2 - v0).norm() / 2;
			triAreas.push_back(area);

		}
		std::sort(triAreas.begin(), triAreas.end());
		_medianArea = triAreas[triAreas.size() / 2];

		lastImId = -1;


		//// Setup tf_gl linker
		_network = std::make_shared<sibr::TF_GL>(
			_graph_path,
			_input_textures,
			_input_texture_mapping,
			_input_texture_channels,
			_input_graph_node_name,
			_output_textures,
			_output_node_names,
			window);
		////

	};

	void OutdoorRelighter::renderRGBDSMap8bitTar(
		sibr::Vector3f sunDir,
		const sibr::InputCamera& cam,
		const sibr::ImageRGB::Ptr currentImg,
		bool improvedTarSM,
		sibr::Texture2DRGBA32F::Ptr SMRGBTex,
		sibr::Texture2DRGBA32F::Ptr SMDTex,
		sibr::Texture2DRGBA32F::Ptr SMATex,
		float thrld) {

		float dynamic = 1.0f;// 1.0 / 20.f;
		auto start = std::chrono::high_resolution_clock::now();


		sibr::Vector3f dx, dy, upLeftOffset;
		sibr::CameraRaycaster::computePixelDerivatives(cam, dx, dy, upLeftOffset);

		std::vector<std::pair<float, int>> bestCouple;
		std::vector<int> bestIm;
		for (int imIt = 0; imIt < _cams.size(); imIt++) {

			if (_cams[imIt]->dir().dot(sunDir) > 0) {
				float angleRay = acos(std::min(1.0f, std::max(-1.0f, sunDir.dot(_cams[imIt]->dir()))));
				float weightOrientation = 1 / (pow(sin(angleRay / 2), 2.0f) + 0.01) - 0.98;
				bestCouple.push_back(std::make_pair(-weightOrientation, imIt));
			}
		}
		std::sort(bestCouple.begin(), bestCouple.end());
		for (int i = 0; i < std::min(std::size_t(20), bestCouple.size()); i++) {
			bestIm.push_back(bestCouple[i].second);
		}

		std::vector<sibr::Vector3f> nfPrecom;
		for (int im = 0; im < _cams.size(); im++) {

			//std::cout << im << " " <<_cams[im].w() << " & "<< _cams[im].h() << " - " << _images[im]->w() << " & " << _images[im]->h() << std::endl;

			nfPrecom.push_back(
				sibr::Vector3f(
					(2.0 * _cams[im]->znear() * _cams[im]->zfar()),
					_cams[im]->zfar() + _cams[im]->znear(),
					(_cams[im]->zfar() - _cams[im]->znear())
				)
			);
		}


		std::chrono::duration<double, std::milli> duration =
			std::chrono::high_resolution_clock::now() - start;
		//std::cout << "Setup 1 ran in " << duration.count() << " ms" << std::endl;
		start = std::chrono::high_resolution_clock::now();

#pragma omp parallel for
		for (int j = 0; j < _height; j++) {
			for (int i = 0; i < _width; i += 8) {

				float pixelVal = 1.0f;

				float jj = j * cam.h() / (float)_height;

				std::array<sibr::Ray, 8> rays;

				for (int i_r = 0; i_r < 8; i_r++) {
					_RGBSMap(i + i_r, j) = sibr::Vector4f(1.0, 1.0, 1.0, 1.0);
					_ratioMap(i + i_r, j) = sibr::Vector4f(dynamic * 20.0, dynamic * 20.0, dynamic * 20.0, 1.0);
					_areaMap(i + i_r, j) = sibr::Vector4f(dynamic * 20.0, dynamic * 20.0, dynamic * 20.0, 1.0);

					float ii = (i + i_r) * cam.w() / (float)_width;
					sibr::Vector3f worldPos = ((float)ii + 0.5f) * dx + ((float)jj + 0.5f) * dy + upLeftOffset;
					sibr::Vector3f dir = (worldPos - cam.position()).normalized();

					rays[i_r] = sibr::Ray(sibr::Ray(cam.position(), dir));

				}

				std::array<sibr::RayHit, 8> rayhits = _raycaster.intersect8(rays);

				for (int i_r = 0; i_r < 8; i_r++) {
					if (!(i + i_r < _width) || !rayhits[i_r].hitSomething()) { continue; }

					const sibr::Vector3f& dir = rayhits[i_r].ray().dir();
					sibr::Vector3f hitPos = cam.position() + dir * rayhits[i_r].dist();

					sibr::Vector3f normal = -rayhits[i_r].normal().normalized();

					if (normal.dot(dir) > 0) {
						normal = -normal;
					}

					sibr::Vector3f pos3D;

					float ratio = -1.0;
					if (normal.dot(sunDir) < -0.01f) {

						pos3D = sibr::Vector3f(hitPos.x(), hitPos.y(), hitPos.z());
						ratio = 0;

						// Compute area of triangle
						sibr::Vector3u tri = _proxy->triangles()[rayhits[i_r].primitive().triID];
						sibr::Vector3f v0 = _proxy->vertices()[tri[0]];
						sibr::Vector3f v1 = _proxy->vertices()[tri[1]];
						sibr::Vector3f v2 = _proxy->vertices()[tri[2]];
						float area = (v1 - v0).cross(v2 - v0).norm() / 2;
						_areaMap(i + i_r, j).x() = std::min(dynamic * area / _medianArea, dynamic * 20.0f);
						//
					}
					else {
						hitPos += 0.002f * normal;

						sibr::RayHit hitSun = _raycaster.intersect(sibr::Ray(hitPos, sunDir));

						if (!hitSun.hitSomething()) { continue; }

						sibr::Vector3f hitPosSun = hitPos + sunDir * hitSun.dist();

						// Compute area of triangle
						sibr::Vector3u tri = _proxy->triangles()[hitSun.primitive().triID];
						sibr::Vector3f v0 = _proxy->vertices()[tri[0]];
						sibr::Vector3f v1 = _proxy->vertices()[tri[1]];
						sibr::Vector3f v2 = _proxy->vertices()[tri[2]];
						float area = (v1 - v0).cross(v2 - v0).norm() / 2;
						_areaMap(i + i_r, j).x() = std::min(dynamic * area / _medianArea, dynamic * 20.0f);
						//


						pos3D = sibr::Vector3f(hitPosSun.x(), hitPosSun.y(), hitPosSun.z());
						ratio = hitSun.dist() / rayhits[i_r].dist();
					}

					_ratioMap(i + i_r, j).x() = std::min(dynamic * ratio, dynamic * 20.f);

					if (ratio >= 0) {

						///// Punch Hole
						if (improvedTarSM && _inputSunDir.dot(sunDir) > 0.7) {
							bool hitSmthg = true;
							sibr::Vector3f startRay = pos3D;
							sibr::RayHit hitInputSun;
							int count = 0;
							while (hitSmthg && count == 0) {
								count++;
								hitInputSun = _raycaster.intersect(sibr::Ray(startRay, -_inputSunDir), 0.001f);
								hitSmthg = hitInputSun.hitSomething();
								if (hitSmthg)
									startRay = startRay - _inputSunDir * hitInputSun.dist();
							}

							if (startRay != pos3D) {
								sibr::Vector2f pos2Sample = cam.projectImgSpaceInvertY(startRay).xy();
								sibr::Vector2i imCoordISample(sibr::round(pos2Sample.x() * _refinedSMSce->w() / cam.w()), sibr::round(pos2Sample.y() * _refinedSMSce->h() / cam.h()));

								if (_refinedSMSce->isInRange(imCoordISample)) {

									_refinedSMSce(imCoordISample).y() = 1.0;

									sibr::Vector3f posProxy = cam.unprojectImgSpaceInvertY(pos2Sample.cast<int>(), 2.0f * _depthMap(imCoordISample).x() - 1.0f);
									//std::cout << posProxy << " & " << startRay <<" & " << (posProxy - startRay).norm() << std::endl;

									if ((posProxy - startRay).norm() < thrld * pow(_inputSunDir.dot(sunDir), 5)) {

										float val = _refinedSMSce(imCoordISample).x();
										if (val > 0.2) {

											//std::cout << "Punching hole" << std::endl;
											_RGBSMap(i + i_r, j) = sibr::Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
											_ratioMap(i + i_r, j).x() = dynamic * 20.f;
											_areaMap(i + i_r, j).x() = dynamic * 20.f;
											continue;
										}
									}
								}

							}
						}

						///// END PUNCH HOLE

						sibr::Vector3f RGBAccumulator(0, 0, 0);
						float countAccumulator = 0;


						if (ratio < 0.001) {
							countAccumulator += 255.0f;
							RGBAccumulator += currentImg((i + i_r) * currentImg->w() / _width, j * currentImg->h() / _height).cast<float>();
						}
						else {

							int bestIm = -1;
							sibr::Vector2i bestSecondPosImInt(0, 0);
							chooseSample(_cams, pos3D, nfPrecom, sibr::Ray(hitPos, sunDir), bestIm, bestSecondPosImInt);

							if (bestIm > 0) {

								countAccumulator += 255.0f;
								RGBAccumulator += _images[bestIm](bestSecondPosImInt.x(), bestSecondPosImInt.y()).cast<float>();
							}
							/*
														for (int imIt : bestIm) {

															if (!_cams[imIt].frustumTest(pos3D)) {
																continue;
															}

															float ratioW = float(_cams[imIt].w()) / _images[imIt]->w();
															float ratioH = float(_cams[imIt].h()) / _images[imIt]->h();

															sibr::Vector2f imCoord = _cams[imIt].projectImgSpaceInvertY(pos3D).xy();
															sibr::Vector2i imCoordI(sibr::round(imCoord.x() / ratioW), sibr::round(imCoord.y() / ratioH));
															// If the ray object caster (which is parallel to sunDir) is colinear with the ray cam<->occluder
															// we minimize the error in reprojection with respect to the error in the mesh.
															// We decide to weight by this term to gett more accurate rgbSM

															float angleRay = acos(std::min(1.0f, std::max(-1.0f, sunDir.dot((pos3D - _cams[imIt].position()).normalized()))));
															float weightOrientation = 1 / (pow(sin(angleRay / 2), 2.0f) + 0.01) - 0.98;

															if (_images[imIt]->isInRange(imCoordI)) {

																const float d = _dm[imIt](imCoordI.x(), imCoordI.y()).x();

																sibr::Vector3f pos3Ddm = cam.unprojectImgSpaceInvertY(imCoordI, d);

																float weightPoint = weightOrientation / ((pos3D - pos3Ddm).squaredNorm() + 0.00001f);
																countAccumulator += 255.0f * weightPoint;
																RGBAccumulator += weightPoint * _images[imIt](imCoordI.x(), imCoordI.y()).cast<float>();

															}
														}*/
						}

						if (countAccumulator > 0) {
							_RGBSMap(i + i_r, j).x() = (RGBAccumulator.x() / countAccumulator);
							_RGBSMap(i + i_r, j).y() = (RGBAccumulator.y() / countAccumulator);
							_RGBSMap(i + i_r, j).z() = (RGBAccumulator.z() / countAccumulator);
							_RGBSMap(i + i_r, j).w() = 1.0f;
						}
						else {
							if (ratio >= 0) {
								_RGBSMap(i + i_r, j) = sibr::Vector4f(0, 0, 0, 1.0f);
							}
						}

					}
				}

			}
		}

		duration =
			std::chrono::high_resolution_clock::now() - start;
		//std::cout << "Setup 2 ran in " << duration.count() << " ms" << std::endl;
		start = std::chrono::high_resolution_clock::now();

		_RGBSMap.flipH();
		_ratioMap.flipH();
		_areaMap.flipH();

		if (SMATex)
			SMATex->update(_areaMap);
		SMRGBTex->update(_RGBSMap);
		SMDTex->update(_ratioMap);

		duration =
			std::chrono::high_resolution_clock::now() - start;
		//std::cout << "Setup 3 ran in " << duration.count() << " ms" << std::endl;
		start = std::chrono::high_resolution_clock::now();
		//show(*refinedSMSce);

	}

	void OutdoorRelighter::generateInputBuffers(const sibr::InputCamera& cam, const sibr::ImageRGB::Ptr inputColor, bool imageChanged, bool tarChanged, bool improvedTarSM, sibr::Vector3f sunDir, float thresholdTar) {

		if (imageChanged) {
			sibr::ImageRGBA32F::Ptr imRGBA(new sibr::ImageRGBA32F(inputColor->w(), inputColor->h()));
			if (inputColor) {
				const cv::Mat& image = inputColor->toOpenCV();
				const float INV_255 = 1.f / 255.f;
				for (int y = 0; y < imRGBA->h(); ++y) {
					for (int x = 0; x < imRGBA->w(); ++x) {
						cv::Vec3b c = image.at<cv::Vec3b>(y, x);
						imRGBA(x, y) = sibr::Vector4f(c[0] * INV_255, c[1] * INV_255, c[2] * INV_255, 1.f);
					}
				}

			}
			sibr::ImageRGBA32F resizedIm = imRGBA->resized(_width, _height, cv::INTER_AREA);
			resizedIm.flipH();
			_inputImageTex->update(resizedIm);

			//NR.setWH(outSize.x(), outSize.y());
			_NR.render(cam, *_proxy);
			_NR._normal_RT_32F->readBackDepth(*_depthMap);
			_depthMap->flipH();
			//showFloat(*_depthMap);

			renderRGBDSMap8bitTar(_inputSunDir, cam, inputColor, false, _SMRGBSceTex, _SMDSceTex, nullptr);

			_LDARin.render(cam, *_proxy, _inputSunDir);

			sibr::Vector3f camInputSunDir = (cam.view() * sibr::Vector4f(_inputSunDir.x(), _inputSunDir.y(), _inputSunDir.z(), 0.0)).xyz().normalized();
			_sunDirInTex->update(sibr::ImageRGBA32F(_width, _height, sibr::Vector4f(0.5f * (camInputSunDir.x() + 1.0f), 0.5f * (camInputSunDir.y() + 1.0f), 0.5f * (camInputSunDir.z() + 1.0f), 1.0f)));
		}
		//Render depth for RGBDSM
		if (imageChanged || tarChanged) {
			renderRGBDSMap8bitTar(sunDir, cam, inputColor, improvedTarSM, _SMRGBTarTex, _SMDTarTex, _SMATarTex, thresholdTar);

			_LDARout.render(cam, *_proxy, sunDir);

			float elevationOut = std::min(1.0, std::max(0.0, (M_PI_2 - acos(std::min(1.0f, std::max(0.0f, sunDir.dot(_zenith))))) / M_PI_2));
			_elevationOutTex->update(sibr::ImageRGBA32F(_width, _height, sibr::Vector4f(elevationOut, elevationOut, elevationOut, 1.0f)));

			sibr::Vector3f camSunDir = (cam.view() * sibr::Vector4f(sunDir.x(), sunDir.y(), sunDir.z(), 0.0)).xyz().normalized();
			_sunDirOutTex->update(sibr::ImageRGBA32F(_width, _height, sibr::Vector4f(0.5f * (camSunDir.x() + 1.0f), 0.5f * (camSunDir.y() + 1.0f), 0.5f * (camSunDir.z() + 1.0f), 1.0f)));
		}
		glFinish();

	}

	void OutdoorRelighter::generateInputBuffers(int imId, bool tarChanged, bool improvedTarSM, sibr::Vector3f sunDir, float thresholdTar) {

		bool imChanged = true;
		if (imId == lastImId)
			imChanged = false;

		lastImId = imId;
		if (imId >= 0) {
			generateInputBuffers(*_cams[imId], _images[imId], imChanged, tarChanged, improvedTarSM, sunDir, thresholdTar);
		}
	}

	void OutdoorRelighter::runInteractiveSession(sibr::Window::Ptr window, bool useCloudiness, int startIm) {


		sibr::Vector3f sunDir = _inputSunDir;

		//float teta = M_PI + atan(_north.cross(_zenith).dot(_inputSunDir) / _north.dot(_inputSunDir));
		sibr::Vector3f flatComp = (_inputSunDir - _zenith.dot(_inputSunDir) * _zenith).normalized();
		//We use the mathematical convention
		//Theta is the angle between north and direction reprojected on the ground [0;2Pi]
		float theta = atan2(flatComp.dot(_zenith.cross(_north)), flatComp.dot(_north));
		if (theta < 0) {
			theta += 2 * M_PI;
		}
		//Phi is the between zenith and direction [0;PI/2]
		float phi = acos(_zenith.dot(_inputSunDir));
		float cloudiness = 0.0f;

		//Setup draw sun position
		sibr::ImageRGB imCoord(256, 256, sibr::Vector3ub(0, 0, 0));
		sibr::ImageRGB imCoordTemp(256, 256);
		cv::circle(imCoord.toOpenCV(), cv::Point(128, 128), 128, cv::Scalar(52, 77, 108), -1);
		int numLinePhi = 6;
		int numLineTeta = 24;
		for (int phiStep = 0; phiStep <= numLinePhi; phiStep++) {
			cv::circle(imCoord.toOpenCV(), cv::Point(128, 128), 128 * cos(M_PI * (90.0f / numLinePhi) * phiStep / 180.0f), cv::Scalar(255, 255, 255), 1);
		}
		for (int tetaStep = 0; tetaStep < numLineTeta; tetaStep++) {
			cv::line(imCoord.toOpenCV(),
				cv::Point(128, 128),
				cv::Point(
					128 + 127 * cos(M_PI * (360.0f / numLineTeta) * tetaStep / 180.0f),
					128 + 127 * sin(M_PI * (360.0f / numLineTeta) * tetaStep / 180.0f)),
				cv::Scalar(255, 255, 255), 1);
		}

		float north_input = _inputSunDir.dot(_north);
		float east_input = _inputSunDir.dot(_north.cross(_zenith));
		cv::circle(imCoord.toOpenCV(), cv::Point(128 + 128 * east_input, 128 + 128 * north_input), 12, cv::Scalar(255, 110, 60), -1);
		cv::circle(imCoord.toOpenCV(), cv::Point(128 + 128 * east_input, 128 + 128 * north_input), 11, cv::Scalar(175, 75, 41), 3);

		float ratio = static_cast<float>(_images.at(0)->w()) / static_cast<float>(_images.at(0)->h());
		if (ratio < 1.0) {
			ratio = 1 / ratio;
		}
		std::cout << "The ratio is " << ratio << std::endl;


		float thbW = 256.0f;
		float thbH = thbW / ratio;

		int numVpVert = 5;
		//Resize window after selection
		window->size(_width + 4 * thbW, std::max(numVpVert * thbH + thbW / 2, _height + thbW / 2));

		float offsetTop = (window->h() - numVpVert * thbH) / 2;

		sibr::Viewport vpIm(0.0f, window->h() - thbH - offsetTop, thbW, window->h() - offsetTop);
		sibr::Viewport vpNormal(thbW, window->h() - thbH - offsetTop, 2 * thbW, window->h() - offsetTop);
		sibr::Viewport vpSMSce(0.0f, window->h() - 2 * thbH - offsetTop, thbW, window->h() - thbH - offsetTop);
		sibr::Viewport vpSMTar(thbW, window->h() - 2 * thbH - offsetTop, 2 * thbW, window->h() - thbH - offsetTop);
		sibr::Viewport vpLDARin(0.0f, window->h() - 3 * thbH - offsetTop, thbW, window->h() - 2 * thbH - offsetTop);
		sibr::Viewport vpLDARout(thbW, window->h() - 3 * thbH - offsetTop, 2 * thbW, window->h() - 2 * thbH - offsetTop);
		sibr::Viewport vpElevin(0.0f, window->h() - 4 * thbH - offsetTop, thbW, window->h() - 3 * thbH - offsetTop);
		sibr::Viewport vpElevout(thbW, window->h() - 4 * thbH - offsetTop, 2 * thbW, window->h() - 3 * thbH - offsetTop);
		sibr::Viewport vpSunDirin(0.0f, window->h() - 5 * thbH - offsetTop, thbW, window->h() - 4 * thbH - offsetTop);
		sibr::Viewport vpSunDirout(thbW, window->h() - 5 * thbH - offsetTop, 2 * thbW, window->h() - 4 * thbH - offsetTop);

		sibr::Viewport vpOut(2.5 * thbW, window->h() / 2 - (_height / 2), 2.5 * thbW + _width, window->h() / 2 + (_height / 2));

		sibr::Viewport vpCoord(window->w() - 256.0f - offsetTop, window->h() - 256.0f - offsetTop, window->w() - offsetTop, window->h() - offsetTop);
		sibr::Viewport vpInria(window->w() - 256.0f - offsetTop, offsetTop, window->w() - offsetTop, offsetTop + 90.0f);
		sibr::ImageRGB inriaLogo;
		inriaLogo.load("inria.jpg", false, false);
		inriaLogo.flipH();

		sibr::Texture2DRGB::Ptr coordTex(std::make_shared<sibr::Texture2DRGB>(sibr::ImageRGB()));
		sibr::Texture2DRGB::Ptr inriaTex(std::make_shared<sibr::Texture2DRGB>(inriaLogo));

		//////////////////

		sibr::GLShader quadShader;

		quadShader.init("Texture",
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vp")),
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.fp")));

		bool needClose = false;
		bool improvedTarSM = false;
		bool showInitCondition = false;

		int imId = startIm;
		bool firstFrame = true;
		float thresholdTar = 0.1f;
		int outNum = 0;
		while (!needClose) {

			sibr::Input::poll();
			window->makeContextCurrent();

			ImGui::Begin("Relighter");

			bool imChanged = false;
			bool cloudinessChanged = false;
			bool tarChanged = false;

			if (firstFrame && imId > 0) {
				firstFrame = false;
				imChanged = true;
			}

			if (ImGui::SliderInt("Im", &imId, 0, _cams.size() - 1)) {
				imChanged = true;
			}

			if (!showInitCondition) {
				ImGui::SliderInt("Output", &outNum, 0, _run_node_names.size() - 1);
			}
			else {
				ImGui::SliderInt("Output", &outNum, 0, _input_textures.size() - 1);
			}

			if (ImGui::SliderAngle("Theta", &theta, 0, 360)) {
				tarChanged = true;
			}
			if (ImGui::SliderAngle("Phi", &phi, 0, 90)) {
				tarChanged = true;
			}

			if (_run_node_names.size() > 2) {
				if (ImGui::Checkbox("Improved Target SM", &improvedTarSM)) {
					tarChanged = true;
				}
			}
			if (improvedTarSM && ImGui::SliderFloat("Hole threshold", &thresholdTar, 0.0f, 2.0f)) {
				tarChanged = true;
			}
			if (ImGui::Checkbox("Show initial light conditions", &showInitCondition)) {
				tarChanged = true;
			}
			if (useCloudiness) {
				if (ImGui::SliderFloat("Cloudiness", &cloudiness, 0.0f, 1.0f)) {
					cloudinessChanged = true;
				}
			}

			if (imChanged) {

				imCoordTemp = imCoord.clone();

				float north_cam = _cams[imId]->dir().dot(_north);
				float east_cam = _cams[imId]->dir().dot(_north.cross(_zenith));
				sibr::Vector2f dirCamCoord(north_cam, east_cam);
				dirCamCoord.normalize();
				dirCamCoord *= 30;

				cv::arrowedLine(imCoordTemp.toOpenCV(), cv::Point(128, 128), cv::Point(128 + dirCamCoord.y(), 128 + dirCamCoord.x()), cv::Scalar(255, 110, 60), 3, 8, 0, 0.2f);

				sibr::ImageRGB copyImCoord = imCoordTemp.clone();

				float north_out = sunDir.dot(_north);
				float east_out = sunDir.dot(_north.cross(_zenith));

				cv::circle(copyImCoord.toOpenCV(), cv::Point(128 + 128 * east_out, 128 + 128 * north_out), 12, cv::Scalar(59, 214, 51), -1);
				cv::circle(copyImCoord.toOpenCV(), cv::Point(128 + 128 * east_out, 128 + 128 * north_out), 11, cv::Scalar(48, 175, 42), 3);

				coordTex->update(copyImCoord);

			}

			if (imChanged || tarChanged) {
				if (imId >= 0) {
					sunDir = sibr::OutdoorRelightingDataGen::sphericalToWorld(sibr::Vector2f(theta, phi), _zenith, _north);
				}
			}

			if (tarChanged) {
				sibr::ImageRGB copyImCoord = imCoordTemp.clone();

				float north_out = sunDir.dot(_north);
				float east_out = sunDir.dot(_north.cross(_zenith));

				cv::circle(copyImCoord.toOpenCV(), cv::Point(128 + 128 * east_out, 128 + 128 * north_out), 12, cv::Scalar(59, 214, 51), -1);
				cv::circle(copyImCoord.toOpenCV(), cv::Point(128 + 128 * east_out, 128 + 128 * north_out), 11, cv::Scalar(48, 175, 42), 3);

				coordTex->update(copyImCoord);
			}

			ImGui::End();
			generateInputBuffers(imId, tarChanged, improvedTarSM, sunDir, thresholdTar);


			if (sibr::Input::global().key().isPressed(sibr::Key::Escape))
				needClose = true;
			else {

				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				window->bind();
				window->clear();

				if (imId >= 0 && phi >= 0 && theta >= 0) {

					glDisable(GL_DEPTH_TEST);

					quadShader.begin();
					glActiveTexture(GL_TEXTURE0);
					vpIm.bind();
					glBindTexture(GL_TEXTURE_2D, _inputImageTex->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpNormal.bind();
					glBindTexture(GL_TEXTURE_2D, _NR._normal_RT_32F->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpSMSce.bind();
					glBindTexture(GL_TEXTURE_2D, _SMRGBSceTex->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpSMTar.bind();
					glBindTexture(GL_TEXTURE_2D, _SMRGBTarTex->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpLDARin.bind();
					glBindTexture(GL_TEXTURE_2D, _LDARin._lightDirAlign_RT_32F->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpLDARout.bind();
					glBindTexture(GL_TEXTURE_2D, _LDARout._lightDirAlign_RT_32F->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpElevin.bind();
					glBindTexture(GL_TEXTURE_2D, _elevationInTex->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpElevout.bind();
					glBindTexture(GL_TEXTURE_2D, _elevationOutTex->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpSunDirin.bind();
					glBindTexture(GL_TEXTURE_2D, _sunDirInTex->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpSunDirout.bind();
					glBindTexture(GL_TEXTURE_2D, _sunDirOutTex->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpCoord.bind();
					glBindTexture(GL_TEXTURE_2D, coordTex->handle());
					sibr::RenderUtility::renderScreenQuad();

					vpInria.bind();
					glBindTexture(GL_TEXTURE_2D, inriaTex->handle());
					sibr::RenderUtility::renderScreenQuad();

					//Compute the output

					glFinish();
					window->makeContextNull();

					auto start = std::chrono::high_resolution_clock::now();
					tensorflow::Tensor blur(tensorflow::DT_FLOAT, tensorflow::TensorShape());
					blur.scalar<float>()() = 10.0 * cloudiness;

					std::vector<std::pair<std::string, tensorflow::Tensor>> ph = {};
					ph.push_back({ "blur",blur });

					if (imChanged || tarChanged) {
						//Run the model
						_network->run(_run_node_names, ph);
					}


					window->makeContextCurrent();

					if (imChanged && improvedTarSM) {
						*_refinedSMSce = _output_textures[2]->readBack();
					}

					vpOut.bind();
					if (useCloudiness && outNum == 0) {

						if (showInitCondition)
							glBindTexture(GL_TEXTURE_2D, _input_textures[0]->handle);
						else
							glBindTexture(GL_TEXTURE_2D, _output_textures[0]->handle());
						sibr::RenderUtility::renderScreenQuad();

						glEnable(GL_SCISSOR_TEST);

						glScissor((GLint)vpOut.left(), (GLint)vpOut.top(), _width, _height);

						glColorMask(FALSE, FALSE, FALSE, TRUE);//This ensures that only alpha will be affected

						glClearColor(0, 0, 0, cloudiness);
						glClear(GL_COLOR_BUFFER_BIT);

						glColorMask(TRUE, TRUE, TRUE, TRUE); //Back to default state

						glDisable(GL_SCISSOR_TEST);

						glEnable(GL_BLEND);

						glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA);

						if (showInitCondition)
							glBindTexture(GL_TEXTURE_2D, _input_textures[0]->handle);
						else
							glBindTexture(GL_TEXTURE_2D, _output_textures[1]->handle());

						sibr::RenderUtility::renderScreenQuad();

						glDisable(GL_BLEND);

					}
					else {
						if (showInitCondition)
							glBindTexture(GL_TEXTURE_2D, _input_textures[outNum]->handle);
						else
							glBindTexture(GL_TEXTURE_2D, _output_textures[outNum]->handle());
						sibr::RenderUtility::renderScreenQuad();
					}

					glFinish();

					quadShader.end();
				}

				window->swapBuffer();
			}

		}

	}

	void OutdoorRelighter::runSaveSession(sibr::Window::Ptr window, std::vector<lightingCondition> lcs, bool improvedTarSM) {

		std::cout << "Run save session" << std::endl;
		sibr::ImageRGBA32F out;
		sibr::ImageRGBA32F out_cloudy;

		if (improvedTarSM) {
			std::vector<lightingCondition> lcsCopy;
			int lcC = -1;
			for (auto lc : lcs) {
				if (lcC != lc.imId) { //duplicate entry to get the good refined SM.
					lcC = lc.imId;
					lcsCopy.push_back(lc);
				}
				lcsCopy.push_back(lc);
			}
			lcs = lcsCopy;
		}
		for (auto lc : lcs) {

			window->makeContextCurrent();

			generateInputBuffers(lc.imId, true, improvedTarSM, lc.sunDir, 0.1);

			window->makeContextNull();

			auto start = std::chrono::high_resolution_clock::now();
			tensorflow::Tensor blur(tensorflow::DT_FLOAT, tensorflow::TensorShape());
			blur.scalar<float>()() = 10.0 * lc.cloudiness;

			std::vector<std::pair<std::string, tensorflow::Tensor>> ph = {};
			ph.push_back({ "blur",blur });

			//Run the model
			_network->run(_run_node_names, ph);

			window->makeContextCurrent();


			if (improvedTarSM) {
				*_refinedSMSce = _output_textures[2]->readBack();
			}


			out = _output_textures[0]->readBack();
			if (lc.cloudiness > 0.0) {
				out_cloudy = _output_textures[1]->readBack();
				out.fromOpenCV((1.0f - lc.cloudiness) * out.toOpenCV() + lc.cloudiness * out_cloudy.toOpenCV());
			}

			if (boost::filesystem::extension(lc.path) == ".jpg") {
				out.save(lc.path);
			}
			else {
				cv::Mat out3chan;
				cv::cvtColor(out.toOpenCVBGR(), out3chan, cv::COLOR_RGBA2RGB);
				cv::imwrite(lc.path, out3chan);
				std::cout << "Result saved at " << lc.path << std::endl;
			}

		}

	}

	void OutdoorRelighter::runULRSession(sibr::Window::Ptr window, sibr::OutdoorRelightingIBRScene::Ptr scene, sibr::OutdoorRelightingAppArgs& myArgs, bool useCloudiness, const std::string& datasetPath) {

		// Window size.
		window->size(_width + 1024, 1080);

		sibr::Vector3f sunDir = _inputSunDir;

		sibr::Vector3f flatComp = (_inputSunDir - _zenith.dot(_inputSunDir) * _zenith).normalized();
		//We use the mathematical convention
		//Theta is the angle between north and direction reprojected on the ground [0;2Pi]
		float theta = atan2(flatComp.dot(_zenith.cross(_north)), flatComp.dot(_north));
		if (theta < 0) {
			theta += 2 * M_PI;
		}
		//Phi is the between zenith and direction [0;PI/2]
		float phi = acos(_zenith.dot(_inputSunDir));
		float cloudiness = 0.0f;

		//Setup draw sun position
		sibr::ImageRGB imCoord(256, 256, sibr::Vector3ub(26, 26, 26));
		sibr::ImageRGB imCoordTemp(256, 256);
		{
			cv::circle(imCoord.toOpenCV(), cv::Point(128, 128), 128, cv::Scalar(52, 77, 108), -1);
			int numLinePhi = 6;
			int numLineTeta = 36;
			for (int phiStep = 0; phiStep <= numLinePhi; phiStep++) {
				cv::circle(imCoord.toOpenCV(), cv::Point(128, 128), 128 * cos(M_PI * (90.0f / numLinePhi) * phiStep / 180.0f), cv::Scalar(255, 255, 255), 1);
			}
			for (int tetaStep = 0; tetaStep < numLineTeta; tetaStep++) {
				cv::line(imCoord.toOpenCV(),
					cv::Point(128, 128),
					cv::Point(
						128 + 127 * cos(M_PI * (360.0f / numLineTeta) * tetaStep / 180.0f),
						128 + 127 * sin(M_PI * (360.0f / numLineTeta) * tetaStep / 180.0f)),
					cv::Scalar(255, 255, 255), 1);
			}

			float north_input = _inputSunDir.dot(_north);
			float east_input = _inputSunDir.dot(_north.cross(_zenith));
			cv::circle(imCoord.toOpenCV(), cv::Point(128 + 128 * east_input, 128 + 128 * north_input), 12, cv::Scalar(255, 110, 60), -1);
			cv::circle(imCoord.toOpenCV(), cv::Point(128 + 128 * east_input, 128 + 128 * north_input), 11, cv::Scalar(175, 75, 41), 3);
		}

		sibr::Texture2DRGB::Ptr coordTex(std::make_shared<sibr::Texture2DRGB>(sibr::ImageRGB()));

		//////////////////

		sibr::GLShader quadShader;

		quadShader.init("Texture",
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vp")),
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.fp")));

		// Compute all sizes.
		// Scene internal setup resolution.
		int hScene = window->h();
		int wScene = hScene * _cams[0]->w() / _cams[0]->h();
		// ULR rendering resolution, should correspond to the network input size.
		int wULR = _width;
		int hULR = _height;

		const uint flags = SIBR_GPU_LINEAR_SAMPLING | SIBR_FLIP_TEXTURE;
		scene->renderTargets()->initRGBandDepthTextureArrays(scene->cameras(), scene->images(), scene->proxies(), flags);

		// Create a ULRV3 view.

		sibr::ULRV3View::Ptr	ulrView(new sibr::ULRV3View(scene, scene->_width, scene->_height));

		// Interactive camera handler setup.
		sibr::InteractiveCameraHandler::Ptr generalCamera(new sibr::InteractiveCameraHandler());
		sibr::InputCamera::Ptr startCamera = scene->cameras()->inputCameras()[0];
		//startCamera.aspect(scene->userCamera().aspect());
		const sibr::Viewport sceneViewport(0, 0, wULR, hULR);
		generalCamera->setup(scene->cameras()->inputCameras(), sibr::Vector2u(wULR, hULR), sceneViewport, std::make_shared<sibr::Raycaster>(_raycaster));
		//scene->userCamera(generalCamera->getCamera());
		generalCamera->setupInterpolationPath(scene->cameras()->inputCameras());

		// ULR destination.
		sibr::RenderTargetRGB ulrDst(wULR, hULR, SIBR_GPU_LINEAR_SAMPLING | SIBR_CLAMP_UVS);

		// Debug view.

		const float scalingDebug = 1.0f;
		const sibr::Viewport debugVP(0, 0, scalingDebug * wULR, scalingDebug * hULR);
		sibr::SceneDebugView::Ptr topView = std::make_shared<sibr::SceneDebugView>(scene, debugVP, generalCamera, myArgs);
		sibr::RenderTargetRGB topViewDst(wULR, hULR, SIBR_GPU_LINEAR_SAMPLING | SIBR_CLAMP_UVS);

		bool applyRelighting = true;
		bool showDebugView = false;
		bool improvedTarSM = false;
		float thresholdTar = 0.1f;

		bool recordingImages = false;
		const std::string recordingPath = datasetPath + "/output_path/";
		int recordingFrame = 0;
		int outNum = 0;

		auto start = std::chrono::high_resolution_clock::now();

		while (window->isOpened()) {

			sibr::Input::poll();
			window->makeContextCurrent();

			if (sibr::Input::global().key().isPressed(sibr::Key::Escape)) {
				window->close();
				continue;
			}

			const float ratio = static_cast<float>(_images.at(0)->w()) / static_cast<float>(_images.at(0)->h());
			const float height = 256.f / ratio;
			const float offset = (256.f - height) / 2.f;
			// Update all viewports.
			const sibr::Viewport vpIm(0.0f, window->h() - 256.0f + offset, 256.0f, window->h() - offset);
			const sibr::Viewport vpNormal(256.0f, window->h() - 256.0f + offset, 512.0f, window->h() - offset);
			const sibr::Viewport vpSMSce(0.0f, window->h() - 512.0f + offset, 256.0f, window->h() - 256.0f - offset);
			const sibr::Viewport vpSMTar(256.0f, window->h() - 512.0f + offset, 512.0f, window->h() - 256.0f - offset);
			const sibr::Viewport vpLDARin(0.0f, window->h() - 768.0f + offset, 256.0f, window->h() - 512.0f - offset);
			const sibr::Viewport vpLDARout(256.0f, window->h() - 768.0f + offset, 512.0f, window->h() - 512.0f - offset);
			const sibr::Viewport vpElevin(0.0f, window->h() - 1024.0f + offset, 256.0f, window->h() - 768.0f - offset);
			const sibr::Viewport vpElevout(256.0f, window->h() - 1024.0f + offset, 512.0f, window->h() - 768.0f - offset);
			const sibr::Viewport vpSunDirin(0.0f, window->h() - 1280.0f + offset, 256.0f, window->h() - 1024.0f - offset);
			const sibr::Viewport vpSunDirout(256.0f, window->h() - 1280.0f + offset, 512.0f, window->h() - 1024.0f - offset);

			const sibr::Viewport vpCoord(window->w() - 256.0f, window->h() - 256.0f, window->w(), window->h());

			// Remaining width...
			sibr::Viewport vpDebug(window->w() - wULR * scalingDebug, 0, window->w(), hULR * scalingDebug);
			// When displaying the debug view we put it below the "radar", so we shift the final render accordingly, thus the 3.
			const int rWidth = window->w() - (showDebugView ? 3 : 2) * 256.0f;

			const int rHeight = showDebugView ? ((window->h() - vpDebug.finalHeight() - hULR) * 0.5 + vpDebug.finalHeight()) : ((window->h() - hULR) * 0.5);
			sibr::Viewport vpOut((rWidth - wULR) * 0.5 + 2 * 256.0f, rHeight, (rWidth + wULR) * 0.5 + 2 * 256.0f, rHeight + hULR);
			//sibr::Viewport vpOut(768.0f, window->h() - (640.0f + _height / 2), 768.0f + _width, window->h() - (640.0f - _height / 2));


			// Recording to disk: check if the path is playing, else stop.
			if (recordingImages && !generalCamera->getCameraRecorder().isPlaying()) {
				recordingImages = false;
			}
			bool currentlyRecording = generalCamera->getCameraRecorder().isPlaying() && recordingImages;

			const std::chrono::duration<double, std::milli> duration = std::chrono::high_resolution_clock::now() - start;
			start = std::chrono::high_resolution_clock::now();
			const double frameTime = currentlyRecording ? 0.016666666f : (duration.count() / 1000.0);

			// update main camera.
			generalCamera->onGUI("ULR Camera");
			// We have to flip the viewport for the inputs.
			const sibr::Viewport vpOutIn(vpOut.finalLeft(), window->h() - vpOut.finalBottom(), vpOut.finalRight(), window->h() - vpOut.finalTop());
			generalCamera->update(sibr::Input::subInput(sibr::Input::global(), vpOutIn, true), frameTime);
			//scene->userCamera(generalCamera->getCamera());
			ulrView->onGUI();



			const auto& currentCam = generalCamera->getCamera();
			bool cloudinessChanged = false;
			bool tarChanged = false;

			if (ImGui::Begin("Relighting")) {
				ImGui::Checkbox("Apply relighting", &applyRelighting);
				ImGui::Checkbox("Show debug view", &showDebugView);


				float teta_deg = (theta) * 360.0f / (2 * M_PI);
				if (ImGui::SliderFloat("Teta", &teta_deg, 0.0f, 360.0f, "%.1f deg", 1.0f)) {
					tarChanged = true;
					theta = teta_deg * (2 * M_PI) / 360.0f;
				}

				float phi_deg = (phi) * 360.0f / (2 * M_PI);
				if (ImGui::SliderFloat("Phi", &phi_deg, 0.0f, 90.0f, "%.1f deg", 1.0f)) {
					tarChanged = true;
					phi = phi_deg * (2 * M_PI) / 360.0f;
				}

				ImGui::SliderInt("Output", &outNum, 0, _run_node_names.size() - 1);

				/*if (ImGui::SliderAngle("Phi", &phi, 0, 90)) {
				tarChanged = true;
				}*/
				if (ImGui::Checkbox("Improved Target SM", &improvedTarSM)) {
					tarChanged = true;
				}
				if (improvedTarSM && ImGui::SliderFloat("Hole threshold", &thresholdTar, 0.0f, 2.0f)) {
					tarChanged = true;
				}
				if (useCloudiness) {
					if (ImGui::SliderFloat("Cloudiness", &cloudiness, 0.0f, 1.0f)) {
						cloudinessChanged = true;
					}
				}
				if (ImGui::Button("Record path images to disk")) {
					recordingImages = true;
					sibr::makeDirectory(recordingPath);
					generalCamera->getCameraRecorder().playback();
					currentlyRecording = true;
				}
				if (currentlyRecording) {
					ImGui::SameLine();
					ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Recording...");
				}
				// We update the camera vector display anyway.
				imCoordTemp = imCoord.clone();

				float north_cam = currentCam.dir().dot(_north);
				float east_cam = currentCam.dir().dot(_north.cross(_zenith));
				sibr::Vector2f dirCamCoord(north_cam, east_cam);
				dirCamCoord.normalize();
				dirCamCoord *= 30;

				cv::arrowedLine(imCoordTemp.toOpenCV(), cv::Point(128, 128), cv::Point(128 + dirCamCoord.x(), 128 + dirCamCoord.y()), cv::Scalar(255, 110, 60), 3, 8, 0, 0.2f);

				sibr::ImageRGB copyImCoord = imCoordTemp.clone();

				float north_out = sunDir.dot(_north);
				float east_out = sunDir.dot(_north.cross(_zenith));

				cv::circle(copyImCoord.toOpenCV(), cv::Point(128 + 128 * north_out, 128 + 128 * east_out), 12, cv::Scalar(59, 214, 51), -1);
				cv::circle(copyImCoord.toOpenCV(), cv::Point(128 + 128 * north_out, 128 + 128 * east_out), 11, cv::Scalar(48, 175, 42), 3);

				coordTex->update(copyImCoord);


				sunDir = sibr::OutdoorRelightingDataGen::sphericalToWorld(sibr::Vector2f(theta, phi), _zenith, _north);


				if (tarChanged) {
					sibr::ImageRGB copyImCoord = imCoordTemp.clone();

					float north_out = sunDir.dot(_north);
					float east_out = sunDir.dot(_north.cross(_zenith));

					cv::circle(copyImCoord.toOpenCV(), cv::Point(128 + 128 * north_out, 128 + 128 * east_out), 12, cv::Scalar(59, 214, 51), -1);
					cv::circle(copyImCoord.toOpenCV(), cv::Point(128 + 128 * north_out, 128 + 128 * east_out), 11, cv::Scalar(48, 175, 42), 3);

					coordTex->update(copyImCoord);
				}
			}
			ImGui::End();

			/// ULR rendering.

			ulrView->onRenderIBR(ulrDst, generalCamera->getCamera());
			ulrDst.bind();
			generalCamera->onRender(sibr::Viewport(0, 0, ulrDst.w(), ulrDst.h()));
			ulrDst.unbind();

			/// Input buffers generation.
			if (applyRelighting) {
				sibr::ImageRGB::Ptr currentColor = std::make_shared< sibr::ImageRGB>();
				ulrDst.readBack(*currentColor);
				generateInputBuffers(currentCam, currentColor, true, tarChanged, improvedTarSM, sunDir, thresholdTar);
			}

			/// Debug view rendering.
			if (showDebugView) {
				topViewDst.bind();
				topView->onGUI();
				const sibr::Viewport vpDebugIn(vpDebug.finalLeft(), window->h() - vpDebug.finalBottom(), vpDebug.finalRight(), window->h() - vpDebug.finalTop());
				topView->onUpdate(sibr::Input::subInput(sibr::Input::global(), vpDebugIn, true), frameTime);
				topView->onRender(sibr::Viewport(0.0, 0.0, topViewDst.w(), topViewDst.h()));
				topViewDst.unbind();
			}

			// Now we place everything on screen.
			glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
			window->bind();
			window->viewport().bind();
			window->clear();

			glDisable(GL_DEPTH_TEST);

			quadShader.begin();

			// Render result.
			glActiveTexture(GL_TEXTURE0);
			if (!applyRelighting) {
				vpOut.bind();
				glBindTexture(GL_TEXTURE_2D, ulrDst.handle());
				sibr::RenderUtility::renderScreenQuad();
			}

			if (showDebugView) {
				vpDebug.bind();
				glBindTexture(GL_TEXTURE_2D, topViewDst.handle());
				sibr::RenderUtility::renderScreenQuad();
			}

			vpCoord.bind();
			glBindTexture(GL_TEXTURE_2D, coordTex->handle());
			sibr::RenderUtility::renderScreenQuad();

			// Other views.

			vpIm.bind();
			glBindTexture(GL_TEXTURE_2D, _inputImageTex->handle());
			sibr::RenderUtility::renderScreenQuad();

			vpNormal.bind();
			glBindTexture(GL_TEXTURE_2D, _NR._normal_RT_32F->handle());
			sibr::RenderUtility::renderScreenQuad();

			vpSMSce.bind();
			glBindTexture(GL_TEXTURE_2D, _SMRGBSceTex->handle());
			sibr::RenderUtility::renderScreenQuad();

			vpSMTar.bind();
			glBindTexture(GL_TEXTURE_2D, _SMRGBTarTex->handle());
			sibr::RenderUtility::renderScreenQuad();

			vpLDARin.bind();
			glBindTexture(GL_TEXTURE_2D, _LDARin._lightDirAlign_RT_32F->handle());
			sibr::RenderUtility::renderScreenQuad();

			vpLDARout.bind();
			glBindTexture(GL_TEXTURE_2D, _LDARout._lightDirAlign_RT_32F->handle());
			sibr::RenderUtility::renderScreenQuad();

			vpElevin.bind();
			glBindTexture(GL_TEXTURE_2D, _elevationInTex->handle());
			sibr::RenderUtility::renderScreenQuad();

			vpElevout.bind();
			glBindTexture(GL_TEXTURE_2D, _elevationOutTex->handle());
			sibr::RenderUtility::renderScreenQuad();

			vpSunDirin.bind();
			glBindTexture(GL_TEXTURE_2D, _sunDirInTex->handle());
			sibr::RenderUtility::renderScreenQuad();

			vpSunDirout.bind();
			glBindTexture(GL_TEXTURE_2D, _sunDirOutTex->handle());
			sibr::RenderUtility::renderScreenQuad();

			quadShader.end();

			glFinish();

			if (applyRelighting) {
				window->makeContextNull();

				//auto start = std::chrono::high_resolution_clock::now();
				tensorflow::Tensor blur(tensorflow::DT_FLOAT, tensorflow::TensorShape());
				blur.scalar<float>()() = 10.0 * cloudiness;

				std::vector<std::pair<std::string, tensorflow::Tensor>> ph = {};
				ph.push_back({ "blur",blur });

				//Run the model
				_network->run(_run_node_names, ph);

				window->makeContextCurrent();


				quadShader.begin();
				glActiveTexture(GL_TEXTURE0);

				vpOut.bind();
				if (useCloudiness && outNum == 0) {

					glBindTexture(GL_TEXTURE_2D, _output_textures[0]->handle());
					sibr::RenderUtility::renderScreenQuad();

					glEnable(GL_SCISSOR_TEST);

					glScissor((GLint)vpOut.left(), (GLint)vpOut.top(), _width, _height);

					glColorMask(FALSE, FALSE, FALSE, TRUE);//This ensures that only alpha will be affected

					glClearColor(0, 0, 0, cloudiness);
					glClear(GL_COLOR_BUFFER_BIT);

					glColorMask(TRUE, TRUE, TRUE, TRUE); //Back to default state

					glDisable(GL_SCISSOR_TEST);

					glEnable(GL_BLEND);

					glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA);

					glBindTexture(GL_TEXTURE_2D, _output_textures[1]->handle());

					sibr::RenderUtility::renderScreenQuad();

					glDisable(GL_BLEND);

				}
				else {

					glBindTexture(GL_TEXTURE_2D, _output_textures[outNum]->handle());
					sibr::RenderUtility::renderScreenQuad();
				}

				quadShader.end();


			}
			if (currentlyRecording) {

				sibr::ImageRGB buffer(vpOut.finalWidth(), vpOut.finalHeight());


				glReadPixels(vpOut.finalLeft(), vpOut.finalTop(), vpOut.finalWidth(), vpOut.finalHeight(), GL_RGB, GL_UNSIGNED_BYTE, buffer.data());
				buffer.flipH();
				buffer.save(recordingPath + "/" + sibr::intToString<8>(recordingFrame) + ".png", false);
				++recordingFrame;
			}


			window->swapBuffer();



		}

	}


	std::vector<OutdoorRelighter::lightingCondition> OutdoorRelighter::createLightingConditionsVideo(std::string path, bool exr) {

		std::vector<lightingCondition> result;
		lightingCondition lc;
		lc.cloudiness = 0.f;

		unsigned int numberOfCams = _cams.size();

		for (unsigned int i = 0; i < numberOfCams; ++i)
		{
			std::cout << path << std::endl;
			if (exr)
				lc.path = path + std::string("/result_im") + std::to_string(i) + std::string(".exr");
			else
				lc.path = path + std::string("/result_im") + std::to_string(i) + std::string(".jpg");
			float i_f = static_cast<float> (i);
			float numberOfCam_f = static_cast<float> (numberOfCams);

			float maxPhi = M_PI / 3.f;

			sibr::Vector3f i_p = _zenith.cross(_north);
			sibr::Vector3f j_p = _north * cosf(maxPhi) + _zenith * sinf(maxPhi);

			float theta = 3.f * 2.f * M_PI * i_f / numberOfCam_f; //3 rounds too

			sibr::Vector3f toSun = cosf(theta) * i_p + sinf(theta) * j_p;

			lc.imId = i;

			lc.sunDir = toSun;
			if (lc.sunDir.dot(_zenith) < 0.f) {
				lc.sunDir = lc.sunDir - 2.0f * lc.sunDir.dot(_zenith) * _zenith;
			}
			result.push_back(lc);

		}
		return result;
	}

	std::vector<OutdoorRelighter::lightingCondition> OutdoorRelighter::createLightingConditions(std::string path, int numFrames, bool exr) {

		std::vector<lightingCondition> result;
		lightingCondition lc;
		lc.cloudiness = 0.f;

		unsigned int numberOfCams = _cams.size();

		int numActCam = 0;
		for (unsigned int i = 0; i < numberOfCams; ++i)
		{
			lc.imId = i;

			if (_cams[i]->isActive()) {
				std::cout << path << std::endl;

				std::ostringstream iId;
				iId << std::setw(8) << std::setfill('0') << numActCam;

				for (unsigned int t = 0; t < numFrames; ++t) {

					std::ostringstream fId;
					fId << std::setw(8) << std::setfill('0') << t;


					if (exr)
						lc.path = path + "/" + iId.str() + "/" + fId.str() + std::string(".exr");
					else
						lc.path = path + "/" + iId.str() + "/" + fId.str() + std::string(".jpg");

					float t_f = static_cast<float> (t);
					float numberOfCam_f = static_cast<float> (numberOfCams);

					float maxPhi = M_PI / 3.f;

					sibr::Vector3f i_p = _zenith.cross(_north);
					sibr::Vector3f j_p = _north * cosf(maxPhi) + _zenith * sinf(maxPhi);

					float theta = 2.f * M_PI * t_f / numFrames;

					sibr::Vector3f toSun = cosf(theta) * i_p + sinf(theta) * j_p;
					lc.sunDir = toSun;
					if (lc.sunDir.dot(_zenith) < 0.f) {
						lc.sunDir = lc.sunDir - 2.0f * lc.sunDir.dot(_zenith) * _zenith;
					}
					result.push_back(lc);
				}

				numActCam++;
			}

		}
		return result;
	}

	void OutdoorRelighter::loadLayout(std::string path) {

		//loading the layout
		if (!boost::filesystem::exists(path)) {
			std::cout << "Error : layout.txt file not found at " << path << ", you need to have a layout file" << std::endl;
			SIBR_ERR;
		}
		std::ifstream inLayout(path);
		std::string line;

		int l = 0;
		while (std::getline(inLayout, line))
		{
			std::stringstream check1(line);
			std::string intermediate;
			std::vector<int> input_layout;
			while (getline(check1, intermediate, ' '))
			{
				input_layout.push_back(std::strtof(intermediate.c_str(), 0));
			}

			if (l < 3)
				_input_texture_channels.push_back(input_layout);
			else
				_input_texture_mapping.push_back(input_layout);

			l++;
		}

	}

	std::vector<OutdoorRelighter::lightingCondition> OutdoorRelighter::loadLightingConditions(std::string path, sibr::Vector3f zenith, sibr::Vector3f north, std::string pathOutPrefix) {

		//loading the lighting
		std::ifstream inLightConds(path);
		std::string line;

		std::vector<lightingCondition> lcs;

		while (std::getline(inLightConds, line))
		{
			std::stringstream check1(line);
			std::string intermediate;
			std::vector <std::string> tokens;
			while (getline(check1, intermediate, ' '))
			{
				tokens.push_back(intermediate);
			}

			lightingCondition lc;
			if (tokens.size() == 0) { continue; }
			else if (tokens.size() == 6) {
				lc = {
					std::stoi(tokens[0].c_str(), 0),
					sibr::Vector3f(
						std::strtof(tokens[1].c_str(), 0),
						std::strtof(tokens[2].c_str(), 0),
						std::strtof(tokens[3].c_str(), 0)),
					std::strtof(tokens[4].c_str(), 0),
					pathOutPrefix + tokens[5].c_str()
				};
			}
			else if (tokens.size() == 5) {

				float phi = M_PI * std::strtof(tokens[1].c_str(), 0) / 180.0f;
				float theta = M_PI * std::strtof(tokens[2].c_str(), 0) / 180.0f;

				sibr::Vector3f sunDir = sibr::OutdoorRelightingDataGen::sphericalToWorld(sibr::Vector2f(theta, phi), zenith, north);

				//std::cout << phi << " & " << teta << " & " << sunDir;

				lc = {
					std::stoi(tokens[0].c_str(), 0),
					sunDir,
					std::strtof(tokens[3].c_str(), 0),
					pathOutPrefix + tokens[4].c_str()
				};

			}

			lcs.push_back(lc);
		}

		return lcs;
	}
}

