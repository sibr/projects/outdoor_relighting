/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

uniform vec3 lightDir;
uniform vec3 camPos;

in vec3 VtoF_normal;
in vec3 VtoF_pos;

out float out_val;

const float PI_2 = 1.57079632679489661923;

void main(void) {
	
	vec3 toCam = normalize(camPos - VtoF_pos);
	vec3 reflexion = 2.0*dot(lightDir,VtoF_normal)*VtoF_normal-lightDir;
    out_val = (PI_2-acos(clamp(dot(toCam,reflexion),0.0,1.0)))/PI_2;

}

