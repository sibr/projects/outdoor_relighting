/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "OutdoorRelightingIBRScene.hpp"

#include <core/scene/ParseData.hpp>

namespace sibr {

	OutdoorRelightingIBRScene::~OutdoorRelightingIBRScene(void) {};

	OutdoorRelightingIBRScene::OutdoorRelightingIBRScene(OutdoorRelightingAppArgs& myArgs, sibr::Window::Ptr window, bool noRTs) : BasicIBRScene()
	{

		// parse metadata file
		_data.reset(new ParseData());


		_data->getParsedData(myArgs);
		std::cout << "Number of input Images to read: " << _data->imgInfos().size() << std::endl;

		if (_data->imgInfos().size() != _data->numCameras())
			SIBR_ERR << "List Image file size do not match number of input cameras in Bundle file!" << std::endl;

		_currentOpts.renderTargets = false;
		createFromData(myArgs.texture_width);

		if (myArgs.outH < 0 || myArgs.outW < 0) {

			if (myArgs.outH > 0) {
				_height = myArgs.outH;
				_width = 4 * (_height * _imgs->image(0).w() / (4 * _imgs->image(0).h()));
			}
			else if (myArgs.outW > 0) {
				_width = myArgs.outW;
				_height = 4 * (_width * _imgs->image(0).h() / (4 * _imgs->image(0).w()));
			}
			else {
				_width = 768;
				_height = 4 * (_width * _imgs->image(0).h() / (4 * _imgs->image(0).w()));
			}
		}
		else {
			_width = myArgs.outW;
			_height = myArgs.outH;
		}

		parseDirOutdoorRelighting(myArgs, window);
	};

	void OutdoorRelightingIBRScene::parseDirOutdoorRelighting(OutdoorRelightingAppArgs& myArgs, sibr::Window::Ptr window)
	{
		sibr::Vector3f zenith(0, -1, 0); //Seems to be the default convention for RC in most scenes

		const sibr::Mesh::Ptr& proxy = _proxies->proxyPtr();
		sibr::OutdoorRelightingDataGen RDG(proxy, zenith, _cams->inputCameras(), std::vector<sibr::Vector2u>{ sibr::Vector2u(_width, _height) });
		std::string inPath;
		if (_data->datasetType() != IParseData::Type::MESHROOM) {
			inPath = myArgs.dataset_path.get();
		}
		else {
			inPath = _data->basePathName();
		}

		std::string drPath = inPath + "/outdoorRelighting";
		if (!boost::filesystem::exists(drPath))
			boost::filesystem::create_directory(drPath);

		if (!boost::filesystem::exists(drPath + "/zenith.txt")) {
			std::cout << "Please select the zenith position" << std::endl;
			RDG.selectZenith(window);

			std::ofstream lighting_file(drPath + "/zenith.txt");
			lighting_file << "zenith" << " sunsky " << RDG.getZenith().x() << " " << RDG.getZenith().y() << " " << RDG.getZenith().z() << std::endl;
			lighting_file.close();

		}
		else {
			RDG.setZenith(sibr::OutdoorRelightingDataGen::loadLight(drPath + "/zenith.txt")[0]);
		}
		_zenith = RDG.getZenith();
		SIBR_LOG << "[OutdoorRelighter] Zenith set to: " << _zenith << std::endl;

		//generateNormals
		proxy->generateSmoothNormals(1);
		float radiusScene;
		sibr::Vector3f centerScene;
		proxy->getBoundingSphere(centerScene, radiusScene);

		//Setup sun position
		if (!boost::filesystem::exists(drPath + "/inputSunDir.txt")) {
			std::cout << "Please select the input sun position" << std::endl;
			_inputSunDir = RDG.manualFitSunPos(window,sibr::Vector3f(0,0,0),_imgs->inputImages());

			std::ofstream lighting_file(drPath + "/inputSunDir.txt");
			lighting_file << "inputSunDir" << " sunsky " << _inputSunDir.x() << " " << _inputSunDir.y() << " " << _inputSunDir.z() << std::endl;
			lighting_file.close();
		}
		else {
			_inputSunDir = sibr::OutdoorRelightingDataGen::loadLight(drPath + +"/inputSunDir.txt")[0];
		}
		SIBR_LOG << "[OutdoorRelighter] Selected sun direction :" << _inputSunDir << std::endl;

		_north = _zenith.cross(sibr::Vector3f(1.0, 1.0, 1.0)).normalized();
	}

}