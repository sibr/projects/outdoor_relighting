/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_ASSETS_LIGHTDIRALIGNRENDER_HPP__
#define __SIBR_ASSETS_LIGHTDIRALIGNRENDER_HPP__

# include "Config.hpp"

# include "core/assets/InputCamera.hpp"
# include "core/graphics/Texture.hpp"
# include "core/graphics/Camera.hpp"
# include "core/graphics/RenderUtility.hpp"
# include "core/assets/Resources.hpp"
# include "core/graphics/Shader.hpp"
# include "core/graphics/Mesh.hpp"


namespace sibr
{
	class SIBR_OUTDOORRELIGHTING_EXPORT LightDirAlignRenderer
	{

	public:
		LightDirAlignRenderer(int w, int h, bool useFloats = false);
		~LightDirAlignRenderer();

		void render( const sibr::InputCamera &cam, const Mesh& mesh, sibr::Vector3f lightDir);
		std::shared_ptr<sibr::RenderTargetLum> _lightDirAlign_RT;
		std::shared_ptr<sibr::RenderTargetRGBA32F> _lightDirAlign_RT_32F;

	private:
		sibr::GLShader				_lightDirAlignShader;
		sibr::GLParameter			_lightDirAlignShader_proj;
		sibr::GLParameter			_lightDir;
		sibr::GLParameter			_camPos;

		bool _useFloats;

	};


} // namespace

#endif
