/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef OUTDOORRELIGHTER_H
#define OUTDOORRELIGHTER_H

#include "core/graphics/Window.hpp"
#include "core/renderer/NormalRenderer.hpp"
#include "core/renderer/DepthRenderer.hpp"
#include "core/graphics/Mesh.hpp"
#include "core/raycaster/CameraRaycaster.hpp"
#include "core/graphics/Input.hpp"
#include "projects/outdoor_relighting/renderer/LightDirAlignRenderer.hpp"
#include "projects/outdoor_relighting/renderer/OutdoorRelightingDataGen.hpp"
#include "OutdoorRelightingIBRScene.hpp"

namespace sibr {
	class TF_GL;
	struct TexInfo;


	class SIBR_OUTDOORRELIGHTING_EXPORT OutdoorRelighter
	{
	public:

		OutdoorRelighter::OutdoorRelighter(
			sibr::Window::Ptr window,
			int w,
			int h,
			std::string modelPath,
			const std::vector<sibr::InputCamera::Ptr> & cams,
			const sibr::Mesh::Ptr & proxy,
			const std::vector<sibr::ImageRGB::Ptr> & images,
			sibr::Vector3f & inputSunDir,
			sibr::Vector3f & zenith,
			sibr::Vector3f & north);

		OutdoorRelighter::~OutdoorRelighter(void);
		int _width;
		int _height;

		sibr::Vector3f _north;
		sibr::Vector3f _zenith;
		sibr::Vector3f _inputSunDir;

		std::vector<sibr::InputCamera::Ptr> _cams;
		const std::vector<sibr::ImageRGB::Ptr> _images;
		std::vector<sibr::ImageFloat1::Ptr> _dm;
		sibr::Mesh::Ptr _proxy;

		sibr::Texture2DRGBA32F::Ptr _inputImageTex;
		sibr::NormalRenderer _NR;
		sibr::Texture2DRGBA32F::Ptr _sunDirInTex;
		sibr::Texture2DRGBA32F::Ptr _sunDirOutTex;
		sibr::LightDirAlignRenderer _LDARin;
		sibr::LightDirAlignRenderer _LDARout;
		sibr::Texture2DRGBA32F::Ptr _elevationInTex;
		sibr::Texture2DRGBA32F::Ptr _elevationOutTex;

		sibr::Texture2DRGBA32F::Ptr _SMRGBSceTex;
		sibr::Texture2DRGBA32F::Ptr _SMRGBTarTex;
		sibr::Texture2DRGBA32F::Ptr _SMDSceTex;
		sibr::Texture2DRGBA32F::Ptr _SMDTarTex;

		sibr::Texture2DRGBA32F::Ptr _SMATarTex;
		sibr::ImageFloat4 _areaMap;
		float _medianArea;

		sibr::ImageFloat4 _RGBSMap;
		sibr::ImageFloat4 _ratioMap;
		sibr::ImageRGBA32F::Ptr  _refinedSMSce;
		sibr::ImageFloat1::Ptr _depthMap;

		std::vector<std::shared_ptr<sibr::TexInfo>> _input_textures;
		std::vector<sibr::Texture2DRGBA32F::Ptr> _output_textures;

		std::string _graph_path;
		std::vector<std::string> _input_graph_node_name;
		std::vector<std::vector<int>> _input_texture_channels;
		std::vector<std::vector<int>> _input_texture_mapping;
		std::vector<std::string> _output_node_names;
		std::vector<std::string> _run_node_names;

		sibr::Raycaster _raycaster;
		std::shared_ptr<sibr::TF_GL> _network;

		sibr::OutdoorRelightingAppArgs _myArgs;

		int lastImId;
		struct lightingCondition {
			int imId;
			sibr::Vector3f sunDir;
			float cloudiness;
			std::string path;
		};

		void renderRGBDSMap8bitTar(
			sibr::Vector3f sunDir,
			const sibr::InputCamera& cam,
			const sibr::ImageRGB::Ptr currentImg,
			bool improvedTarSM,
			sibr::Texture2DRGBA32F::Ptr SMRGBTex,
			sibr::Texture2DRGBA32F::Ptr SMDTex,
			sibr::Texture2DRGBA32F::Ptr SMATex,
			float thrld = 0.1);

		void chooseSample(
			const std::vector<sibr::InputCamera::Ptr>& cams,
			const sibr::Vector3f& point,
			const std::vector<sibr::Vector3f>& nfPrecom,
			const sibr::Ray& ray,
			int& bestIm,
			sibr::Vector2i& pos);



		void generateInputBuffers(const sibr::InputCamera& cam, const sibr::ImageRGB::Ptr inputColor, bool imageChanged, bool tarChanged, bool improvedTarSM, sibr::Vector3f sunDir, float thresholdTar = 0.1);


		void generateInputBuffers(int imId, bool tarChanged, bool improvedTarSM, sibr::Vector3f sunDir, float thresholdTar = 0.1);

		void runInteractiveSession(sibr::Window::Ptr window, bool useCloudiness, int startIm=-1);
		void runSaveSession(sibr::Window::Ptr window, std::vector<lightingCondition> lcs, bool improvedTarSM);
		void runULRSession(sibr::Window::Ptr window, sibr::OutdoorRelightingIBRScene::Ptr scene, sibr::OutdoorRelightingAppArgs& myArgs, bool useCloudiness, const std::string& datasetPath);



		std::vector<lightingCondition> createLightingConditionsVideo(std::string path, bool exr = false);

		std::vector<lightingCondition> createLightingConditions(std::string path, int numFrames = 120, bool exr = false);

		void loadLayout(std::string path);

		static std::vector<lightingCondition> loadLightingConditions(std::string path, sibr::Vector3f zenith, sibr::Vector3f north, std::string pathOutPrefix = "");


	};

	void sibr::OutdoorRelighter::chooseSample(
		const std::vector<sibr::InputCamera::Ptr>& cams,
		const sibr::Vector3f& point,
		const std::vector<sibr::Vector3f>& nfPrecom,
		const sibr::Ray& ray,
		int& bestIm,
		sibr::Vector2i& pos)
	{
		float bestW = -1.0f;

		for (int im = 0; im < cams.size(); im++) {

			sibr::Vector3f proj = cams[im]->project(point);
			if (

				abs(proj.x()) < 0.99 &&
				abs(proj.y()) < 0.99

				) {
				proj.y() = -proj.y();
				sibr::Vector2f pos2dImg = (0.5f * (proj.xy() + sibr::Vector2f(1, 1))).cwiseProduct(sibr::Vector2f(cams[im]->w(), cams[im]->h()));

				float ratioW = float(_cams[im]->w()) / _images[im]->w();
				float ratioH = float(_cams[im]->h()) / _images[im]->h();

				sibr::Vector2i secondPosImInt(floor(pos2dImg.x()/ratioW), floor(pos2dImg.y()/ratioH));
				float& z = _dm[im](secondPosImInt).x(); // back to NDC
				float zLin = nfPrecom[im].x() / (nfPrecom[im].y() - z * nfPrecom[im].z());
				float projzLin = nfPrecom[im].x() / (nfPrecom[im].y() - proj.z() * nfPrecom[im].z());
				if (abs(zLin - projzLin) < 0.07) {

					float w = ray.dir().dot((point - cams[im]->position()).normalized());
					if (w > bestW) {
						bestIm = im;
						bestW = w;
						pos = secondPosImInt;
					}
				}
			}
		}

	}

}

#endif