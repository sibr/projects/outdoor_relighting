/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "OutdoorRelightingDataGen.hpp"
#include <random>
#include <imgui/imgui.h>
#include "core/graphics/GUI.hpp"
#include "core/renderer/DepthRenderer.hpp"
#include "core/renderer/NormalRenderer.hpp"
#include "core/renderer/ShadowMapRenderer.hpp"
#include "projects/outdoor_relighting/renderer/LightDirAlignRenderer.hpp"
#include "core/view/interface/MeshViewer.h"
#include "core/view/InteractiveCameraHandler.hpp"
#include "core/renderer/ColoredMeshRenderer.hpp"
#include "core/raycaster/CameraRaycaster.hpp"

namespace sibr
{

	OutdoorRelightingDataGen::OutdoorRelightingDataGen(sibr::Mesh::Ptr proxy, sibr::Vector3f zenith, const std::vector<sibr::InputCamera::Ptr>& cams, std::vector<sibr::Vector2u>& outWH) :
		_proxy(proxy), _zenith(zenith), _cams(cams), _outWH(outWH)
	{

		_AAgrids = std::map<int, std::vector<sibr::Vector2f>>();
		_AAgrids[1] = std::vector<sibr::Vector2f>{ sibr::Vector2f(0,0) };
		_AAgrids[2] = std::vector<sibr::Vector2f>{ sibr::Vector2f(0.25,0.25),sibr::Vector2f(-0.25,-0.25) };
		_AAgrids[4] = std::vector<sibr::Vector2f>{ sibr::Vector2f(-0.125,-0.375),sibr::Vector2f(0.375,-0.125),sibr::Vector2f(-0.375,0.125),sibr::Vector2f(0.125,0.375) };
		_AAgrids[16] = std::vector<sibr::Vector2f>();
		for (int i = 0; i < 16; i++) {
			_AAgrids[16].push_back(hammersley2d(i, 16) - sibr::Vector2f(0.5f, 0.5f));
		}
	}

	sibr::Vector3f OutdoorRelightingDataGen::manualFitSunPos(sibr::Window::Ptr window, sibr::Vector3f dirSun,const std::vector<sibr::ImageRGB::Ptr>& imgs) {

		float radiusScene;
		sibr::Vector3f centerScene;
		_proxy->getBoundingSphere(centerScene, radiusScene);

		if (dirSun.squaredNorm() == 0)
			dirSun = (_zenith + sibr::Vector3f(1, 0, 0)).cross(_zenith);

		sibr::Vector3f north = _zenith.cross(sibr::Vector3f(0, 0, 1)).normalized();

		//Create an Orthographic camera to display the scene.
		int w = 1200;
		int h = 1200;
		window->size(w + 500, h);
		sibr::InputCamera camSun = sibr::OutdoorRelightingDataGen::lightCam(centerScene, radiusScene, dirSun, _zenith, 1200, 1200);
		sibr::ColoredMeshRenderer colMeshRenderer;


		window->makeContextCurrent();

		//Build the raycaster to intersect clicked position
		sibr::Raycaster raycaster;
		raycaster.init();
		_proxy->generateNormals();
		raycaster.addMesh(*_proxy);

		bool needClose = false;
		int numClick = 0;
		std::vector<sibr::Vector3f> posClicked;
		sibr::Mesh pointMesh;

		float tempTheta = 0;
		float tempPhi = 0.01f;
		sibr::RenderTargetRGB orthoRT(1200, 1200);

		int imId = 0;
		sibr::GLShader quadShader;
		quadShader.init("Texture",
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vp")),
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.fp")));



		while (!needClose)
		{

			sibr::Input::poll();

			ImGui::Begin("Direction selection");
			ImGui::SliderFloat("Theta", &tempTheta, 0.0f, 2 * M_PI);
			ImGui::SliderFloat("Phi", &tempPhi, 0.01f, M_PI - 0.01f);
			if (ImGui::Button("Validate")) {
				needClose = true;
			}
			ImGui::End();

			//window->viewport().bind();
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			window->clear();
			glClearColor(0.8f, 0.8f, 0.8f, 1.0f);

			sibr::Viewport vp(0, 0, 1200, 1200);
			vp.bind();
			sibr::Vector3f dirGui = sphericalToWorld(sibr::Vector2f(tempTheta, tempPhi), _zenith, north);
			camSun = sibr::OutdoorRelightingDataGen::lightCam(centerScene, radiusScene, dirGui, _zenith, 1200, 1200);
			colMeshRenderer.process(*_proxy, camSun, orthoRT, sibr::Mesh::FillRenderMode, false);

			sibr::showImGuiWindow("Direction selection", orthoRT, 0, vp, false, false);

			if (imgs.size() > 0) {
				window->bind();
				ImGui::Begin("Shadow map visualization");
				ImGui::SliderInt("Image", &imId, 0, imgs.size() - 1);

				sibr::ImageL8::Ptr shadowMap;
				sibr::Vector2u whSM(512, 512 * _cams[imId]->h() / _cams[imId]->w());
				sibr::Viewport vpSM(0, 0, whSM.x(), whSM.y());
				shadowMap = sibr::OutdoorRelightingDataGen::renderShadowMapRay(raycaster, whSM, dirGui, dirGui + sibr::Vector3f(1, 1, 1), *_cams[imId], 8, _AAgrids[1]);

				sibr::ImageRGB shadowedIm= imgs[imId]->resized(whSM.x(),whSM.y());
				for (int j = 0; j < shadowedIm.h(); j++) {
					for (int i = 0; i < shadowedIm.w(); i++) {
						sibr::Vector3f pix = shadowedIm(i, j).cast<float>();
						pix *= 0.2 + 0.8*shadowMap(i, j).x() / (255.0f);
						shadowedIm(i, j) = pix.cast<unsigned char>();
					}

				}
				//	sibr::Texture2DRGB smTex(test,SIBR_GPU_LINEAR_SAMPLING); DOES NOT WORK
				// Create a OpenGL texture identifier
				GLuint image_texture;
				glGenTextures(1, &image_texture);
				glBindTexture(GL_TEXTURE_2D, image_texture);
				// Setup filtering parameters for display
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				// Upload pixels into texture
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, whSM.x(), whSM.y(), 0, GL_RGB, GL_UNSIGNED_BYTE, shadowedIm.data());
				//	ImGui::Image((void*)(intptr_t)smTex.handle(), ImVec2(200, 200));
				ImGui::Image((void*)(intptr_t)image_texture, ImVec2( whSM.x(), whSM.y()));
				ImGui::End();
			}

			window->swapBuffer();

		}

		std::cout << "Selected direction" << -camSun.dir() << std::endl;
		return -camSun.dir().normalized();

	}

	sibr::InputCamera OutdoorRelightingDataGen::lightCam(sibr::Vector3f center, float radius, sibr::Vector3f dirLight, sibr::Vector3f up, int w, int h)
	{
		dirLight.normalize();
		sibr::Camera camOrthoConfig;
		camOrthoConfig.setLookAt(center + (2 * radius) * dirLight, center, up);
		camOrthoConfig.znear(radius);
		camOrthoConfig.zfar(3 * radius);
		camOrthoConfig.setOrthoCam(radius, radius);

		return sibr::InputCamera(camOrthoConfig, w, h);
	}

	sibr::Vector3f OutdoorRelightingDataGen::sphericalToWorld(sibr::Vector2f thetaPhi, sibr::Vector3f zenith, sibr::Vector3f north)
	{

		zenith.normalize();
		north.normalize();

		sibr::Vector3f west = (zenith.cross(north)).normalized();

		double theta = thetaPhi.x();
		double phi = thetaPhi.y();

		sibr::Vector3f worldDir(0, 0, 0);

		worldDir += sin(phi) * cos(theta) * north;
		worldDir += sin(phi) * sin(theta) * west;
		worldDir += cos(phi) * zenith;

		worldDir.normalize();

		return worldDir;
	}


	void OutdoorRelightingDataGen::selectZenith(sibr::Window::Ptr window) {

		_zenith = OutdoorRelightingDataGen::manualFitSunPos(window);

	}

	void OutdoorRelightingDataGen::setZenith(sibr::Vector3f zenith) {

		_zenith = zenith;

	}
	sibr::Vector3f sibr::OutdoorRelightingDataGen::getZenith() {

		return _zenith;

	}

	void sibr::OutdoorRelightingDataGen::genSaveShadowMap(
		sibr::Raycaster& raycaster,
		sibr::Vector3f sunDir,
		std::string path,
		std::string lightName,
		sibr::MaterialMesh::Ptr proxyRayCaster)
	{
		bool mitsubaSystem = true;

		sibr::Vector3f dirSun;

		float radiusScene;
		sibr::Vector3f centerScene;
		_proxy->getBoundingSphere(centerScene, radiusScene);

		std::cout << "Vert " << _proxy->vertices().size() << std::endl;
		//sibr::InputCamera camSun = sibr::OutdoorRelightingDataGen::lightCam(centerScene, radiusScene, sunDir, _zenith);

		/*	sibr::DepthRenderer DR(1024, 1024);
			DR.render(camSun, *_proxy, false, true);
			sibr::ImageFloat1 test(1024, 1024);
			DR._depth_RT->readBack(test);
			showFloat(test);*/

			/*sibr::DepthRenderer DRFull(16384, 16384);
			DRFull.render(camSun, *_proxy, false, true);

			std::vector<sibr::ImageL8> shadowMap(_cams.size());

			for (int imId = 0; imId < _cams.size(); imId++) {
				sibr::ShadowMapRenderer SMR(camSun, DRFull._depth_RT);
				SMR.render(_outWH[imId].x(), _outWH[imId].y(), _cams[imId], *_proxy);
				SMR._shadowMap_RT->readBack(shadowMap[imId]);
			}*/

		bool useOpacity = false;
		if (proxyRayCaster->opacityMaps().size() > 0)
			useOpacity = true;
#pragma omp parallel for
		for (int imId = 0; imId < _cams.size(); imId++) {

			sibr::ImageL8::Ptr shadowMap;
			if (useOpacity)
				shadowMap = sibr::OutdoorRelightingDataGen::renderShadowMapRayOpacity(raycaster, _outWH[imId], sunDir, sunDir + sibr::Vector3f(1, 1, 1), *_cams[imId], 64, _AAgrids[4], proxyRayCaster);
			else {
				shadowMap = sibr::OutdoorRelightingDataGen::renderShadowMapRay(raycaster, _outWH[imId], sunDir, sunDir + sibr::Vector3f(1, 1, 1), *_cams[imId], 64, _AAgrids[4]);
			}
			shadowMap->save(path + _cams[imId]->name() + "/" + lightName + ".png", false);
			//shadowMap[imId].save(path + _cams[imId].name() + "/" + lightName + ".png",false);
		}


	}


	void sibr::OutdoorRelightingDataGen::genSaveNormals(
		std::string path)
	{

		std::cout << "Normal " << _proxy->normals().size() << std::endl;
		std::vector<sibr::ImageRGB> normals(_cams.size());
		for (int imId = 0; imId < _cams.size(); imId++) {
			sibr::NormalRenderer NR(_outWH[imId].x(), _outWH[imId].y(), false, false, true);
			NR.render(*_cams[imId], *_proxy);
			NR._normal_RT->readBack(normals[imId]);
		}
#pragma omp parallel for
		for (int imId = 0; imId < _cams.size(); imId++) {
			normals[imId].save(path + _cams[imId]->name() + ".png", false);
		}

	}

	void sibr::OutdoorRelightingDataGen::genSaveAO(sibr::Raycaster& raycaster, std::string path)
	{
		for (int imId = 0; imId < _cams.size(); imId++) {

			sibr::ImageL8::Ptr AO = sibr::OutdoorRelightingDataGen::renderAORay(raycaster, _outWH[imId], _zenith, _zenith + sibr::Vector3f(1, 1, 1), *_cams[imId], 256, _AAgrids[4], nullptr);
			AO->save(path + _cams[imId]->name() + ".png", false);
		}
	}

	void OutdoorRelightingDataGen::genSaveSkyMask(sibr::Raycaster& raycaster, std::string path)
	{
		for (int imId = 0; imId < _cams.size(); imId++) {

			sibr::ImageL8::Ptr skyMask = sibr::OutdoorRelightingDataGen::renderSkyMaskRay(raycaster, _outWH[imId], *_cams[imId]);
			skyMask->save(path + _cams[imId]->name() + ".png", false);
		}
	}


	void sibr::OutdoorRelightingDataGen::genSaveFirstBounceMap(
		sibr::Raycaster& raycaster,
		sibr::Vector3f sunDir,
		std::string path,
		std::string lightName) {

#pragma omp parallel for
		for (int imId = 0; imId < _cams.size(); imId++) {

			sibr::ImageL8::Ptr firstBounceMap = sibr::OutdoorRelightingDataGen::renderFirstBounceRay(raycaster, _outWH[imId], sunDir, sunDir + sibr::Vector3f(1, 1, 1), *_cams[imId], 128, _AAgrids[16], nullptr);
			firstBounceMap->save(path + _cams[imId]->name() + "/" + lightName + ".png", false);
		}
	}

	void sibr::OutdoorRelightingDataGen::genSaveElevation(
		sibr::Vector3f sunDir,
		std::string path,
		std::string lightName)
	{
		bool mitsubaSystem = true;

		float elevation = std::min(1.0, std::max(0.0, (M_PI_2 - acos(std::min(1.0f, std::max(0.0f, sunDir.dot(_zenith))))) / M_PI_2));

		unsigned char valUchar = elevation * 255;

#pragma omp parallel for
		for (int imId = 0; imId < _cams.size(); imId++) {
			sibr::ImageL8 camDirIm(_outWH[imId].x(), _outWH[imId].y(), valUchar);
			camDirIm.save(path + _cams[imId]->name() + "/" + lightName + ".png", false);
		}

	}

	void sibr::OutdoorRelightingDataGen::genSaveSunDir(
		sibr::Vector3f sunDir,
		std::string path,
		std::string lightName)
	{

#pragma omp parallel for
		for (int imId = 0; imId < _cams.size(); imId++) {
			sibr::Vector3f camSunDir = (_cams[imId]->view() * sibr::Vector4f(sunDir.x(), sunDir.y(), sunDir.z(), 0.0)).xyz().normalized();
			sibr::Vector3ub valUchar = ((camSunDir + sibr::Vector3f(1.0, 1.0, 1.0)) * 127.5).cast<unsigned char>();
			sibr::ImageRGB sunDirIm(_outWH[imId].x(), _outWH[imId].y(), valUchar);
			sunDirIm.save(path + _cams[imId]->name() + "/" + lightName + ".png", false);

		}

	}

	void sibr::OutdoorRelightingDataGen::genSaveReflexions(
		sibr::Vector3f sunDir,
		std::string path,
		std::string lightName)
	{
		bool mitsubaSystem = true;

		sibr::Vector3f dirSun;

		std::vector<sibr::ImageL8> reflexionMaps(_cams.size());
		for (int imId = 0; imId < _cams.size(); imId++) {

			sibr::LightDirAlignRenderer LDAR(_outWH[imId].x(), _outWH[imId].y());
			LDAR.render(*_cams[imId], *_proxy, sunDir);
			LDAR._lightDirAlign_RT->readBack(reflexionMaps[imId]);
		}

#pragma omp parallel for
		for (int imId = 0; imId < _cams.size(); imId++) {
			reflexionMaps[imId].save(path + _cams[imId]->name() + "/" + lightName + ".png", false);
		}


	}

	std::vector<sibr::Vector3f> sibr::OutdoorRelightingDataGen::loadLight(std::string lightFile)
	{

		//loading the lighting
		std::ifstream inLights(lightFile);
		std::string line;

		std::vector<sibr::Vector3f> sunDirs;

		while (std::getline(inLights, line))
		{
			std::stringstream check1(line);
			std::string intermediate;
			std::vector <std::string> tokens;
			while (getline(check1, intermediate, ' '))
			{
				tokens.push_back(intermediate);
			}

			sibr::Vector3f sunDir;
			std::cout << "type" << tokens[1].c_str() << std::endl;
			if (strcmp(tokens[1].c_str(), "constant") == 0 || strcmp(tokens[1].c_str(), "envmap") == 0) {
				sunDir = sibr::Vector3f(0, 0, 0);
			}
			else if (tokens.size() == 5) {
				sunDir = sibr::Vector3f(
					std::strtof(tokens[2].c_str(), 0),
					std::strtof(tokens[3].c_str(), 0),
					std::strtof(tokens[4].c_str(), 0));
			}

			sunDirs.push_back(sunDir);
		}
		return sunDirs;
	}

	void sibr::OutdoorRelightingDataGen::genSaveLightFile(int num, sibr::Vector3f zenith, std::string path, bool inOut)
	{

		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_real_distribution<double> r(0, 0.99);
		std::uniform_real_distribution<double> theta(0.0, 2 * M_PI);

		int k = 1;

		std::ofstream outLight;
		outLight.open(path);
		std::vector<sibr::Vector3f> points;
		std::vector<sibr::Vector2f> points_circle;

		zenith.normalize();
		sibr::Vector3f north = zenith.cross(zenith + sibr::Vector3f(1, 1, 1));
		north.normalize();
		sibr::Vector3f east = zenith.cross(north);
		east.normalize();

		for (int i = 0; i < num; i++) {

			std::ostringstream iString;
			iString << std::setw(3) << std::setfill('0') << i;

			std::vector<sibr::Vector3f> cands;
			std::vector<sibr::Vector2f> cands_circle;
			for (int c = 0; c < k; c++) {
				float r_c = r(mt);
				float theta_c = theta(mt);
				float xc = sqrt(r_c) * cos(theta_c);
				float yc = sqrt(r_c) * sin(theta_c);
				float zc = sqrt(1 - (xc * xc + yc * yc));
				sibr::Vector3f cand = xc * north + yc * east + zc * zenith;

				//std::cout << "cand norm" << cand.norm() << std::endl;

				cands_circle.push_back(sibr::Vector2f(xc, yc));
				cands.push_back(cand);
			}

			sibr::Vector3f bestCand = cands[0];
			sibr::Vector2f bestCand_circle = cands_circle[0];
			float bestMinDist = 0;

			for (int c = 0; c < cands.size(); c++) {

				float minDist = 2;
				float distW = 0.3;
				for (int p = 0; p < points.size(); p++) {
					if ((points_circle[p] - cands_circle[c]).norm() + distW * (points[p] - cands[c]).norm()
						< minDist) {
						minDist = (points_circle[p] - cands_circle[c]).norm() + distW * (points[p] - cands[c]).norm();
					}
				}

				if (minDist > bestMinDist) {
					bestMinDist = minDist;
					bestCand = cands[c];
					bestCand_circle = cands_circle[c];
				}
			}

			points.push_back(bestCand);
			points_circle.push_back(bestCand_circle);

		}

		std::sort(std::begin(points), std::end(points), [zenith](sibr::Vector3f a, sibr::Vector3f b) {return a.dot(zenith) > b.dot(zenith); });

		outLight << "light000" << " constant" << std::endl;

		for (int i = 1; i < num; i++) {

			sibr::Vector3f bestCand = points[i];
			std::ostringstream iString;
			iString << std::setw(3) << std::setfill('0') << i;

			if (inOut) {
				outLight << "light_in_" << iString.str() << " sun " << bestCand.x() << " " << bestCand.y() << " " << bestCand.z() << std::endl;
				if (i % 10 == 0)
					outLight << "light_out_" << iString.str() << " sun " << bestCand.x() << " " << bestCand.y() << " " << bestCand.z() << std::endl;
			}
			else {
				outLight << "light" << iString.str() << " sun " << bestCand.x() << " " << bestCand.y() << " " << bestCand.z() << std::endl;
			}
		}

		outLight.close();
		sibr::Window::Ptr	window(new sibr::Window(1200, 600, "test"));

		sibr::MaterialMesh mesh;
		mesh.vertices(points);

		sibr::MeshViewer viewer;
		viewer.setMainMesh(*window, mesh);
		viewer.renderer->addPoints(points, sibr::Vector3f(1.0, 0.0, 0.0));
		viewer.renderLoop(window);
		//outLight.close();
	}

	sibr::ImageL8::Ptr sibr::OutdoorRelightingDataGen::renderShadowMapRay(
		sibr::Raycaster& raycaster,
		const sibr::Vector2u& resolution,
		const sibr::Vector3f& sunDir,
		const sibr::Vector3f& north,
		const sibr::InputCamera& cam,
		const int spp, const std::vector<sibr::Vector2f> AApattern,
		sibr::ImageRGB::Ptr normals)
	{

		const int AA = AApattern.size();
		const int numRayPacket = spp / (8 * AA);
		if (numRayPacket <= 0) {
			std::cout << "Error : The number of sample per pixel is to small, spp should be higher then 8*AA" << std::endl;
			SIBR_ERR;
		}
		if (AA != 1 && AA != 2 && AA != 4) {
			std::cout << "Error : AA should be 1,2 or 4." << std::endl;
			SIBR_ERR;
		}

		sibr::ImageL8::Ptr shadowMap = std::make_shared<sibr::ImageL8>(sibr::ImageL8(resolution.x(), resolution.y(), 255));

		sibr::Vector3f dx, dy, upLeftOffset;
		sibr::CameraRaycaster::computePixelDerivatives(cam, dx, dy, upLeftOffset);
		sibr::Vector3f camZaxis = cam.dir().normalized();
		float maxD = -1.0f, minD = -1.0f;

		//auto t1 = std::chrono::high_resolution_clock::now();

		sibr::Vector3f u1 = (sunDir.cross(north)).normalized();
		sibr::Vector3f u2 = (sunDir.cross(u1)).normalized();

		for (int j = 0; j < resolution.y(); j++) {
			for (int i = 0; i < resolution.x(); i++) {

				float pixelVal = 1.0f;

				for (int aa = 0; aa < AA; aa++) {

					float ii = (i + AApattern[aa].x()) * cam.w() / (float)resolution.x();
					float jj = (j + AApattern[aa].y()) * cam.h() / (float)resolution.y();

					sibr::Vector3f worldPos = ((float)ii + 0.5f) * dx + ((float)jj + 0.5f) * dy + upLeftOffset;
					sibr::Vector3f dir = (worldPos - cam.position()).normalized();

					sibr::RayHit hit = raycaster.intersect(sibr::Ray(cam.position(), dir));

					if (!hit.hitSomething()) { continue; }

					sibr::Vector3f hitPos = cam.position() + dir * hit.dist();

					sibr::Vector3f normal = -hit.normal().normalized();

					if (normal.dot(dir) > 0) {
						normal = -normal;
					}
					if (normal.dot(sunDir) < -0.01f) {
						shadowMap(i, j).x() = 0;
						pixelVal -= 1.0f / AA;
						continue;
					}

					hitPos += 0.002f * normal;


					for (int rp = 0; rp < numRayPacket; rp++) {

						std::array<sibr::Ray, 8> rays;

						for (int rs = 0; rs < 8; rs++) {
							sibr::Vector3f dirSunRay = sunDir
								+ tan((M_PI / 180) * (0.5358 / 2.f)) * getDisk(aa * numRayPacket * 8 + rp * 8 + rs).x() * u1
								+ tan((M_PI / 180) * (0.5358 / 2.f)) * getDisk(aa * numRayPacket * 8 + rp * 8 + rs).y() * u2;
							dirSunRay.normalize();
							rays[rs] = sibr::Ray(hitPos, dirSunRay);
						}

						std::array<bool, 8> hitSmthg = raycaster.hitSomething8(rays, 0.0005f);

						for (int rs = 0; rs < 8; rs++) {
							if (hitSmthg[rs])
								pixelVal -= 1.0f / (8 * numRayPacket * AA);
						}
					}
				}

				shadowMap(i, j).x() = (unsigned char)255.0f * pixelVal;

			}
		}

		auto t2 = std::chrono::high_resolution_clock::now();

		//std::cout << "f() took "
		//	<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		//	<< " milliseconds\n";

		return shadowMap;

	}

	sibr::ImageL8::Ptr sibr::OutdoorRelightingDataGen::renderShadowMapRayOpacity(
		sibr::Raycaster& raycaster,
		const sibr::Vector2u& resolution,
		const sibr::Vector3f& sunDir,
		const sibr::Vector3f& north,
		const sibr::InputCamera& cam,
		const int spp, const std::vector<sibr::Vector2f> AApattern,
		sibr::MaterialMesh::Ptr proxyRayCast)
	{

		const int AA = AApattern.size();
		const int numRayPacket = spp / (8 * AA);
		if (numRayPacket <= 0) {
			std::cout << "Error : The number of sample per pixel is to small, spp should be higher then 8*AA" << std::endl;
			SIBR_ERR;
		}
		if (AA != 1 && AA != 2 && AA != 4) {
			std::cout << "Error : AA should be 1,2 or 4." << std::endl;
			SIBR_ERR;
		}

		sibr::ImageL8::Ptr shadowMap = std::make_shared<sibr::ImageL8>(sibr::ImageL8(resolution.x(), resolution.y(), 255));

		sibr::Vector3f dx, dy, upLeftOffset;
		sibr::CameraRaycaster::computePixelDerivatives(cam, dx, dy, upLeftOffset);
		sibr::Vector3f camZaxis = cam.dir().normalized();
		float maxD = -1.0f, minD = -1.0f;

		//auto t1 = std::chrono::high_resolution_clock::now();

		sibr::Vector3f u1 = (sunDir.cross(north)).normalized();
		sibr::Vector3f u2 = (sunDir.cross(u1)).normalized();

		const sibr::MaterialMesh::UVs& UVs = proxyRayCast->texCoords();

		auto positive_modulo = [](int i, int n) {return (i % n + n) % n; };

		for (int j = 0; j < resolution.y(); j++) {
			for (int i = 0; i < resolution.x(); i++) {

				float pixelVal = 1.0f;

				for (int aa = 0; aa < AA; aa++) {

					float ii = (i + AApattern[aa].x()) * cam.w() / (float)resolution.x();
					float jj = (j + AApattern[aa].y()) * cam.h() / (float)resolution.y();

					sibr::Vector3f worldPos = ((float)ii + 0.5f) * dx + ((float)jj + 0.5f) * dy + upLeftOffset;
					sibr::Vector3f dir = (worldPos - cam.position()).normalized();

					sibr::RayHit hit;
					sibr::Vector3f posRay = cam.position();
					{
						bool transparent = true;
						bool continueBool = false;

						while (transparent) {

							hit = raycaster.intersect(sibr::Ray(posRay, dir));
							if (!hit.hitSomething()) { continueBool = true; break; }
							else {

								uint triId = hit.primitive().triID;
								const sibr::Vector3u& tri = proxyRayCast->triangles()[triId];

								sibr::Vector3f interpUV = hit.interpolateUV();

								sibr::Vector2f UVhit = interpUV[0] * UVs[tri[0]] + interpUV[1] * UVs[tri[1]] + interpUV[2] * UVs[tri[2]];

								int matId = proxyRayCast->matIds()[triId];
								std::string MatName = proxyRayCast->matId2Name()[matId];

								sibr::ImageRGB::Ptr opacityTexture = proxyRayCast->opacityMap(MatName);
								if (opacityTexture) {
									sibr::Vector2i posTexture(
										positive_modulo(UVhit.x() * opacityTexture->w(), opacityTexture->w()),
										opacityTexture->h() - 1 - positive_modulo(UVhit.y() * opacityTexture->h(), opacityTexture->h())
									);

									if (opacityTexture(posTexture).x() > 10) {
										transparent = false;
									}
									else {
										posRay = posRay + dir * (hit.dist() + 0.001f);
									}
								}
								else
								{
									transparent = false;
								}

							}
						}

						if (continueBool)
							continue;
					}

					sibr::Vector3f hitPos = posRay + dir * hit.dist();

					sibr::Vector3f normal = -hit.normal().normalized();

					if (normal.dot(dir) > 0) {
						normal = -normal;
					}
					if (normal.dot(sunDir) < -0.01f) {
						shadowMap(i, j).x() = 0;
						pixelVal -= 1.0f / AA;
						continue;
					}

					hitPos += 0.002f * normal;

					for (int rp = 0; rp < numRayPacket; rp++) {

						for (int rs = 0; rs < 8; rs++) {
							sibr::Vector3f posRaySun = hitPos;

							sibr::Vector3f dirSunRay = sunDir
								+ tan((M_PI / 180) * (0.5358 / 2.f)) * getDisk(aa * numRayPacket * 8 + rp * 8 + rs).x() * u1
								+ tan((M_PI / 180) * (0.5358 / 2.f)) * getDisk(aa * numRayPacket * 8 + rp * 8 + rs).y() * u2;
							dirSunRay.normalize();

							bool transparentSun = true;
							bool continueBool = false;

							while (transparentSun) {
								hit = raycaster.intersect(sibr::Ray(posRaySun, dirSunRay));

								if (!hit.hitSomething()) { continueBool = true; break; }
								else {
									uint triId = hit.primitive().triID;
									const sibr::Vector3u& tri = proxyRayCast->triangles()[triId];
									sibr::Vector3f interpUV = hit.interpolateUV();
									sibr::Vector2f UVhit = interpUV[0] * UVs[tri[0]] + interpUV[1] * UVs[tri[1]] + interpUV[2] * UVs[tri[2]];
									int matId = proxyRayCast->matIds()[triId];
									std::string MatName = proxyRayCast->matId2Name()[matId];

									sibr::ImageRGB::Ptr opacityTexture = proxyRayCast->opacityMap(MatName);
									if (opacityTexture) {
										sibr::Vector2i posTexture(
											positive_modulo(UVhit.x() * opacityTexture->w(), opacityTexture->w()),
											opacityTexture->h() - 1 - positive_modulo(UVhit.y() * opacityTexture->h(), opacityTexture->h())
										);

										if (opacityTexture(posTexture).x() > 10) {
											transparentSun = false;
										}
										else {
											posRaySun = posRaySun + dirSunRay * (hit.dist() + 0.001f);
										}
									}
									else
									{
										transparentSun = false;
									}
								}
							}

							if (continueBool)
								continue;

							pixelVal -= 1.0f / (8 * numRayPacket * AA);
						}
					}

				}

				shadowMap(i, j).x() = (unsigned char)255.0f * pixelVal;

			}
		}

		return shadowMap;

	}

	void sibr::OutdoorRelightingDataGen::renderShadowMapRay(
		sibr::Texture2DLum::Ptr texture,
		sibr::Raycaster& raycaster,
		const sibr::Vector2u& resolution,
		const sibr::Vector3f& sunDir,
		const sibr::Vector3f& north,
		const sibr::InputCamera& cam,
		const int spp, const std::vector<sibr::Vector2f> AApattern,
		sibr::ImageRGB::Ptr normals) {

		/*sibr::ImageL8::Ptr SM = renderShadowMapRay(raycaster, resolution, sunDir, north, cam, spp, AApattern, normals);
		SM->flipH();
		texture->update(*SM);*/

	}

	sibr::ImageFloat4::Ptr OutdoorRelightingDataGen::render3DSMapRay(sibr::Raycaster& raycaster, const sibr::Vector2u& resolution, const sibr::Vector3f& sunDir, const sibr::Vector3f& north, const sibr::InputCamera& cam, sibr::ImageFloat1::Ptr& SMap3Dr)
	{

		sibr::ImageFloat4::Ptr SMap3D = std::make_shared<sibr::ImageFloat4>(sibr::ImageFloat4(resolution.x(), resolution.y(), sibr::Vector4f(-1, -1, -1, -1)));
		SMap3Dr = std::make_shared<sibr::ImageFloat1>(sibr::ImageFloat1(resolution.x(), resolution.y(), 20));

		sibr::Vector3f dx, dy, upLeftOffset;
		sibr::CameraRaycaster::computePixelDerivatives(cam, dx, dy, upLeftOffset);
		sibr::Vector3f camZaxis = cam.dir().normalized();
		float maxD = -1.0f, minD = -1.0f;

		//auto t1 = std::chrono::high_resolution_clock::now();

		sibr::Vector3f u1 = (sunDir.cross(north)).normalized();
		sibr::Vector3f u2 = (sunDir.cross(u1)).normalized();

#pragma omp parallel for
		for (int j = 0; j < resolution.y(); j++) {
			for (int i = 0; i < resolution.x(); i += 8) {

				float pixelVal = 1.0f;
				float jj = j * cam.h() / (float)resolution.y();

				std::array<sibr::Ray, 8> rays;

				for (int i_r = 0; i_r < 8; i_r++) {
					float ii = (i + i_r) * cam.w() / (float)resolution.x();
					sibr::Vector3f worldPos = ((float)ii + 0.5f) * dx + ((float)jj + 0.5f) * dy + upLeftOffset;
					sibr::Vector3f dir = (worldPos - cam.position()).normalized();

					rays[i_r] = sibr::Ray(sibr::Ray(cam.position(), dir));

				}

				std::array<RayHit, 8> rayhits = raycaster.intersect8(rays);

				for (int i_r = 0; i_r < 8; i_r++) {
					if (!(i + i_r < resolution.x()) || !rayhits[i_r].hitSomething()) { continue; }

					const sibr::Vector3f& dir = rayhits[i_r].ray().dir();
					sibr::Vector3f hitPos = cam.position() + dir * rayhits[i_r].dist();

					sibr::Vector3f normal = -rayhits[i_r].normal().normalized();

					if (normal.dot(dir) > 0) {
						normal = -normal;
					}
					if (normal.dot(sunDir) < -0.01f) {

						SMap3D(i + i_r, j) = sibr::Vector4f(hitPos.x(), hitPos.y(), hitPos.z(), 0);
						SMap3Dr(i + i_r, j).x() = 0;

						continue;
					}

					hitPos += 0.002f * normal;

					sibr::RayHit hitSun = raycaster.intersect(sibr::Ray(hitPos, sunDir));

					if (!hitSun.hitSomething()) { continue; }

					sibr::Vector3f hitPosSun = hitPos + sunDir * hitSun.dist();

					SMap3D(i + i_r, j) = sibr::Vector4f(hitPosSun.x(), hitPosSun.y(), hitPosSun.z(), hitSun.dist() / rayhits[i_r].dist());
					SMap3Dr(i + i_r, j).x() = hitSun.dist() / rayhits[i_r].dist();
				}

			}
		}

		//showFloat(*SMap3Dr);

		/*auto t2 = std::chrono::high_resolution_clock::now();

		std::cout << "f() took "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
			<< " milliseconds\n";*/

		return SMap3D;
	}

	sibr::ImageFloat1::Ptr OutdoorRelightingDataGen::renderAreaSMapRay(sibr::Raycaster& raycaster, sibr::Mesh::Ptr proxyRC, float medianArea, const sibr::Vector2u& resolution, const sibr::Vector3f& sunDir, const sibr::Vector3f& north, const sibr::InputCamera& cam)
	{

		sibr::ImageFloat1::Ptr SMapArea = std::make_shared<sibr::ImageFloat1>(sibr::ImageFloat1(resolution.x(), resolution.y(), 20));

		sibr::Vector3f dx, dy, upLeftOffset;
		sibr::CameraRaycaster::computePixelDerivatives(cam, dx, dy, upLeftOffset);
		sibr::Vector3f camZaxis = cam.dir().normalized();
		float maxD = -1.0f, minD = -1.0f;

		//auto t1 = std::chrono::high_resolution_clock::now();

		sibr::Vector3f u1 = (sunDir.cross(north)).normalized();
		sibr::Vector3f u2 = (sunDir.cross(u1)).normalized();

#pragma omp parallel for
		for (int j = 0; j < resolution.y(); j++) {
			for (int i = 0; i < resolution.x(); i += 8) {

				float jj = j * cam.h() / (float)resolution.y();

				std::array<sibr::Ray, 8> rays;

				for (int i_r = 0; i_r < 8; i_r++) {
					float ii = (i + i_r) * cam.w() / (float)resolution.x();
					sibr::Vector3f worldPos = ((float)ii + 0.5f) * dx + ((float)jj + 0.5f) * dy + upLeftOffset;
					sibr::Vector3f dir = (worldPos - cam.position()).normalized();

					rays[i_r] = sibr::Ray(sibr::Ray(cam.position(), dir));

				}

				std::array<RayHit, 8> rayhits = raycaster.intersect8(rays);

				for (int i_r = 0; i_r < 8; i_r++) {
					if (!(i + i_r < resolution.x()) || !rayhits[i_r].hitSomething()) { continue; }

					const sibr::Vector3f& dir = rayhits[i_r].ray().dir();
					sibr::Vector3f hitPos = cam.position() + dir * rayhits[i_r].dist();

					sibr::Vector3f normal = -rayhits[i_r].normal().normalized();

					if (normal.dot(dir) > 0) {
						normal = -normal;
					}
					if (normal.dot(sunDir) < -0.01f) {

						//Compute area here;
						// Compute area of triangle
						sibr::Vector3u tri = proxyRC->triangles()[rayhits[i_r].primitive().triID];
						sibr::Vector3f v0 = proxyRC->vertices()[tri[0]];
						sibr::Vector3f v1 = proxyRC->vertices()[tri[1]];
						sibr::Vector3f v2 = proxyRC->vertices()[tri[2]];
						float area = (v1 - v0).cross(v2 - v0).norm() / 2;
						SMapArea(i + i_r, j).x() = std::min(area / medianArea, 20.0f);

						continue;
					}

					hitPos += 0.002f * normal;

					sibr::RayHit hitSun = raycaster.intersect(sibr::Ray(hitPos, sunDir));

					if (hitSun.hitSomething()) {

						sibr::Vector3u tri = proxyRC->triangles()[hitSun.primitive().triID];
						sibr::Vector3f v0 = proxyRC->vertices()[tri[0]];
						sibr::Vector3f v1 = proxyRC->vertices()[tri[1]];
						sibr::Vector3f v2 = proxyRC->vertices()[tri[2]];
						float area = (v1 - v0).cross(v2 - v0).norm() / 2;
						SMapArea(i + i_r, j).x() = std::min(area / medianArea, 20.0f);


					}

					//Compute area here;

				}

			}
		}

		return SMapArea;
	}

	sibr::ImageL8::Ptr sibr::OutdoorRelightingDataGen::renderAORay(
		sibr::Raycaster& raycaster,
		const sibr::Vector2u& resolution,
		const sibr::Vector3f& zenith,
		const sibr::Vector3f& north,
		const sibr::InputCamera& cam,
		const int spp,
		const std::vector<sibr::Vector2f> AApattern,
		sibr::ImageRGB::Ptr normals)
	{
		const int AA = AApattern.size();

		const int numRayPacket = spp / (8 * AA);
		if (numRayPacket <= 0) {
			std::cout << "Error : The number of sample per pixel is to small, spp should be higher then 8*AA" << std::endl;
			SIBR_ERR;
		}
		if (AA != 1 && AA != 2 && AA != 4) {
			std::cout << "Error : AA should be 1,2 or 4." << std::endl;
			SIBR_ERR;
		}

		sibr::ImageL8::Ptr AO = std::make_shared<sibr::ImageL8>(sibr::ImageL8(resolution.x(), resolution.y(), 0));

		sibr::Vector3f dx, dy, upLeftOffset;
		sibr::CameraRaycaster::computePixelDerivatives(cam, dx, dy, upLeftOffset);
		sibr::Vector3f camZaxis = cam.dir().normalized();
		float maxD = -1.0f, minD = -1.0f;

		//auto t1 = std::chrono::high_resolution_clock::now();

		sibr::Vector3f u1 = (zenith.cross(north)).normalized();
		sibr::Vector3f u2 = (zenith.cross(u1)).normalized();

#pragma omp parallel for
		for (int j = 0; j < resolution.y(); j++) {
			for (int i = 0; i < resolution.x(); i++) {

				float pixelVal = 0.0f;
				float weightVal = 0.0f;
				int rayNum = 0;
				for (int aa = 0; aa < AA; aa++) {

					float ii = (i + AApattern[aa].x()) * cam.w() / (float)resolution.x();
					float jj = (j + AApattern[aa].y()) * cam.h() / (float)resolution.y();

					sibr::Vector3f worldPos = ((float)ii + 0.5f) * dx + ((float)jj + 0.5f) * dy + upLeftOffset;
					sibr::Vector3f dir = (worldPos - cam.position()).normalized();

					sibr::RayHit hit = raycaster.intersect(sibr::Ray(cam.position(), dir));

					if (!hit.hitSomething()) { continue; }

					sibr::Vector3f hitPos = cam.position() + dir * hit.dist();

					sibr::Vector3f normal = -hit.normal().normalized();
					if (normal.dot(dir) > 0) {
						normal = -normal;
					}

					hitPos += 0.002f * normal;

					for (int rp = 0; rp < numRayPacket; rp++) {

						std::array<sibr::Ray, 8> rays;

						for (int rs = 0; rs < 8; rs++) {

							sibr::Vector2f uv = hammersley2d(rayNum, spp);
							rayNum++;
							float phi = uv.y() * 2.0 * M_PI;
							float cosTheta = 1.0 - uv.x();
							float sinTheta = sqrt(1.0 - cosTheta * cosTheta);

							sibr::Vector3f dirSkyRay = cos(phi) * sinTheta * u1 + sin(phi) * sinTheta * u2 + cosTheta * zenith;
							dirSkyRay.normalize();
							rays[rs] = sibr::Ray(hitPos, dirSkyRay);
						}

						std::array<bool, 8> hitSmthg = raycaster.hitSomething8(rays, 0.001f);

						for (int rs = 0; rs < 8; rs++) {
							if (!hitSmthg[rs])
								pixelVal += 1.0f / (8 * numRayPacket * AA);
						}
					}
				}

				AO(i, j).x() = (unsigned char)255.0f * pixelVal;

			}
		}

		return AO;
	}

	sibr::ImageL8::Ptr OutdoorRelightingDataGen::renderSkyMaskRay(sibr::Raycaster& raycaster, const sibr::Vector2u& resolution, const sibr::InputCamera& cam)
	{

		sibr::ImageL8::Ptr skyMask = std::make_shared<sibr::ImageL8>(sibr::ImageL8(resolution.x(), resolution.y(), 0));

		sibr::Vector3f dx, dy, upLeftOffset;
		sibr::CameraRaycaster::computePixelDerivatives(cam, dx, dy, upLeftOffset);
		sibr::Vector3f camZaxis = cam.dir().normalized();
		float maxD = -1.0f, minD = -1.0f;

#pragma omp parallel for
		for (int j = 0; j < resolution.y(); j++) {
			for (int i = 0; i < resolution.x(); i++) {

				float ii = i * cam.w() / (float)resolution.x();
				float jj = j * cam.h() / (float)resolution.y();

				sibr::Vector3f worldPos = ((float)ii + 0.5f) * dx + ((float)jj + 0.5f) * dy + upLeftOffset;
				sibr::Vector3f dir = (worldPos - cam.position()).normalized();

				sibr::RayHit hit = raycaster.intersect(sibr::Ray(cam.position(), dir));

				if (!hit.hitSomething()) { continue; }

				skyMask(i, j).x() = 255;

			}
		}
		return skyMask;
	}

	sibr::ImageL8::Ptr sibr::OutdoorRelightingDataGen::renderFirstBounceRay(sibr::Raycaster& raycaster, const sibr::Vector2u& resolutionFinal, const sibr::Vector3f& sunDir, const sibr::Vector3f& north, const sibr::InputCamera& cam, const int spp, const std::vector<sibr::Vector2f> AApattern, sibr::ImageRGB::Ptr normals)
	{
		const int AA = AApattern.size();

		const int numRayPacket = spp / (8 * AA);
		if (numRayPacket <= 0) {
			std::cout << "Error : The number of sample per pixel is to small, spp should be higher then 8*AA" << std::endl;
			SIBR_ERR;
		}
		if (AA != 1 && AA != 2 && AA != 4 && AA != 16) {
			std::cout << "Error : AA should be 1,2,4 or 16." << std::endl;
			SIBR_ERR;
		}

		sibr::Vector2i resolution(resolutionFinal.x() / 2, resolutionFinal.y() / 2);

		sibr::ImageL8::Ptr FB = std::make_shared<sibr::ImageL8>(sibr::ImageL8(resolution.x(), resolution.y(), 0));
		sibr::ImageL8::Ptr FBfinal = std::make_shared<sibr::ImageL8>(sibr::ImageL8(resolutionFinal.x(), resolutionFinal.y(), 0));

		sibr::Vector3f dx, dy, upLeftOffset;
		sibr::CameraRaycaster::computePixelDerivatives(cam, dx, dy, upLeftOffset);
		sibr::Vector3f camZaxis = cam.dir().normalized();
		float maxD = -1.0f, minD = -1.0f;

		//auto t1 = std::chrono::high_resolution_clock::now();

		//sibr::Vector3f u1 = (zenith.cross(north)).normalized();
		//sibr::Vector3f u2 = (zenith.cross(u1)).normalized();

		int normalNum = 0;
		int normalWrong = 0;

		for (int j = 0; j < resolution.y(); j++) {
			for (int i = 0; i < resolution.x(); i++) {

				float pixelVal = 0.0f;
				float weightVal = 0.0f;
				int rayNum = 0;
				for (int aa = 0; aa < AA; aa++) {

					float ii = (i + AApattern[aa].x()) * cam.w() / (float)resolution.x();
					float jj = (j + AApattern[aa].y()) * cam.h() / (float)resolution.y();

					sibr::Vector3f worldPos = ((float)ii + 0.5f) * dx + ((float)jj + 0.5f) * dy + upLeftOffset;
					sibr::Vector3f dir = (worldPos - cam.position()).normalized();

					sibr::RayHit hit = raycaster.intersect(sibr::Ray(cam.position(), dir));

					if (!hit.hitSomething()) { continue; }

					sibr::Vector3f hitPos = cam.position() + dir * hit.dist();

					sibr::Vector3f normal = -hit.normal().normalized();

					normalNum++;
					if (normal.dot(dir) > 0) {
						normalWrong++;
						normal = -normal;
					}

					hitPos += 0.001f * normal;

					sibr::Vector3f u1 = (normal.cross(north)).normalized();
					sibr::Vector3f u2 = (normal.cross(u1)).normalized();

					for (int rp = 0; rp < numRayPacket; rp++) {

						std::array<sibr::Ray, 8> rays;

						for (int rs = 0; rs < 8; rs++) {

							sibr::Vector2f uv = hammersley2d(rayNum, spp);
							rayNum++;
							float phi = uv.y() * 2.0 * M_PI;
							float cosTheta = 1.0 - uv.x();
							float sinTheta = sqrt(1.0 - cosTheta * cosTheta);

							sibr::Vector3f dirRay = cos(phi) * sinTheta * u1 + sin(phi) * sinTheta * u2 + cosTheta * normal;
							dirRay.normalize();
							rays[rs] = sibr::Ray(hitPos, dirRay);
						}

						std::array<RayHit, 8> rayhits = raycaster.intersect8(rays, std::vector<int>(8, -1), 0.002f);

						for (int rs = 0; rs < 8; rs++) {
							if (rayhits[rs].hitSomething()) {

								sibr::Vector3f hitPosBounce = hitPos + rayhits[rs].ray().dir() * rayhits[rs].dist();

								sibr::Vector3f normalBounce = rayhits[rs].normal().normalized();
								if (normalBounce.dot(rayhits[rs].ray().dir()) > 0) {
									normalBounce = -normalBounce;
								}

								hitPosBounce += 0.002f * normalBounce;

								if (!raycaster.hitSomething(sibr::Ray(hitPosBounce, sunDir), 0.001f)) {
									pixelVal += M_PI_2 * normal.dot(rayhits[rs].ray().dir()) / (8 * numRayPacket * AA);
								}

							}
						}
					}
				}

				FB(i, j).x() = (unsigned char)255.0f * pixelVal;

			}
		}
		std::cout << normalWrong << " over " << normalNum << std::endl;
		*FBfinal = FB->resized(resolutionFinal.x(), resolutionFinal.y(), cv::INTER_LINEAR);
		return FBfinal;
	}

	sibr::Vector2f OutdoorRelightingDataGen::getDisk(int i) {
		return disk[i];
	}

}
