/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


# include "projects/outdoor_relighting/renderer/LightDirAlignRenderer.hpp"
# include "core/graphics/RenderUtility.hpp"

namespace sibr
{

	LightDirAlignRenderer::~LightDirAlignRenderer() {};

	LightDirAlignRenderer::LightDirAlignRenderer(int w, int h, bool useFloats)
	{
		_useFloats = useFloats;
		_lightDirAlignShader.init("LightDirAlignShader",
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("lightDirAlignRenderer.vp")),
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("lightDirAlignRenderer.fp")));


		_lightDirAlignShader_proj.init(_lightDirAlignShader, "MVP");
		_lightDir.init(_lightDirAlignShader, "lightDir");
		_camPos.init(_lightDirAlignShader, "camPos");

		if(_useFloats)
			_lightDirAlign_RT_32F.reset(new sibr::RenderTargetRGBA32F(w, h));
		else
			_lightDirAlign_RT.reset(new sibr::RenderTargetLum(w, h));

	}

	void LightDirAlignRenderer::render(const sibr::InputCamera& cam, const Mesh& mesh, sibr::Vector3f lightDir)
	{

		if (_useFloats) {
			_lightDirAlign_RT_32F->clear(sibr::Vector4f(0.0f, 0.0f, 0.0f, 1.0f));
			glViewport(0, 0, _lightDirAlign_RT_32F->w(), _lightDirAlign_RT_32F->h());
			_lightDirAlign_RT_32F->bind();
		}
		else {
			_lightDirAlign_RT->clear();
			glViewport(0, 0, _lightDirAlign_RT->w(), _lightDirAlign_RT->h());
			_lightDirAlign_RT->bind();
		}


		_lightDirAlignShader.begin();
		_lightDirAlignShader_proj.set(cam.viewproj());
		_lightDir.set(lightDir);
		_camPos.set(cam.position());

		mesh.render(true, true, sibr::Mesh::FillRenderMode);

		_lightDirAlignShader.end();

	}

} // namespace