/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef RELIGHTINGDATAGEN_H
#define RELIGHTINGDATAGEN_H

#include "core/assets/InputCamera.hpp"
#include "core/graphics/MaterialMesh.hpp"
#include "core/raycaster/Raycaster.hpp"
#include "core/graphics/Window.hpp"
#include <map>
#include "projects/outdoor_relighting/renderer/Config.hpp"

namespace sibr
{
	class OutdoorRelightingDataGen
	{
	private:
		sibr::Mesh::Ptr _proxy;
		sibr::Vector3f _zenith;
		const std::vector<sibr::InputCamera::Ptr> _cams;
		std::vector<sibr::Vector2u> _outWH;
		std::map<int, std::vector<sibr::Vector2f>> _AAgrids;
		std::map<int, std::vector<sibr::ImageFloat4::Ptr>> _SMap3D;

	public:
		OutdoorRelightingDataGen(const sibr::Mesh::Ptr proxy, sibr::Vector3f zenith, const std::vector<sibr::InputCamera::Ptr>& cams, std::vector<sibr::Vector2u>& outWH);

		//utilities functions
		static sibr::InputCamera lightCam(sibr::Vector3f center, float radius, sibr::Vector3f dirLight, sibr::Vector3f up, int w, int h);
		//Using mathmatical convetion, theta is the angle between north and reprojected vector on the ground, phi is angle between zenith and dir
		static sibr::Vector3f sphericalToWorld(sibr::Vector2f thetaPhi, sibr::Vector3f zenith, sibr::Vector3f north);
		static void genSaveLightFile(int num, sibr::Vector3f zenith, std::string path, bool inOut = false);
		static sibr::Vector2f OutdoorRelightingDataGen::getDisk(int i);

		sibr::Vector3f manualFitSunPos(
			sibr::Window::Ptr window,
			sibr::Vector3f dirSun = sibr::Vector3f(0, 0, 0),const std::vector<sibr::ImageRGB::Ptr>& imgs = std::vector<sibr::ImageRGB::Ptr>());
		void selectZenith(sibr::Window::Ptr window);

		void setZenith(sibr::Vector3f zenith);

		sibr::Vector3f getZenith();

		static float radicalInverse_VdC(uint bits) {
			bits = (bits << 16u) | (bits >> 16u);
			bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
			bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
			bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
			bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
			return float(bits) * 2.3283064365386963e-10; // / 0x100000000
		}

		static sibr::Vector2f hammersley2d(uint i, uint N) {
			return sibr::Vector2f(float(i) / float(N), radicalInverse_VdC(i));
		}

		//Dataset related functions
		void genSaveNormals(
			std::string path);
		void genSaveSkyMask(
			sibr::Raycaster& raycaster,
			std::string path);
		void genSaveAO(
			sibr::Raycaster& raycaster,
			std::string path);
		void genSaveShadowMap(
			sibr::Raycaster& raycaster,
			sibr::Vector3f sunDir,
			std::string path,
			std::string lightName,
			sibr::MaterialMesh::Ptr proxyRayCaster);

		void genSaveSM3D(sibr::Raycaster& raycaster, sibr::Vector3f sunDir, std::string path, std::string lightName, int l) {

			_SMap3D[l] = std::vector<sibr::ImageFloat4::Ptr>(_cams.size());

#pragma omp parallel for num_threads(2)
			for (int imId = 0; imId < _cams.size(); imId++) {
				sibr::ImageFloat1::Ptr SMap3Dr(new sibr::ImageFloat1());
				_SMap3D[l][imId] = sibr::OutdoorRelightingDataGen::render3DSMapRay(raycaster, _outWH[imId], sunDir, sunDir + sibr::Vector3f(1, 1, 1), *_cams[imId], SMap3Dr);
				sibr::ImageL16 imL16;
				SMap3Dr->toOpenCV().convertTo(imL16.toOpenCVnonConst(), CV_16U, (1.0f / 20.0f) * 65535.0f);

				std::string filename = path + _cams[imId]->name() + "_" + lightName + ".png";
				{ // Create the output dir if doesn't exists
					boost::filesystem::path outdir = boost::filesystem::path(filename).parent_path();
					if (outdir.empty() == false)
						boost::filesystem::create_directories(outdir);
				}

				cv::imwrite(filename, imL16.toOpenCV());
			}
		}

		void genSaveSMArea(sibr::Raycaster& raycaster, sibr::Mesh::Ptr proxyRC, float medianArea, sibr::Vector3f sunDir, std::string path, std::string lightName, int l) {

#pragma omp parallel for num_threads(2)
			for (int imId = 0; imId < _cams.size(); imId++) {

				sibr::ImageFloat1::Ptr SMapArea = sibr::OutdoorRelightingDataGen::renderAreaSMapRay(raycaster, proxyRC, medianArea, _outWH[imId], sunDir, sunDir + sibr::Vector3f(1, 1, 1), *_cams[imId]);
				sibr::ImageL16 imL16;
				SMapArea->toOpenCV().convertTo(imL16.toOpenCVnonConst(), CV_16U, (1.0f / 20.0f) * 65535.0f);

				std::string filename = path + _cams[imId]->name() + "_" + lightName + ".png";
				{ // Create the output dir if doesn't exists
					boost::filesystem::path outdir = boost::filesystem::path(filename).parent_path();
					if (outdir.empty() == false)
						boost::filesystem::create_directories(outdir);
				}

				cv::imwrite(filename, imL16.toOpenCV());
			}
		}

		template<typename T, typename A>
		void genSaveShadowMapRGB(sibr::Raycaster& raycaster,
			std::string pathRGB,
			std::string pathStD,
			const sibr::Vector3f& sunDir,
			std::vector<int> lightSceIds,
			std::string lightNameTarget,
			const std::vector<sibr::InputCamera>& camDS,
			std::vector<sibr::ImageFloat1::Ptr>& dm,
			std::map<int, std::vector<typename T::Ptr, A>>& images,
			int lt)
		{

			sibr::makeDirectory(pathRGB);
			sibr::makeDirectory(pathStD);

#pragma omp parallel for num_threads(8)
			for (int imId = 0; imId < _cams.size(); imId++) {
				std::map<int, typename T::Ptr> RGBSMap;
				std::map<int, typename T::Ptr> StDSMap;
				renderRGBSMap<T>(raycaster, _outWH[imId], _cams[imId], sunDir, _SMap3D[lt][imId], camDS, dm, images, lightSceIds, RGBSMap, StDSMap);

				for (auto ls : lightSceIds) {

					std::ostringstream idLightSource;
					idLightSource << std::setw(8) << std::setfill('0') << ls;
					std::string lightNameSource = idLightSource.str();

					std::string filename = pathRGB + _cams[imId]->name() + "/" + lightNameSource + "_" + lightNameTarget + ".png";
					{ // Create the output dir if doesn't exists
						boost::filesystem::path outdir = boost::filesystem::path(filename).parent_path();
						if (outdir.empty() == false)
							boost::filesystem::create_directories(outdir);
					}
					cv::imwrite(pathRGB + _cams[imId]->name() + "/" + lightNameSource + "_" + lightNameTarget + ".png", RGBSMap[ls]->toOpenCVBGR());
					//cv::imwrite(pathStD + _cams[imId].name() + "_" + lightNameSource + "_" + lightNameTarget + ".png", StDSMap->toOpenCVBGR());
					//get distance map and 3d point
					//reproject into images
				}
			}
		}

		void genSaveFirstBounceMap(
			sibr::Raycaster& raycaster,
			sibr::Vector3f sunDir,
			std::string path,
			std::string lightName);
		void genSaveElevation(
			sibr::Vector3f sunDir,
			std::string path,
			std::string lightName);
		void genSaveSunDir(
			sibr::Vector3f sunDir,
			std::string path,
			std::string lightName);
		void genSaveReflexions(
			sibr::Vector3f sunDir,
			std::string path,
			std::string lightName);

		//Ray casting functions
		static void renderShadowMapRay(
			sibr::Texture2DLum::Ptr texture,
			sibr::Raycaster& raycaster,
			const sibr::Vector2u& resolution,
			const sibr::Vector3f& sunDir,
			const sibr::Vector3f& north,
			const sibr::InputCamera& cam,
			const int spp, const std::vector<sibr::Vector2f> AApattern,
			sibr::ImageRGB::Ptr normals = nullptr);
		static sibr::ImageL8::Ptr renderShadowMapRay(
			sibr::Raycaster& raycaster,
			const sibr::Vector2u& resolution,
			const sibr::Vector3f& sunDir,
			const sibr::Vector3f& north,
			const sibr::InputCamera& cam,
			const int spp, const std::vector<sibr::Vector2f> AApattern,
			sibr::ImageRGB::Ptr normals = nullptr);
		static sibr::ImageL8::Ptr renderShadowMapRayOpacity(
			sibr::Raycaster& raycaster,
			const sibr::Vector2u& resolution,
			const sibr::Vector3f& sunDir,
			const sibr::Vector3f& north,
			const sibr::InputCamera& cam,
			const int spp, const std::vector<sibr::Vector2f> AApattern,
			sibr::MaterialMesh::Ptr proxyRayCast);

		static sibr::ImageFloat4::Ptr render3DSMapRay(
			sibr::Raycaster& raycaster,
			const sibr::Vector2u& resolution,
			const sibr::Vector3f& sunDir,
			const sibr::Vector3f& north,
			const sibr::InputCamera& cam,
			sibr::ImageFloat1::Ptr& SMap3Dr);

		static sibr::ImageFloat1::Ptr renderAreaSMapRay(
			sibr::Raycaster& raycaster,
			sibr::Mesh::Ptr proxyRC,
			float medianArea,
			const sibr::Vector2u& resolution,
			const sibr::Vector3f& sunDir,
			const sibr::Vector3f& north,
			const sibr::InputCamera& cam);


		template<typename T>
		void renderRGBSMap(sibr::Raycaster& raycaster,
			const sibr::Vector2u& resolution,
			const sibr::InputCamera& cam,
			const sibr::Vector3f& sunDir,
			sibr::ImageFloat4::Ptr SMap3D,
			const std::vector<sibr::InputCamera>& camDS,
			std::vector<sibr::ImageFloat1::Ptr>& dm,
			std::map<int, std::vector<typename T::Ptr>>& images,
			std::vector<int> lightSceIds,
			std::map<int, typename T::Ptr>& RGBSMap,
			std::map<int, typename T::Ptr>& StDSMap)
		{

			typename T::Type val = std::numeric_limits<typename T::Type>::max();

			std::map<int, sibr::ImageFloat3> RGBAccumulator;
			//std::map<sibr::ImageFloat3> StDAccumulator;
			std::map<int, sibr::ImageFloat1> countAccumulator;

			for (auto ls : lightSceIds) {
				RGBSMap[ls] = std::make_shared<T>(T(resolution.x(), resolution.y(), typename T::Pixel(val, val, val)));
				StDSMap[ls] = std::make_shared<T>(T(resolution.x(), resolution.y(), typename T::Pixel(val, val, val)));
				RGBAccumulator[ls] = sibr::ImageFloat3(SMap3D->w(), SMap3D->h(), sibr::Vector3f(0, 0, 0));
				//StDAccumulator[ls] = sibr::ImageFloat3(SMap3D->w(), SMap3D->h(), sibr::Vector3f(0, 0, 0));
				countAccumulator[ls] = sibr::ImageFloat1(SMap3D->w(), SMap3D->h(), 0);
			}



			/*int idIn = 0;
			float minDist = 100000000;
			for (int imId = 0; imId < camDS.size(); imId += 1) {
				if ((camDS[imId].position() - cam.position()).norm() < minDist) {
					idIn = imId;
					minDist = (camDS[imId].position() - cam.position()).norm();
				}
			}*/
			//We use Welford's method to compute the variance online
			float radius;
			sibr::Vector3f center;
			_proxy->getBoundingSphere(center, radius);

			float stopThrld = radius / 10000;

#pragma omp parallel for
			for (int j = 0; j < resolution.y(); j++) {
				for (int i = 0; i < resolution.x(); i++) {

					float ratio = SMap3D(i, j).w();
					for (int imId = 0; imId < camDS.size(); imId += 1) {

						float ratioW = float(camDS[imId].w()) / images[0][imId]->w();
						float ratioH = float(camDS[imId].h()) / images[0][imId]->h();

						if (ratio >= 0) {
							sibr::Vector3f pos3D(SMap3D(i, j).xyz());
							sibr::Vector2f imCoord = camDS[imId].projectImgSpaceInvertY(pos3D).xy();
							sibr::Vector2i imCoordI(sibr::round(imCoord.x() / ratioW), sibr::round(imCoord.y() / ratioH));
							// If the ray object caster (which is parallel to sunDir) is colinear with the ray cam<->occluder
							// we minimize the error in reprojection with respect to the error in the mesh.
							// We decide to weight by this term to gett more accurate rgbSM

							float weightOrientation;
							if (ratio < 0.001) {//self shadowing
								float angleRay = acos(std::min(1.0f, std::max(-1.0f, (pos3D - cam.position()).normalized().dot((pos3D - camDS[imId].position()).normalized()))));
								weightOrientation = 1 / (pow(sin(angleRay / 2), 2.0f) + 0.01) - 0.98;
							}
							else {
								float angleRay = acos(std::min(1.0f, std::max(-1.0f, sunDir.dot((pos3D - camDS[imId].position()).normalized()))));
								weightOrientation = 1 / (pow(sin(angleRay / 2), 2.0f) + 0.01) - 0.98;
							}

							if (images[0][imId]->isInRange(imCoordI)) {

								const float d = dm[imId](imCoordI.x(), imCoordI.y()).x();

								sibr::Vector3f pos3Ddm = camDS[imId].unprojectImgSpaceInvertY(imCoordI, d);

								float weightPoint = weightOrientation / ((pos3D - pos3Ddm).squaredNorm() + 0.00001f);

								for (auto ls : lightSceIds) {
									countAccumulator[ls](i, j).x() += weightPoint;

									RGBAccumulator[ls](i, j) += weightPoint * images[ls][imId](imCoordI.x(), imCoordI.y()).cast<float>();

								}

								/*if ((pos3D - pos3Ddm).norm() < stopThrld)
									break;*/

									/*sibr::Vector3f X = images[imId](imCoordI.x(), imCoordI.y()).cast<float>();
									sibr::Vector3f oldM = RGBAccumulator(i, j);
									sibr::Vector3f X_oldM = X - oldM;

									RGBAccumulator(i, j) = oldM + X_oldM / countAccumulator(i, j).x();
									sibr::Vector3f S_prod = (X - RGBAccumulator(i, j)).array()*X_oldM.array();
									StDAccumulator(i, j) = StDAccumulator(i, j) + S_prod;*/
							}
						}
					}
				}

			}

#pragma omp parallel for
			for (int j = 0; j < resolution.y(); j++) {
				for (int i = 0; i < resolution.x(); i++) {

					if (countAccumulator[0](i, j).x() > 0) {
						for (auto ls : lightSceIds) {
							RGBSMap[ls](i, j) = (RGBAccumulator[ls](i, j) / countAccumulator[ls](i, j).x()).cast<typename T::Type>();
							//sibr::Vector3f temp = (StDAccumulator(i, j) / countAccumulator(i, j).x()).array().sqrt();
							//StDSMap(i, j) = (temp).cast<typename T::Type>();
						}
					}
					else {
						float ratio = SMap3D(i, j).w();
						if (ratio >= 0) {
							for (auto ls : lightSceIds) {
								RGBSMap[ls](i, j) = typename T::Pixel(0, 0, 0);
							}
							//StDSMap(i, j) = typename T::Pixel(0, 0, 0);
						}
					}

				}
			}

			//show(*RGBSMap);
			//show(*VarSMap);
		}

		static sibr::ImageL8::Ptr renderAORay(
			sibr::Raycaster& raycaster,
			const sibr::Vector2u& resolution,
			const sibr::Vector3f& zenith,
			const sibr::Vector3f& north,
			const sibr::InputCamera& cam,
			const int spp,
			const std::vector<sibr::Vector2f> AApattern,
			sibr::ImageRGB::Ptr normals = nullptr);

		template<typename T>
		static typename T::Ptr renderMirrorIm(
			sibr::Raycaster& raycaster,
			const sibr::InputCamera& cam,
			const std::vector<typename T::Ptr>& images,
			const std::vector<sibr::ImageL32F::Ptr>& depths,
			const std::vector<sibr::InputCamera>& cams,
			sibr::ImageRGB32F::Ptr& normalIm = nullptr) {


			sibr::Vector3f dx, dy, upLeftOffset;
			sibr::CameraRaycaster::computePixelDerivatives(cam, dx, dy, upLeftOffset);

			sibr::Vector2u resolution(cam.w(), cam.h());

			T::Ptr mirrorIm(new T(resolution.x(), resolution.y()));



#pragma omp parallel for
			for (int j = 0; j < resolution.y(); j++) {
				for (int i = 0; i < resolution.x(); i += 8) {

					float jj = j * cam.h() / (float)resolution.y();

					std::array<sibr::Ray, 8> rays;

					for (int i_r = 0; i_r < 8; i_r++) {
						float ii = (i + i_r) * cam.w() / (float)resolution.x();
						sibr::Vector3f worldPos = ((float)ii + 0.5f) * dx + ((float)jj + 0.5f) * dy + upLeftOffset;
						sibr::Vector3f dir = (worldPos - cam.position()).normalized();

						rays[i_r] = sibr::Ray(sibr::Ray(cam.position(), dir));

					}

					std::array<RayHit, 8> rayhits = raycaster.intersect8(rays);

					for (int i_r = 0; i_r < 8; i_r++) {
						if (!(i + i_r < resolution.x())) { continue; }

						float ii = (i + i_r) * cam.w() / (float)resolution.x();

						if (!rayhits[i_r].hitSomething()) {
							mirrorIm(ii, jj).x() = 1.0;
							mirrorIm(ii, jj).y() = 1.0;
							mirrorIm(ii, jj).z() = 1.0;
							continue;
						}



						const sibr::Vector3f& dir = rayhits[i_r].ray().dir();
						sibr::Vector3f hitPos = cam.position() + dir * rayhits[i_r].dist();

						sibr::Vector3f normal = -rayhits[i_r].normal().normalized();

						if (normal.dot(dir) > 0) {
							normal = -normal;
						}

						hitPos += 0.002f * normal;

						if (normalIm) {
							sibr::Vector4f normal4f(normal.x(), normal.y(), normal.z(), 0.0f);
							sibr::Vector3f normalCamScaled = 0.5 * ((cam.view() * normal4f).xyz() + sibr::Vector3f(1.0f, 1.0f, 1.0f));
							//sibr::Vector3ub normalColor = normalCamScaled.cast<unsigned char>();
							normalIm(ii, jj) = normalCamScaled;
						}

						sibr::Vector3f mirrorDir = dir - 2 * normal.dot(dir) * normal;

						sibr::RayHit hitMirror = raycaster.intersect(sibr::Ray(hitPos, mirrorDir));

						if (hitMirror.hitSomething()) {

							sibr::Vector3f hitPosMirror = hitPos + mirrorDir * hitMirror.dist();

							std::vector<std::pair<float, int>> weightIm;
							for (int im = 0; im < cams.size(); im++) {
								float w = mirrorDir.dot((hitPosMirror - cams[im].position()).normalized());

								weightIm.push_back(std::make_pair(w, im));
							}

							std::sort(weightIm.begin(), weightIm.end(),
								[](const std::pair<float, int>& a, const std::pair<float, int>& b)-> bool
								{return (a.first > b.first); });

							for (int wIm = 0; wIm < weightIm.size(); wIm++) {
								int im = weightIm[wIm].second;
								sibr::Vector2f mirrorPosIm = cams[im].projectImgSpaceInvertY(hitPosMirror).xy();
								sibr::Vector2i mirrorPosImInt(floor(mirrorPosIm.x()), floor(mirrorPosIm.y()));
								if (images[im]->isInRange(mirrorPosImInt)) {
									sibr::Vector3f pos3D = cams[im].unprojectImgSpaceInvertY(mirrorPosImInt, depths[im](mirrorPosImInt).x());

									if ((pos3D - hitPosMirror).norm() < 0.01) {
										mirrorIm(ii, jj) = images[im](mirrorPosImInt);
										break;
									}
								}
							}
						}
						else {
							mirrorIm(ii, jj).x() = 1.0;
							mirrorIm(ii, jj).y() = 1.0;
							mirrorIm(ii, jj).z() = 1.0;
						}

						//Compute area here;

					}

				}
			}

			return mirrorIm;
		}

		template<typename T>
		static typename T::Ptr renderDiffuseIm(
			sibr::Raycaster& raycaster,
			const sibr::InputCamera& cam,
			const std::vector<typename T::Ptr>& images,
			const std::vector<sibr::ImageL32F::Ptr>& depths,
			const std::vector<sibr::InputCamera>& cams) {


			int numRayPacket = 64;
			int spp = 8 * numRayPacket;


			sibr::Vector3f dx, dy, upLeftOffset;
			sibr::CameraRaycaster::computePixelDerivatives(cam, dx, dy, upLeftOffset);

			sibr::Vector2u resolution(cam.w(), cam.h());

			T::Ptr diffuseIm(new T(resolution.x(), resolution.y()));

			std::random_device rd;
			std::mt19937 gen(rd());
			std::uniform_real_distribution<> dist(0.0, 1.0);

#pragma omp parallel for
			for (int j = 0; j < resolution.y(); j++) {
				for (int i = 0; i < resolution.x(); i += 8) {

					float jj = j * cam.h() / (float)resolution.y();

					std::array<sibr::Ray, 8> rays;

					for (int i_r = 0; i_r < 8; i_r++) {
						float ii = (i + i_r) * cam.w() / (float)resolution.x();
						sibr::Vector3f worldPos = ((float)ii + 0.5f) * dx + ((float)jj + 0.5f) * dy + upLeftOffset;
						sibr::Vector3f dir = (worldPos - cam.position()).normalized();

						rays[i_r] = sibr::Ray(sibr::Ray(cam.position(), dir));

					}

					std::array<RayHit, 8> rayhits = raycaster.intersect8(rays);

					for (int i_r = 0; i_r < 8; i_r++) {
						if (!(i + i_r < resolution.x())) { continue; }

						float ii = (i + i_r) * cam.w() / (float)resolution.x();

						if (!rayhits[i_r].hitSomething()) {
							diffuseIm(ii, jj).x() = 1.0;
							diffuseIm(ii, jj).y() = 1.0;
							diffuseIm(ii, jj).z() = 1.0;
							continue;
						}

						const sibr::Vector3f& dir = rayhits[i_r].ray().dir();
						sibr::Vector3f hitPos = cam.position() + dir * rayhits[i_r].dist();

						sibr::Vector3f normal = -rayhits[i_r].normal().normalized();

						if (normal.dot(dir) > 0) {
							normal = -normal;
						}

						hitPos += 0.001f * normal;

						sibr::Vector3f u1 = (normal.cross(sibr::Vector3f(0.728, 0.568, 0.845))).normalized();
						sibr::Vector3f u2 = (normal.cross(u1)).normalized();

						int rayNum = 0;
						float weightRay = 0;
						sibr::Vector3f pixelVal(0, 0, 0);



						for (int rp = 0; rp < numRayPacket; rp++) {

							std::array<sibr::Ray, 8> raysSecond;
							std::array<float, 8> thetas;

							for (int rs = 0; rs < 8; rs++) {

								rayNum++;
								float r = dist(gen);
								float phi = r * 2.0 * M_PI;

								float cosTheta = dist(gen);
								float theta = acos(cosTheta);

								thetas[rs] = theta;
								float sinTheta = sqrt(1.0 - cosTheta * cosTheta);

								sibr::Vector3f dirSkyRay = cos(phi) * sinTheta * u1 + sin(phi) * sinTheta * u2 + cosTheta * normal;
								dirSkyRay.normalize();
								raysSecond[rs] = sibr::Ray(hitPos, dirSkyRay);
							}

							std::array<sibr::RayHit, 8> rayHitsSecond = raycaster.intersect8(raysSecond);

							for (int rs = 0; rs < 8; rs++) {
								if (rayHitsSecond[rs].hitSomething()) {
									weightRay += cos(thetas[rs]);
									sibr::Vector3f hitPosMirror = hitPos + raysSecond[rs].dir() * rayHitsSecond[rs].dist();

									std::vector<std::pair<float, int>> weightIm;
									for (int im = 0; im < cams.size(); im++) {
										float w = raysSecond[rs].dir().dot((hitPosMirror - cams[im].position()).normalized());

										weightIm.push_back(std::make_pair(w, im));
									}

									std::sort(weightIm.begin(), weightIm.end(),
										[](const std::pair<float, int>& a, const std::pair<float, int>& b)-> bool
										{return (a.first > b.first); });

									for (int wIm = 0; wIm < weightIm.size(); wIm++) {
										int im = weightIm[wIm].second;
										sibr::Vector2f mirrorPosIm = cams[im].projectImgSpaceInvertY(hitPosMirror).xy();
										sibr::Vector2i mirrorPosImInt(floor(mirrorPosIm.x()), floor(mirrorPosIm.y()));
										if (images[im]->isInRange(mirrorPosImInt)) {
											sibr::Vector3f pos3D = cams[im].unprojectImgSpaceInvertY(mirrorPosImInt, depths[im](mirrorPosImInt).x());

											if ((pos3D - hitPosMirror).norm() < 0.01) {
												pixelVal += cos(thetas[rs]) * images[im](mirrorPosImInt);
												break;
											}
										}
									}
								}
								else {
									weightRay += cos(thetas[rs]);
									pixelVal.x() += cos(thetas[rs]) * 1.0;
									pixelVal.y() += cos(thetas[rs]) * 1.0;
									pixelVal.z() += cos(thetas[rs]) * 1.0;
								}

							}
						}

						if (weightRay > 0) {
							diffuseIm(ii, jj).x() = pixelVal.x() / weightRay;
							diffuseIm(ii, jj).y() = pixelVal.y() / weightRay;
							diffuseIm(ii, jj).z() = pixelVal.z() / weightRay;
						}

					}

				}
			}

			return diffuseIm;
		}

		static sibr::ImageL8::Ptr renderSkyMaskRay(
			sibr::Raycaster& raycaster,
			const sibr::Vector2u& resolution,
			const sibr::InputCamera& cam);
		static sibr::ImageL8::Ptr renderFirstBounceRay(
			sibr::Raycaster& raycaster,
			const sibr::Vector2u& resolution,
			const sibr::Vector3f& sunDir,
			const sibr::Vector3f& north,
			const sibr::InputCamera& cam,
			const int spp, const std::vector<sibr::Vector2f> AApattern,
			sibr::ImageRGB::Ptr normals);
		static std::vector<sibr::Vector3f> loadLight(std::string lightFile);

	};
#ifndef DISKVAR_H // header guards
#define DISKVAR_H
static	std::array<sibr::Vector2f, 64> disk = { {
sibr::Vector2f(-0.613392, 0.617481),
sibr::Vector2f(0.170019, -0.040254),
sibr::Vector2f(-0.299417, 0.791925),
sibr::Vector2f(0.645680, 0.493210),
sibr::Vector2f(-0.651784, 0.717887),
sibr::Vector2f(0.421003, 0.027070),
sibr::Vector2f(-0.817194, -0.271096),
sibr::Vector2f(-0.705374, -0.668203),
sibr::Vector2f(0.977050, -0.108615),
sibr::Vector2f(0.063326, 0.142369),
sibr::Vector2f(0.203528, 0.214331),
sibr::Vector2f(-0.667531, 0.326090),
sibr::Vector2f(-0.098422, -0.295755),
sibr::Vector2f(-0.885922, 0.215369),
sibr::Vector2f(0.566637, 0.605213),
sibr::Vector2f(0.039766, -0.396100),
sibr::Vector2f(0.751946, 0.453352),
sibr::Vector2f(0.078707, -0.715323),
sibr::Vector2f(-0.075838, -0.529344),
sibr::Vector2f(0.724479, -0.580798),
sibr::Vector2f(0.222999, -0.215125),
sibr::Vector2f(-0.467574, -0.405438),
sibr::Vector2f(-0.248268, -0.814753),
sibr::Vector2f(0.354411, -0.887570),
sibr::Vector2f(0.175817, 0.382366),
sibr::Vector2f(0.487472, -0.063082),
sibr::Vector2f(-0.084078, 0.898312),
sibr::Vector2f(0.488876, -0.783441),
sibr::Vector2f(0.470016, 0.217933),
sibr::Vector2f(-0.696890, -0.549791),
sibr::Vector2f(-0.149693, 0.605762),
sibr::Vector2f(0.034211, 0.979980),
sibr::Vector2f(0.503098, -0.308878),
sibr::Vector2f(-0.016205, -0.872921),
sibr::Vector2f(0.385784, -0.393902),
sibr::Vector2f(-0.146886, -0.859249),
sibr::Vector2f(0.643361, 0.164098),
sibr::Vector2f(0.634388, -0.049471),
sibr::Vector2f(-0.688894, 0.007843),
sibr::Vector2f(0.464034, -0.188818),
sibr::Vector2f(-0.440840, 0.137486),
sibr::Vector2f(0.364483, 0.511704),
sibr::Vector2f(0.034028, 0.325968),
sibr::Vector2f(0.099094, -0.308023),
sibr::Vector2f(0.693960, -0.366253),
sibr::Vector2f(0.678884, -0.204688),
sibr::Vector2f(0.001801, 0.780328),
sibr::Vector2f(0.145177, -0.898984),
sibr::Vector2f(0.062655, -0.611866),
sibr::Vector2f(0.315226, -0.604297),
sibr::Vector2f(-0.780145, 0.486251),
sibr::Vector2f(-0.371868, 0.882138),
sibr::Vector2f(0.200476, 0.494430),
sibr::Vector2f(-0.494552, -0.711051),
sibr::Vector2f(0.612476, 0.705252),
sibr::Vector2f(-0.578845, -0.768792),
sibr::Vector2f(-0.772454, -0.090976),
sibr::Vector2f(0.504440, 0.372295),
sibr::Vector2f(0.155736, 0.065157),
sibr::Vector2f(0.391522, 0.849605),
sibr::Vector2f(-0.620106, -0.328104),
sibr::Vector2f(0.789239, -0.419965),
sibr::Vector2f(-0.545396, 0.538133),
sibr::Vector2f(-0.178564, -0.596057) } };

#endif


}
#endif