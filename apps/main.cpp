/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#define _DISABLE_EXTENDED_ALIGNED_STORAGE

//Relighting app related
#include "projects/outdoor_relighting/renderer/OutdoorRelighter.hpp"
#include "core/system/CommandLineArgs.hpp"
#include "core/raycaster/Raycaster.hpp"
#include "projects/outdoor_relighting/renderer/OutdoorRelightingIBRScene.hpp"
#include "core/scene/BasicIBRScene.hpp"
#include "core/raycaster/PlaneEstimator.hpp"


#define PROGRAM_NAME "sibr_outdoor_relighting_app"
//------------------------------------------------------------------------------

int main(int argc, char** argv) {

	sibr::CommandLineArgs::parseMainArgs(argc, argv);
	sibr::OutdoorRelightingAppArgs myArgs;

	//const std::string inPath = "D:/Users/jphilip/Relighting/RelightingScene/stonehenge/sibr_scene";
	const std::string inPath = myArgs.dataset_path;
	// /!\ do not put the model.pb at the end we now read the layout too
	const std::string model_path = myArgs.model;
	bool useCloudiness = myArgs.cloud;
	bool interactive = myArgs.interactive;
	bool interactiveULR = myArgs.ulr;
	bool video = false;
	int numFrames = myArgs.numFrames;
	int specifiedW = myArgs.outW;
	int specifiedH = myArgs.outH;
	bool fitPlane = myArgs.fitPlane;
	int incrlc = 1;
	bool synthetic = myArgs.synthetic;
	bool exr = myArgs.exr;
	if (!interactive && !interactiveULR && myArgs.video) {
		video = true;
	}
	if (myArgs.ablation) {
		incrlc = 10;
	}
	std::string outPath = inPath;
	if (myArgs.outPath.get() != std::string("")) {
		outPath = myArgs.outPath;
	}

	//We need to do that for tensorflow to work correctly
	for (int i = 1; i < argc; ++i)
		argv[i] = "";
	argc = 1;

	const std::string data_folder = "D:/Users/jphilip/IBR/sibr/install/test/blend/";
	const std::string log_folder = data_folder + "logs/";
	const bool logging_enabled = false;



	sibr::Window::Ptr			window = std::make_shared<sibr::Window>(256, 256, PROGRAM_NAME);

	// Setup Basic IBR Scene
	sibr::OutdoorRelightingIBRScene::Ptr		scene(new sibr::OutdoorRelightingIBRScene(myArgs,window,true));
	std::cout << scene->_width << " " << scene->_height << std::endl;
	if (fitPlane) {
		PlaneEstimator PE(scene->proxies()->proxy().vertices(), true);
		PE.computePlanes(10, 0.5, 10000);
		std::vector<sibr::Vector3f> upCams;
		for (auto & c : scene->cameras()->inputCameras()) {
			upCams.push_back(c->up());
		}
		sibr::Vector3f zenithTest = PlaneEstimator::estimateMedianVec(upCams);
		sibr::Vector4f groundPlane = PE.estimateGroundPlane(zenithTest);

		if (groundPlane.xyz().dot(zenithTest) < 0) {
			groundPlane.x() = -groundPlane.x();
			groundPlane.y() = -groundPlane.y();
			groundPlane.z() = -groundPlane.z();
		}
		float radius;
		sibr::Vector3f center;
		scene->proxies()->proxy().getBoundingSphere(center, radius);
		sibr::Mesh::Ptr proxyPlane = std::make_shared<sibr::Mesh>(PlaneEstimator::getMeshPlane(groundPlane, center, radius));
		proxyPlane->makeWhole();
		//std::cout << "Col:" << proxy->hasColors() << std::endl;
		scene->proxies()->proxyPtr()->merge(*proxyPlane);
		//std::cout << "Col:" << proxy->hasColors() << std::endl;
	}

	//// Build the relighter

	sibr::OutdoorRelighter RL(
		window,
		scene->_width,
		scene->_height,
		model_path,
		scene->cameras()->inputCameras(),
		scene->proxies()->proxyPtr(),
		scene->images()->inputImages(),
		scene->_inputSunDir,
		scene->_zenith,
		scene->_north
		);

	if (interactive) {
		RL.runInteractiveSession(window, useCloudiness, myArgs.startIm);
	}
	else if (interactiveULR) {
		RL.runULRSession(window, scene,myArgs, myArgs.cloud.get(), myArgs.dataset_path.get());
	}
	else {
		std::cout << inPath << std::endl;

		std::vector<sibr::OutdoorRelighter::lightingCondition> lightConds = sibr::OutdoorRelighter::loadLightingConditions(inPath + "/lightingConditions.txt", scene->_zenith, scene->_north, outPath);
		if (lightConds.empty()) {
			std::cout << "Creating lighting conditions" << std::endl;
			if (video) {
				lightConds = RL.createLightingConditionsVideo(outPath + "/VideoFrames/", exr);
			}
			else {
				lightConds = RL.createLightingConditions(outPath, numFrames, exr);
			}
		}
		//Copy input
		for (auto lc : lightConds) {
			std::string ext;

			std::string oriImg = inPath + "/images/" + scene->data()->imgInfos()[lc.imId].filename;
			boost::filesystem::path outdir = boost::filesystem::path(lc.path).parent_path();
			if (outdir.empty() == false)
				boost::filesystem::create_directories(outdir);
			boost::filesystem::copy_file(oriImg, outdir.append("/input.png"), boost::filesystem::copy_option::overwrite_if_exists);
		}
		std::vector<sibr::OutdoorRelighter::lightingCondition> lightCondsIncr;
		for (int lc_num = 0; lc_num < lightConds.size(); lc_num += incrlc) {
			sibr::OutdoorRelighter::lightingCondition lc = lightConds[lc_num];
			lightCondsIncr.push_back(lc);
		}
		RL.runSaveSession(window, lightCondsIncr, false);
	}



	return 0;

}
